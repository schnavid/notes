\section{Exkurs: Gleichmäßige Konvergenz von Funktionenfolgen}

Sind Potenzreihen (genauer: die durch Potenzreihen definierten Funktionen) stetig? Um die Antwort gleich vorwegzunehmen: Ja, im Inneren des Konvergenzkreises jedenfalls. Insbesondere sind die Exponentialfunktion und die trigonometrischen Funktionen stetig auf $\mathbb{C}$.

Die allgemeine Aussage für Potenzreihen kann man durch direkte Rechnung verifizieren, vgl $5.4.8$ im Skript. Eine Alternative bestegt im folgenden Argument:

\begin{enumerate}[label = (\roman*)]
    \item Polynome sind stetig. (s.o.)
    \item Ist $P(z) = \sum\limits_{n=0}^\infty a_n z^n = \lim\limits_{N \to \infty} \sum\limits_{n = 0}^N a_n z^n$ eine Potenzreihe, so handelt es sich um den Grenzwert einer Folge von Polynomen (der Partialsummenfolge nämlich).
\end{enumerate}

Hieraus möchte man auf die Stetigkeit von $P$ schliessen. Das ist problematisch wie das folgende Beispiel zeigt: Für $n \in \mathbb{N}$ sei

\[ f_n : [0, 1] \to \mathbb{R}, x \mapsto f_n(x) = x^n. \]

Dann sind alle Funktionen $f_n$ stetig, aber

\[ \lim_{n \to \infty} f_n(x) = \lim_{n \to \infty} x^n = \left\{\begin{aligned}0 &\text{ für } &0 \leq x < 1\\ 1 &\text{ für } &x = 1\end{aligned}\right\} =: f(x),\]

die Grenzfunktion $f$ ist also unstetig in $x_0 = 1$.

Der Ausweg aus dieser mißlichen Situation besteht darin, zwischen \emph{punktweiser} und \emph{gleichmäßiger} Konvergenz von Funktionenfolgen zu unterscheiden.

Dazu sei $(f_n)_n$ eine Folge von Funktionen

\[f_n: \mathbb{C}\supset X \to \mathbb{C}\]

mit gemeinsamen Definitionsbereich $X$.

\begin{definition}
    Eine Funktionenfolge $(f_n)_n$ heißt

    \begin{enumerate}
        \item punktweise konvergent gegen $f: X \to \mathbb{C}$, wenn für alle $x \in X$ gilt: $\lim\limits_{n \to \infty} f_n(x) = f(x)$;
        \item gleichmäßig konvergent gegen $f: X \to \mathbb{C}$, wenn $\lim\limits_{n \to \infty} \sup_{x \in X} \abs{f_n(x) - f(x)} = 0$ gilt.
    \end{enumerate}
\end{definition}

\begin{discussion}
    \begin{enumerate}
        \item Alternative Definitionen der glm. Konvergenz mit Hilfe von $\varepsilon$ und $N(\varepsilon)$:

            \begin{align*}
                &(f_n)_n \text{ konvergiert gleichmäßig gegen } f\\
                \Leftrightarrow &\forall \varepsilon > 0 \exists N = N(\varepsilon) \in \mathbb{N} \forall n \geq N: \sup \abs{f_n(x) - f(x)} \leq x \in X\\
                \Leftrightarrow &\forall \varepsilon > 0 \exists N = N(\varepsilon) \in \mathbb{N} \forall n \geq N: \abs{f_n(x) - f(x)} \leq \varepsilon \forall x \in X.
            \end{align*}

        \item Für die punktweise Kovergenz ist lediglich gefordert:

            \[\forall \varepsilon > 0, x \in X \exists N = N(\varepsilon, x) \in \mathbb{N} \forall n \geq N: \abs{f_n(x) - f(x)} \leq \varepsilon\]

            Hier darf $N$ also auch vom Punkt $x \in X$ abhängen, was bei der glm. Konvergenz nicht zugelassen ist. Wir sehen:

        \item glm. Konvergenz $\Rightarrow$ punktweises Konvergenz
        \item Die glm. Konvergenz ist eine \emph{echt} stärkere Eigenschaft als die punktweise, wie auch das obige Beispiel zeigt. Dafür haben wir:

            \[\lim_{n \to \infty} \sup\limits_{x \in [0, 1]} \abs{f_n(x) - f(x)} = \lim_{n \to \infty} \sup\limits_{0 \leq x < 1} x^n = 1,\]

            also ist die Konvergenz nicht glm.
    \end{enumerate}
\end{discussion}

Der für unseren Zweck entscheidene Vorteil der glm. Konvergenz ist, dass hierber sowohl Stetigkeit wie auch glm. Stetigkeit von der approximierenden Folge auf die Grenzfunktion vererbt wird. Genauer gilt:

\begin{theorem}
    Es sei $(f_n)$ eine Folge (gleichmäßig) stetiger Funktionen $f_n: \mathbb{C} \supset X \to \mathbb{C}$, die gleichmäßig gegen eine Funktion $f: \mathbb{C} \supset X \to \mathbb{C}$ konvergiert. Dann ist auch $f$ (gleichmäßig) stetig.
\end{theorem}

\begin{proof}
    (Für glm. stetig.): Für $x,y \in X$ gilt:

    \[f(x) - f(y) = f(x) - f_n(x) + f_n(x) - f_n(y) + f_n(y) - f(y),\]

    also mit der Dreiecksungleichung

    \[\abs{f(x) - f(y)} \leq \abs{f(x) - f_n(x)} + \abs{f_n(x) - f_n(y)} + \abs{f_n(y) - f(y)} =: \Rn{1} + \Rn{2} + \Rn{3}\]

    Nun sei $\varepsilon > 0$ vorgegeben. Dann finden wir wegen der glm. Konvergenz $f_n \to f$ ein $N \in \mathbb{N}$, sodass für alle $n \geq N$ gilt

    \[\sup\limits_{x \in X} \abs{f_n(x) - f(y)} < \frac{\varepsilon}{3}.\]

    Das ergibt $\Rn{1} < \frac{\varepsilon}{3}$ und $\Rn{3} < \frac{\varepsilon}{3}$. Nun sei ein $n \geq N$ fixiert. Da $f_n: X \to \mathbb{C}$ glm. stetig ist, existiert ein $\delta > 0$, sodass $\forall x, y \in X$ mit $\abs{x - y} < \delta$ gilt

    \[\Rn{2} = \abs{f_n(x) - f_n(y)} < \frac{\varepsilon}{3}.\]

    \emph{Zusammenfassend:} Ist $\delta$ wie oben gewählt, so gilt $\forall x, y \in X$ mit $\abs{x - y} < \delta$, dass $\abs{f(x) - f(y)} < \varepsilon$.

    Das ist die glm. Stetigkeit von $f: X \to \mathbb{C}$.
\end{proof}

\subsection{Anwendung}

Nun sei $P(z) = \sum\limits_{n = 0}^\infty a_n z^n$ eine Potenzreihe mit Konvergenzradius $R > 0$. Dann gilt für jedes $r \in [0, R)$ und für jedes $z \in \overline{K_r(0)} = \{z \in \mathbb{C}: \abs{z} < r\}$:

\[\abs{P(z) - \sum_{n = 0}^N a_n z^n} = \abs{\sum_{n = N+1}^\infty a_n z^n} \leq \sum_{n = N+1}^\infty \abs{a_n} r^n \xrightarrow[N \to \infty]{} 0,\]

letzteres, da $r < R$ ist und Potenzreihen im Inneren ihres Konvergenzkreises absolut konvergieren. Es folgt

\[\lim_{N \to \infty} \sup_{\abs{x} \leq r} \abs{P(z) - \sum_{n = 0}^N a_n z^n} \leq \lim_{N \to \infty} \sum_{n = N + 1}^\infty \abs{a_n} r^n = 0,\]

also die \emph{glm.} Konvergenz von $P(z)$ auf $\overline{K_r(0)}$. Zusammen mit Satz 3 aus dem vorigen Abschnitt und dem Satz E1 ergibt sich:

\begin{theorem}
    Sei $P(z) = \sum\limits_{n = 0}^\infty a_n z^n$ eine Potenzreihe mit Konvergenzradius $R>0$. Dann konvergiert $P$ für jedes $r \in [0, R)$ gleichmäßig auf $\overline{K_r(0)}$. Die durch die Potenzreihe definierte Funktion

    \[P: K_R(0) = \{z \in \mathbb{C}: \abs{z} < R\} \to \mathbb{C}\]

    ist für jedes $r \in [0, R)$ glm. stetig auf $\overline{K_r(0)}$ und somit stetig auf $K_R(0)$.

    \emph{Bsp.:} $\exp, \cos, \sin: \mathbb{C} \to \mathbb{C}$ sind stetig.
\end{theorem}
