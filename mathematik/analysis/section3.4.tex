\section{Exponentialreihen und trigonometrische Funktionen}

\begin{theorem}
    Für jedes $z \in \mathbb{C}$ konvergiert die Potenzreihe

    \[ \exp(z) := \sum_{n=0}^\infty \frac{z^n}{n!} \text{ absolut.} \]
\end{theorem}

\begin{remark}
    Diese Reihe wird als Exponentialreihe bezeichnet.
\end{remark}

\begin{proof}
    Folgt aus der Eulerschen Formel, da für $a_n = \frac{1}{n!}$ gilt:

    \[ \lim_{n \to \infty} \abs{\frac{a_n}{a_{n+1}}} = \lim_{n \to \infty} \frac{(n+1)!}{n!} = \lim_{n \to \infty} n + 1 = \infty. \]
\end{proof}

\begin{definition}{(Exponentialfunktion)}
    Die Abbildung $\exp: \mathbb{C} \to \mathbb{C}, z \mapsto \exp{z}$ heißt \emph{Exponentialfunktion}.
\end{definition}

Der Zusammenhang zur Eulerschen Zahl $e = \lim\limits_{n \to \infty} (1 + \frac{1}{n})^n$ wird hergestellt durch den folgenden Satz:

\begin{theorem}
    Für alle $z \in \mathbb{C}$ gilt $\exp(z) = \lim\limits_{n \to \infty} {(1 + \frac{z}{n})}^n$. Insbesondere ist $\exp(1) = e$.
\end{theorem}

\begin{proof}
    Wir fixieren $z \in \mathbb{C}$ und schreiben $\exp(z) = \sum\limits_{k=0}^\infty \frac{z^k}{k!}$ sowie

    \begin{align*}
        {\left(1 + \frac{z}{n}\right)}^n &= \sum_{k=0}^n \binom{n}{k} \frac{z^k}{n^k} \text{\hfill (binomischer Lehrsatz)}\\
        &= \sum_{k=0}^n \frac{n!}{(n-k)!n^k}* \frac{z^k}{k!}
    \end{align*}

    sodass

    \[ \exp(z) - {\left(1 + \frac{z}{n}\right)}^n = \sum_{k=0}^n \left(1 - \frac{n!}{(n-k)!n^k}\right) \frac{z^k}{k!} + \sum_{k=n+1}^\infty \frac{z^k}{k!}. \]

    Nun ist

    \[ \frac{n!}{(n-k)!n^k} = \frac{n (n-1) \cdots (n-k+1)}{n \cdot n \cdots n} = 1 \cdot \left(1 - \frac{1}{n}\right)\left(1 - \frac{2}{n}\right) \cdots \left(1 - \frac{k-1}{n}\right) \]

    und daher

    \begin{equation}
        \frac{n!}{(n-k)!n^k} \in (0, 1) \Rightarrow \left(1 - \frac{n!}{(n-k)!n^k}\right) \in (0, 1) 
    \end{equation}

    sowie, für jedes feste $k$:

    \begin{equation}
        \lim_{n \to \infty} 1 - \frac{n!}{(n-k)!n^k} = 0
    \end{equation}

    Nun wählen wir $N$ so groß, dass $\sum\limits_{k=N+1}^\infty \frac{\abs{z}^k}{k!} < \varepsilon ist$, hierbei sei $\varepsilon > 0$ vorgegeben. Dann folgt mit $(1)$

    \[ \left|\exp(z) - {\left(1 + \frac{z}{n}\right)}^n\right| \leq \sum_{k=0}^N \left(1 - \frac{n!}{(n-k)!n^k}\right) \cdot \frac{\abs{z}^k}{k!} + \varepsilon \]

    Beachten wir zusätzlivh $(2)$, so ergibt sich

    \[ \limsup_{n \to \infty} \left|\exp(z) - \left(1 + \frac{z}{n}\right)^n\right| \leq \varepsilon \]

    Dies gilt für jedes $\varepsilon > 0$ und daher

    \[ \lim_{n \to \infty} \left|\exp(z) - \left(1 + \frac{z}{n}\right)^n\right| = 0 \text{, d.h.} \]

    \[ \lim_{n \to \infty} \left(1 + \frac{z}{n}\right)^n = \exp(z) \text{, wie behauptet.} \]
\end{proof}

Eine wichtige Eigenschaft der Exponentialfunktion ist ihre Funktionalgleichung:

\begin{theorem}
    Für alle $z, w \in \mathbb{C}$ gilt $\exp(z + w) = \exp(z)\exp(w)$.
\end{theorem}

\begin{proof}
    Wir benutzen den binomischen Lehrsatz und das Cauchy-Produkt von Reihen:

    \begin{align*}
        \exp(z + w) &= \sum_{n=0}^\infty \frac{1}{n!} {(z + w)}^n \text{\hfill (Def.)}\\
        &= \sum_{n=0}^\infty \frac{1}{n!} \sum_{k=0}^n \binom{n}{k} z^{n-k}w^k \text{\hfill binomischer Satz}\\
        &= \sum_{n=0}^\infty \sum_{k=0}^\infty \frac{z^{n-k}}{(n-k)!} \frac{w^k}{k!}\\
        \text{(Cauchy-Produkt)} &= \left(\sum_{n=0}^\infty \frac{z^n}{n!}\right)\left(\sum_{m=0}^\infty \frac{w^m}{m!}\right) = \exp(z)\exp(w).
    \end{align*}
\end{proof}

\begin{deduction}
    \begin{enumerate}
        \item $\exp(z) \neq 0$ und $\exp(-z) = \frac{1}{\exp(z)}$ $\forall z \in \mathbb{C}$
        \item $\exp(r) = e^r \forall r \in \mathbb{Q}$ (erlaubt die Schreibweise $e^z$ für $\exp(z)$.)
    \end{enumerate}
\end{deduction}

\begin{proof}
    \begin{enumerate}
        \item $1 = \exp(0) = \exp(z - z) = exp(z) \cdot \exp(z)$.
        \item Für $n \in \mathbb{N}$: $\exp(n) = \exp(\underbrace{1 + \dots + 1}_{n\text{-mal}}) = {\left(\exp(1)\right)}^n = e^n$.

            \begin{align*}
                \text{Ferner für }& n, m \in \mathbb{N}: e^m = \exp(m) = \exp\left(\frac{m}{n} n\right) = \exp{\left(\frac{m}{n}\right)}^n \\
                \Rightarrow & e^{m/n} = \sqrt[n]{e^m} = \exp\left(\frac{m}{n}\right) \\
                \text{sowie } & e^{-m/n} = \frac{1}{e^{m/n}} = \frac{1}{\exp\left(\frac{m}{n}\right)} = \exp\left(- \frac{m}{n}\right).
            \end{align*}
    \end{enumerate}
\end{proof}

\begin{theorem}{(Abschätzung für den Reihenrest)}
    Es ist $\exp(z) = \sum\limits_{k=0}^n \frac{z^k}{k!} + r_{n+1}(z)$, wobei für alle $z \in \mathbb{C}$ mit $|z| \leq 1 + \frac{n}{2}$ gilt $|r_n+1 (z)| \leq \frac{2}{(n+1)!} {|z|}^{n+1}$.
\end{theorem}

\begin{proof}
    Wir haben

    \begin{align*}
        \left|r_{n+1}(z)\right| &= \left|\sum_{k=n+1}^\infty \frac{z^k}{k!} \right| = \frac{{\left| z \right|}^{n+1}}{(n+1)!} \left|\sum_{k=n+1}^\infty \frac{(n+1)!}{k!} z^{k-(n+1)}\right|\\
                                &\leq \frac{{|z|}^{n+1}}{(n+1)!} \cdot \left|\sum_{k=0}^\infty \frac{(n+1)!}{(n+1+k)!} \cdot z^k\right|
    \end{align*}

    Nun ist $|z|^k \underbrace{\leq}_{\text{Vor.}} {\left(1 + \frac{n}{2}\right)}^k = \frac{1}{2^k} {(n+2)}^k$ und daher

    \[ \frac{(n+1)!}{(n+1+k)!} {|z|}^k \leq \frac{1}{2^k} \frac{(n+2) \cdots (n+2)}{(n+2) \cdots (n+k+1)} \leq \frac{1}{2^k}. \]

    Hieraus folgt $|r_{n+1} (z)| \leq \frac{{|z|}^{n+1}}{(n+1)!} \cdot \sum\limits_{k=0}^\infty \frac{1}{2^k} = \frac{2}{(n+1)!} {|z|}^{n+1}$, wie behauptet.
\end{proof}

\begin{deduction}
    $e$ ist irrational
\end{deduction}

\begin{remark}
    $e = \frac{m}{n}$ mit $m, n \in \mathbb{N}$, $n \geq 2$ (wissen ja bereits, dass $2 < e < 3$, also $e \notin \mathbb{N}$ ist!)

    \[\Rightarrow n! \cdot e = (n-1)! \cdot m \in \mathbb{N}\]

    \[\Rightarrow \underbrace{n! \cdot \sum_{k=0}^n \frac{1}{k!}}_{\in \mathbb{N}} + \underbrace{n! \cdot \underbrace{\sum_{k=n+1}^\infty \frac{1}{k!}}_{\leq \frac{2}{(n+1)!}}}_{\leq \frac{2}{n+1} \leq \frac{2}{3} < 1} \in \mathbb{N}. \text{Widerspruch!}\]
\end{remark}


Mit Hilfe der Exponentialfunktionen definieren wir jetzt die trigonometrischen Funktionen:

\begin{definition}{Cosinus und Sinus}
    Für $z \in \mathbb{C}$ heißt

    \[\cos(z) := \frac{1}{2}(\exp(iz) + \exp(-iz))\]

    der Cosinus und

    \[ \sin(z) = \frac{1}{2i} (\exp(iz) - \exp(-iz))\]

    der Sinus von $z$.
\end{definition}

\begin{theorem}
    Für alle $z \in \mathbb{C}$ gilt:

    \begin{enumerate}
        \item $\exp(iz) = \cos(z) + i \sin(z)$ (Eulersche Formel),
        \item $\cos(z) = \sum\limits_{k=0}^\infty \frac{{(-1)}^k}{(2k)!} \cdot z^{2k}$,
        \item $\sin(z) = \sum\limits_{k=0}^\infty \frac{{(-1)}^k}{(2k+1)!} \cdot z^{2k+1}$.
    \end{enumerate}

    Insbesondere ist $\cos(z) = \cos(-z)$ und $\sin(z) = -\sin(-z)$.
\end{theorem}

\begin{proof}
    \begin{enumerate}
        \item $\cos(z) + i \sin(z) = \frac{1}{2} \left(e^{iz} + e^{-iz}\right) + \frac{1}{2} \left(e^{iz} - e^{-iz}\right) = e^{iz}$.
        \item \begin{align*}
            & \frac{1}{2} (\exp(iz) + \exp(-iz)) \\
                = & \frac{1}{2} \cdot \sum_{k=0}^{\infty} \frac{1}{k!} \left({(iz)}^k + {(-iz)}^k\right) \\
                = & \frac{1}{2} \cdot \underbrace{\sum_{k=0}^\infty}_{\text{$k$ gerade}} \frac{1}{k!} \left({(iz)}^k + {(-iz)}^k\right) = \underbrace{\sum_{k=0}^\infty}_{\text{$k$ gerade}} \frac{{(iz)}^k}{k!}\\
                (k=2e) = & \sum_{e=0}^\infty \frac{{(iz)}^{2e}}{(2e)!} = \sum_{k=0}^\infty \frac{{(-1)}^k}{(2k)!} z^2k.
        \end{align*}
    \item analog. Folgerung klar.
    \end{enumerate}
\end{proof}

Aus der Funktionalgleichung der Exponentialfunktion ergeben sich die folgenden Additionstheoreme

\begin{theorem}
    Für alle $z, w \in \mathbb{C}$ gelten:

    \begin{enumerate}[label= (\arabic*)]
        \item $\cos(z + w) = \cos(z)\cos(w) - \sin(z)\sin(w)$,
        \item $\sin(z+w) = \cos(z)\sin(w) + \sin(z)\cos(w)$.
    \end{enumerate}
\end{theorem}

\begin{proof}
    Mit Satz 3 und der Eulerschen Formel erhalten wir

    \begin{align*}
        \exp(i(z+w)) &= \exp(iz)\exp(iw) = (\cos(z) + i\sin(z))(\cos(w) + i\sin(w))\\
                     &=\cos(z)\cos(w) - \sin(z)\sin(w) + i(\sin(z)\cos(w) + \cos(z)\sin(w))
    \end{align*}

    Ersetzt man $z$ und $w$ durch $-z$ und $-w$ und beachtet $\cos(z) = \cos(-z)$, $\sin(z) = -\sin(-z)$ folgt

    \begin{align*} 
        \exp(-i(z+w)) = &\cos(z)\cos(w) - \sin(z)\sin(w)\\
                        &- i(\sin(z)\cos(w) + \cos(z)\sin(w)).
    \end{align*}

    Addition beider Gleichungen ergibt (1), Subtraktion (2).
\end{proof}

\begin{deduction}
    Für $z \in \mathbb{C}$ gelten:

    \begin{enumerate}[label= (\arabic*)]
        \item $\cos^2(z) + \sin^2(z) = 1$ (Pythagoras)
        \item $\cos(2z) = \cos^2(z) - \sin^2(z)$
        \item $\sin(2z) = 2\sin(z)\cos(z)$
    \end{enumerate}
\end{deduction}

\begin{proof}
    \begin{enumerate}[label= (\arabic*)]
        \item 
            \begin{align*}
                1 &= \exp(iz) \exp(-iz) \underbrace{=}_{\text{Euler}} (\cos(z) + i\sin(z))(\cos(z) - i\sin(z))\\
                  &= \cos^2(z) + \sin^2(z).
            \end{align*}
        \item[(2) u. (3)] $w = z$ in Satz 6.
    \end{enumerate}
\end{proof}
