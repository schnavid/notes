\section{Potenzreihen}

\begin{definition}{Potenzreihen}
    Eine Reihe der Form $P(z) = \sum\limits_{n=0}^\infty a_n(z - a)^n$ mit festem $a \in \mathbb{C}$ und einer Folge $(a_n)$ in $\mathbb{C}$ heißt eine \emph{Potenzreihe}. Der Punkt $a$ ist der \emph{Entwicklungspunkt} von $P$, die $a_n$ werden als \emph{Koeffizienten} bezeichnet. Falls $a \in \mathbb{R}$ und $a_n \in \mathbb{R}$ für alle $n \in \mathbb{N}$, nennt man $P$ eine \emph{reelle Potenzreihe}.
\end{definition}

\begin{remark}
    \begin{enumerate}
        \item Durch eine Potenzreihe wird eine Abbildung

            \[ P: \{z \in \mathbb{C} ; \sum_{n=0}^\infty a_n {(z - a)}^n \text{ konvergiert} \} \to \mathbb{C},  z \mapsto P(z) \]

            definiert.
        \item Ist $P(z)$ eine reelle Potenzreihe, so gilt $P(\overline{z}) = \overline{P(z)}$ für alle $z \in \mathbb{C}$. Insbesondere ist $P(x) \in \mathbb{R}$ für reelles $x$.
        \item Bsp: $P(z) = \sum\limits_{n=0}^\infty z^n = \frac{1}{1-z}$ (geometrische Reihe). Dies ist eine reelle Potenzreihe mit $a=0$ und $a_n = 1$ $\forall n \in \mathbb{N}$. Sie konvergiert für $z \in \mathbb{C}$ mit $|z| < 1$, der Definitionsbereich der Abbildung $P$ ist hier der Kreis mit Radius $1$ um den Entwicklungspunkt $a=0$ (Kreisrand ausgenommen!)
        \item Im folgenden (fast) immer: $a = 0$.
    \end{enumerate}
\end{remark}

Das Konvergenzverhalten von Potenzreihen wird charakterisiert durch den folgenden

\begin{theorem}
    Es sei $P(z) = \sum\limits_{n=0}^\infty a_n z^n$ eine Potenzreihe und $L = \limsup\limits_{n \to \infty} \sqrt[n]{| a_n |}$. Dann gelten:

    \begin{enumerate}[label= (\arabic*)]
        \item Ist $L=0$, so konvergiert $P(z)$ absolut für alle $z \in \mathbb{C}$
        \item Ist $0 < L < \infty$, so konvergiert $P(z)$ absolut für alle $z \in \mathbb{C}$ mit $|z| < \frac{1}{L}$ und divergiert für alle $z \in \mathbb{C}$ mit $|z| > \frac{1}{L}$.
        \item Ist $L = \infty$, so konvergiert $P(z)$ nur für $z = 0$.
    \end{enumerate}
\end{theorem}

\begin{proof}
    Es ist $\limsup\limits_{n \to \infty} \sqrt[n]{| a_n z^n |} = L * |z|$. Nach dem Wurzelkriterium konvergiert $P(z)$ absolut, falls $L |z| < 1$ ist. Dies zeigt Teil (1) und die erste Aussage in (2).

    Ferner gilt --- ebenfalls nach dem Wurzelkriterium --- dass $P(z)$ divergiert, falls $L |z| > 1$ ist. Das liefert die 2. Aussage in (2) und die Divervenzaussage in (3).

    Die Konvergenz in $z = 0$ ist trivial, da die Reihe abbricht.
\end{proof}

Die Konvergenzbereiche von potenzreigen sind also Kreise in der komplexen Evene, wobei wir die Evene selbst als ``unendlich großen'' kreis auffassen. Dies führt zu folgender Begriffsbildung:

\begin{definition}{(Konvergenzradius und -kreis)}

    Zu einer gegebenen Potenzreihe $P(z) = \sum\limits_{n=0}^\infty a_n z^n$ nennen wir

    \[ R := \sup \{ r \geq 0: P(r) \text{ konvergiert} \} \in [0, \infty] \]

    den \emph{Konvergenzradius} von $P$ und $k_R(0) := \{z \in \mathbb{C} : |z| < R\}$ den \emph{Konvergenzkreis} von $P$.
\end{definition}

Satz 1 sagt dann aus: $P(z)$ konvergiert absolut im Inneren des Konvergenzkreises und divergiert außerhalb. Den Konvergenzradius kann man berechnen nach der \emph{Formel von Cauchy-Hadamard}:

\[ R = {\left( \limsup_{n \to \infty} \sqrt[n]{| a_n |} \right)}^{-1} \]

(hierbei: $\frac{1}{\infty} = 0$, $\frac{1}{0} = \infty$.)

Über das Verhalten einer Potenzreihe am Rand des Konvergenzkreises ist keine allgemeine Aussage möglich. Dazu ein

\begin{example}
    \begin{enumerate}[label= (\roman*)]
        \item $P(z) = \sum\limits_{n=0}^\infty z^n : R=1$, Divergenz für $|z| = 1$.
        \item $P(z) = \sum\limits_{n=1}^\infty \frac{z^n}{n}$: auch hier ist $R=1$, da $\lim\limits_{n \to \infty} \sqrt[n]{n} = 1$. Divergenz für $z = 1$, Konvergenz für $z \in \mathbb{C} \setminus \{1\}$ mit $|z| = 1$, nach dem verallg. Leibniz-Kriterium.
        \item $P(z) = \sum\limits_{n=1}^\infty \frac{z^n}{n^2}: R = 1$, da $\lim\limits_{n \to \infty} \sqrt[n]{n^2} = 1$. Für $z \in \mathbb{C}$ mit $|z| = 1$ konvergiert die Reihe absolut.
    \end{enumerate}
\end{example}

In vielen Fällen läßt sich der Konvergenzradius einer Potenzreihe auch mit Hilfe des Quotientenkriteriums bestimmen:

\begin{theorem}{(Eulersche Formel für den Konvergenzradius)}
    
    Für den Konvergenzradius $R$ einer Potenzreihe $P(z) = \sum\limits_{n = 0}^\infty a_n z ^n$ gilt

    \[ R = \lim_{n \to \infty} \left| \frac{a_n}{a_{n+1}} \right| \]

    falls dieser limes (eigentlich oder uneigentlich) existiert.
\end{theorem}

\begin{proof}

    Ist $|z| < \lim\limits_{n \to \infty} \left|\frac{a_n}{a_{n+1}}\right|$, so existiert auch

    \[ \lim\limits_{n \to \infty} \left|\frac{a_{n+1}z^{n+1}}{a_n z^n}\right| = \lim\limits_{n \to \infty} \left|\frac{a_{n+1}}{a_n}\right| \cdot |z| < 1 .\]

    Also existiert ein $n_0 \in \mathbb{N}$, sodass

    \[ \left|\frac{a_{n+1} z^{n+1}}{a_n z^n}\right| > 1 \forall n \in \mathbb{N}_0 .\]

    Also ist ${(a_n z^n)}_{n \in \mathbb{N}}$ keine Nullfolge und somit $P(z)$ divergent.
\end{proof}
