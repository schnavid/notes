\section{Die Körperaxiome}

\begin{definition}
  $M$ sei eine Menge. Eine Abbildung $\circ : M \times M \to M$, $(m_{1}, m_{2}) \mapsto m_{1} \circ m_{2}$ heißt eine \emph{innere Verknüpfung} von $M$.
\end{definition}

\begin{example}
  Die Addition $+ : \mathbb{N} \times \mathbb{N} \to \mathbb{N}$, $(n, m) \mapsto n + m$ und die Multiplikation $* : \mathbb{N} \times \mathbb{N} \to \mathbb{N}$, $(n, m) \mapsto n * m$ sind innere Verknüpfungen von $\mathbb{N}$. Desgleichen für $\mathbb{Z}, \mathbb{Q}, \mathbb{R}$.
\end{example}

\begin{definition}
  Ein Paar $(G, \circ)$ aus einer Menge $G \neq \emptyset$ und einer inneren Verknüpfung $\circ$ von G heißt eine \emph{Gruppe}, falls gilt

  \begin{enumerate}
    \item[(G1)] $(a \circ b) \circ c = a \circ (b \circ c)$ für alle $a, b, c \in G$ \hfill (Assoziativität).
    \item[(G2)] Es gibt $e \in G$ mit $e \circ a = a$ für alle $a zin G$ \hfill (neutrrales Element).
    \item[(G3)] Zu jedem $a \in G$ existiert ein $b \in G$ mit $b \circ a = e$ \hfill (inverses Element).
  \end{enumerate}

  Eine Gruppe heißt \emph{kommutativ} (oder \emph{abelsch}), falls zusätzlich gilt

  \begin{enumerate}
    \item[(G4)] $a \circ b = b \circ a$ für alle $a, b \in G$.
  \end{enumerate}
\end{definition}

\begin{remark}
  \begin{enumerate}
    \item[($i$)] abkürzende Schreibweise: $G$ statt $(G, \circ)$ und $ab$ statt $a \circ b$ (wenn klar ist, welche innere Verknüpfung $\circ$ gemeint ist).
    \item[($ii$)] Für die Analysis I sind fast ausschließlich die abelschen Gruppen von Belang.
    \item[($iii$)] $(\mathbb{Z}, +)$, $(\mathbb{Q}, +)$, $(\mathbb{R}, +)$ sind abelsche Gruppen; $(\mathbb{N}, +)$ nicht, hier fehlen das neutrale und folglich auch die inversen Elemente.
    \item[($iv$)] $(\mathbb{Q}^{*}, *)$, $(\mathbb{R}^{*}, *)$\footnote{allgemein: $M^{*} = M \setminus \{0\}$, wenn $M$ eine Menge ist, die eine Null enthält.} sind ebenfalls abelsche Gruppen. $(\mathbb{Z}^{*}, *)$ hingegen nicht --- wieder fehlen die Inversen.
  \end{enumerate}
\end{remark}

\begin{lemma}\label{lemma1}
  Es sei $G$ eine abelsche Gruppe. Dann gelten:

  \begin{enumerate}
    \item Das neutrale Element $e$ ist eindeutig bestimmt.
    \item Zu $a \in G$ existiert genau ein $b \in G$ mit $ab = ba = e$. Dieses wird mit $a^{-1}$ bezeichnet (Eindeutigkeit des Inversen).
    \item Für alle $a, b \in G$ ist $(ab)^{-1} = b^{-1}a^{-1}$.
    \item $\left(a^{-1}\right)^{-1} = a$, $e^{-1} = e$
    \item Für $a, x, y \in G$ gilt $ax = ay \Leftrightarrow x = y$
    \item Zu $a, b \in G$ existiert genau ein $x \in G$, sodass $ax = b$. (Die Gleichung $ax = b$ ist also in abelschen Gruppe stets eindeutig lösbar.)
  \end{enumerate}
\end{lemma}

\begin{remark}
  Lemma gilt in vollem Umfang auch in nicht-abelschen Gruppen.
\end{remark}

\begin{proof}\hfill
  \begin{itemize}
    \item[Zu 1.] Seien $e$ und $e'$ neutrale Elemente. Dann folgt $e = e' e = e e' = e'$.
    \item[Zu 2.] Hier ist nur die Eindeutigkeit zu zeigen. Dazu seien $ba = e = b'a$. Dann ist $b = eb = b'ab = b'ba = b'e = eb' = b'$.
    \item[Zu 3.] Es gilt $\left(b^{-1}a^{-1}\right)(ab) = b^{-1}\left(a^{-1}a\right)b = b^{-1}b = e$. Damit ist $b^{-1}a^{-1}$ invers zu $ab$. Nach 2 also $b^{-1}a^{-1}={(ab)}^{-1}$.
    \item[Zu 4.] Es ist $aa^{-1} = a^{-1}a=e$, also $a$ invers zu $a^{-1} \xRightarrow[2.]{} {(a^{-1})}^{-1}=a$. Ebenso folgt aus $ee=e$, dass $e = e^{-1}$.
    \item[Zu 5.]
      \begin{itemize}
        \item[``$\Rightarrow$''] Klar (einsetzen)
        \item[``$\Leftarrow$'']\begin{align*}
            ax = ay &\Rightarrow\\
            a^{-1}(ax) = a^{-1}(ay) &\Rightarrow\\
            (a^{-1}a)x = (a^{-1}a)y &\Rightarrow\\
            ex = ey &\Rightarrow\\
            x = y
          \end{align*}
      \end{itemize}
    \item[Zu 6.] $ax = b \xLeftrightarrow[5.]{} a^{-1}ax = a^{-1}b \Leftrightarrow x = a^{-1}b$.
  \end{itemize}
\end{proof}

\begin{definition}
  Ein Tripel $(K, +, *)$ bestehend aus einer Menge $K$ und zwei inneren Verknüpfungen heißt ein \emph{Körper}, falls gilt

  \begin{enumerate}[label=(K\arabic*)]
    \item $\forall x, y, z \in K : x + (y + z) = (x + y) + z$ \hfill (Assoziativität)
    \item $\forall x, y, z \in K : x + y = y + x$ \hfill (Kommutativität)
    \item Es gibt $0 \in K$ mit $x + 0 = x$ für alle $x \in K$ \hfill (Nullelement)
    \item Zu $x \in K$ existiert $-x \in K$, sodass $x + (-x) = 0$ \hfill (Negatives)
      \smallskip
    \item $\forall x, y, z \in K : x * (y * z) = (x * y) * z$ \hfill (Assoziativität)
    \item $\forall x, y, z \in K : x * y = y * x$ \hfill (Kommutativität)
    \item Es gibt $1 \in K$ mit $1 * x = x$ für alle $x \in K$ \hfill (Einselement)
    \item Zu $x \in K^{*}$ existiert $x^{-1} \in K^{*}$, sodass $x^{-1}x = 1$ \hfill (inverses)
      \smallskip
    \item $\forall x, y, z \in K : x * (y + z) = x*y + x*z$ \hfill (Distributivgesetz)
  \end{enumerate}
\end{definition}

\begin{remark}\hfill
  \begin{enumerate}[label=(\roman*)]
    \item Schreibweise: $xy$ statt $x * y$, $x-y$ statt $x + (-y)$, falls $y \neq 0$: $\frac{x}{y}$ statt $y^{-1}x$
    \item Die Axiome (K1) bis (K4) heißen Axiome der Addition. Sie sind gleichbedeutend damit, dass $(K, +)$ eine abelsche Gruppe ist.
    \item (K5) bis (K8) sind die Axiome der Multiplikation. Sie enthalten die Aussage, dass $(K^{*}, *)$ eine abelsche Gruppe ist.
  \end{enumerate}
\end{remark}

Die einfachen Eigenschaften abelscher Gruppen (Lemma \autoref{lemma1}) haben daher in Körpern die folgenden Entsprechungen:

\begin{deduction}\label{deduction1}
  Sei $(K, +, *)$ ein Körper. Dann gelten:

  \begin{enumerate}
    \item $0$ und $1$ sind eindeutig bestimmt.
    \item Negatives ($-x$) und Inverses ($x^{-1}$) sind eindeutig bestimmt.
    \item $-(x+y) = -x - y$ und, falls $x, y \in K^{*}, (xy)^{-1} = y^{-1}x^{-1}$.
    \item $-(-x) = x$, $-0 = 0$, $\left(x^{-1}\right)^{-1} = x$, $1^{-1} = 1$.
    \item $a + x = a + y \Leftrightarrow x = y$ und, falls $a \neq 0$, $ax = ay \Leftrightarrow x = y$
    \item Die Gleichung $a + x = b$ und, falls $a \neq 0$, $ax = b$ sind eindeutig lösbar.
  \end{enumerate}
\end{deduction}

Zieht man zusätzlich das Distributivgesetz heran erhält man weitere einfache Folgerungen aus den Körperaxiomen:

\begin{lemma}
  Sei $(K, +, *)$ ein Körper und $x, y, z \in K$. Dann gelten:

  \begin{enumerate}
    \item $(x + y) z = xz + yz$,
    \item $x * 0 = 0$,
    \item $xy = 0 \Leftrightarrow x = 0 \text{ oder } y = 0$ \hfill (Nullteilerfreiheit),
    \item $(-x)y = -xy$, insbesondere $-y = (-1)y$,
    \item $(-x)(-y) = xy$.
  \end{enumerate}
\end{lemma}

\begin{proof}\hfill
  \begin{enumerate}[label=Zu \arabic*.]
    \item $(x + y)z \underset{\text{(K6)}}{=} z (x + y) \underset{\text{(K9)}}{=} zx + zy \underset{\text{(K6)}}{=} xz + yz$
    \item Aus $0 = 0 + 0$ folgt mit dem Distributivgesetz (K9) $x * 0 = x (0 + 0) \underset{\text{(K9)}}{=} x * 0 + x * 0$. Teil 5 der vorangegangenen Folgerung \autoref{deduction1} ergibt $0 = x * 0$.
    \item Sei $xy = 0$ und $x \neq 0 \xRightarrow[Folg. 1.5]{} y = x^{-1} * 0 \underset{\text{2.}}{=} 0$. Das zeigt ``$\Rightarrow$''. ``$\Leftarrow$'' folgt aus 8.
    \item $0 \underset{\text{2.}}{=} 0 * y \underset{\text{(K3, K4)}}{=} (x + (-x)) y \underset{\text{(K9)}}{=} xy + (-x)y$.

      Mit der Eindeutigkeit des Negativen folgt $(-x)y = -xy$.
    \item $(-x)(-y) \underset{\text{4}}{=} -x (-1) y \underset{\text{(K6)}}{=} -(-1)(xy) \underset{\text{4}}{=} -(-xy) \underset{\text{Folg. 1.4}}{=} xy$ \qedhere
  \end{enumerate}
\end{proof}

\begin{example}\hfill
  \begin{enumerate}[label=(\roman*)]
    \item $(\mathbb{Z}, +, *)$ ist \emph{kein} Körper, hier fehlen die inversen Elemente der Multiplikation. Nimmt man solche hinzu, entsteht der Körper $(\mathbb{Q}, +, *)$ der rationalen Zahlen.
    \item Die reellen Zahlen fassen wir auf als einen Körper $(\mathbb{R}, +, *)$, in dem noch weitere Axiome gelten (s. u.) und in dem $\mathbb{Q}$ und damit auch $\mathbb{Z}$ und $\mathbb{N}$ eingebettet sind.
    \item Endliche Körper gibt es auch (s. Algebra), ein Beispiel werden wir in den Übungen oder im Tutorium diskutieren.
    \item Ein für die Analysis sehr wichtiger Körper ist der Körper $\mathbb{C} = (\mathbb{C}, +, *)$ der komplexen Zahlen, der folgendermaßen definiert wird:
  \end{enumerate}
\end{example}

\begin{definition}
  Für $(x, y) \in \mathbb{R} \times \mathbb{R} =: \mathbb{R}^{2}$ und $(u, v) \in \mathbb{R}^{2}$ definieren

  wir \hfill $(x, y) + (u, v) := (x + y, u + v)$ \hfill (Addition)
 
  und \hfill $(x, y) (u, v) := (xu - yv, xv + yu)$ \hfill (Multiplikation)

  (Auf der rechten Seite sind $+$ und $*$ die Addition bzw. Multiplikation in den reellen Zahlen!)

  Durch diese Definitionen wird auf der Ebene $\mathbb{R}^{2}$ tatsächlich eine Körperstruktur gegeben. Genauer gilt:
\end{definition}

\begin{theorem}
  $(\mathbb{R}^{2}, +, *)$ ist ein Körper mit Nullement $0 := (0, 0)$ und Einselement $1 := (1, 0)$. Das Negative von $z = (x, y)$ ist $-z := (-x, -y)$. Für $z = (x, y) \neq 0$ ist das Inverse $z^{-1} := \frac{1}{z} := \left(\frac{x}{x^{2} + y^{2}},\frac{-y}{x^{2} + y^{2}}\right)$.
\end{theorem}

\begin{bez}
  Der auf diese Weise definierte Körper wird mit $(\mathbb{C}, +, *)$ oder kurz mit $\mathbb{C}$ bezeichnet, seine Elemente heißen \emph{komplexe Zahlen}.
\end{bez}

\begin{proof}
  Die geforderten Axiome werden auf die entsprechenden Eigenschaften der reellen Zahlen zurückgeführt. Diskussion (zum Teil) in den Übungen.
\end{proof}

\begin{remark}
  zur Einbettung von $(\mathbb{R}, +, *)$ in $(\mathbb{C}, +, *)$

  $(\{(x, 0) : x \in \mathbb{R}\}, +, *)$ ist ein Teilkörper von $(\mathbb{C}, +, *)$, denn

  \[
    (x, 0) + (u, 0) = (x + u, 0) ; (x, 0) (u, 0) = (xu, 0),
  \]

  d.h. er ist abgeschlossen unter $+$ und $*$. Durch $I : \mathbb{R} \to \{(x, 0) : x \in \mathbb{R}\}$, $x \mapsto I(x) := (x, 0)$ wird $\mathbb{R}$ isomorph auf $\{(x, 0) : x \in \mathbb{R}\}$ abgebildet. Isomorph heißt hier bijektiv und verträglich mit der Körperstruktur, d.h. $I(x + u) = I(x) + I(u)$ und $I(xu) = I(x)I(u)$. Wir nennen daher die Zahlen $(x, 0)$ reell und schreiben kurz $x$ statt $(x, 0)$.
\end{remark}

\begin{definition}
  Die Zahl $i := (0, 1)$ heißt \emph{imaginäre Einheit}.
\end{definition}

\begin{lemma}
  Es ist $i^{2} = -1$ und $(x, y) = x + iy ~\forall x,y \in \mathbb{R}$.
\end{lemma}

\begin{proof}
  $i^{2} = (0,1)(0,1) = (0-1,0+0) = (-1, 0) = -1$.

  $x +iy = (x, 0) + (0, 1)(y, 0) = (x, 0) + (0, y) = (x, y)$. \qedhere
\end{proof}

Mit dieser Schreibweise gehen $+$ und $*$ über in:

$x +iy + u + iv = (x + u) + i(y + v)$

$(x + iy)(u + iv) = (xu - yv) + i(xv + yu)$

(Man rechnet also wie im reellen und beachtet $i^{2} = -1$!)

\begin{definition}
  Es sei $z = x + iy \in \mathbb{C}$ mit $x, y \in \mathbb{R}$. Dann heißen

  \begin{itemize}[label=]
    \item Re $z := x$ der Realteil,
    \item Im $z := y$ der Imaginärteil von z und
    \item $\overline{z} := x - iy$ die zu $z$ konjugiert komplexe Zahl.
  \end{itemize}
\end{definition}

Es gelten die folgenden Rechenregeln:

\begin{lemma}
  Für $z = x + iy \in \mathbb{C}$ und $w = u + iv \in \mathbb{C}$ mit $x, y, u, v \in \mathbb{R}$ gelten:

  \begin{enumerate}
    \item $\overline{z + w} = \overline{z} + \overline{w}, \overline{zw} = \overline{z} * \overline{w}$;
    \item $\frac{1}{2} (z + \overline{z}) = x$; $\frac{1}{2i} (z - \overline{z}) = y$;
    \item $z = \overline{z} \Leftrightarrow z$ reell (wie oben erklärt);
    \item $z\overline{z} = x^{2} + y^{2}$, $z^{-1}= \frac{\overline{z}}{z\overline{z}}=\frac{x-iy}{x^{2} + y^{2}}$ ($z \neq 0$).
  \end{enumerate}

  (Beweis zur Übung empfohlen.)
\end{lemma}

Für den Rest dieses Abschnitts sei $K = (K, +, *)$ ein Körper.

\begin{remark}
  Aufgrund der Assoziativgesetze

  \hfill $x_{1} + (x_{2} + x_{3}) = (x_{1} + x_{2}) + x_{3}$ \hfill ; \hfill $x_{1} (x_{2}x_{3}) = (x_{1}x_{2})x_{3}$ \hfill ($x_{1,2,3} \in K$)

  können wir $x_{1} + x_{2} + x_{3} := (x_{1} + x_{2}) + x_{3}$ und $x_{1}x_{2}x_{3} := (x_{1}x_{2})x_{3}$ definieren. Auch für mehr als drei Summanden bzw. Faktoren sind die Ausdrücke

  \hfill $\sum\limits_{k=1}^{n}x_{k} := x_{1} + \dots + x_{n}$ und $\prod\limits_{k=1}^{n}x_{k} := x_{1} \cdots x_{n}$ \hfill ($x_{k} \in K$)

  wohldefiniert. Allgemeiner setzen wir für $n, m \in \mathbb{Z}$, $x_{k} \in K$:

  \[
    \sum_{k=m}^{n}x_{k} :=
    \begin{cases}
      x_{m} + \dots + x_{n} &, \text{ falls $n \geq m$ }\\
      0 &, \text{ falls $n < m$ (``leere Summe'')}
    \end{cases}
  \]

  und

  \[
    \prod_{k=m}^{n}x_{k} :=
    \begin{cases}
      x_{m} \cdots x_{n} &, \text{ falls $n \geq m$ }\\
      1 &, \text{ falls $n < m$ (``leeres Produkt'',)}
    \end{cases}
  \]

  wobei Null und Eins die Körperelemente sind.

  Mit diesen Bezeichnungen gelten die folgenden Verallgemeinerungen der Kommutativgesetze und des Distributivgesetzes:
\end{remark}

\begin{lemma}
  Für $m \leq k \leq n$, $m' \leq j \leq n'$ seien $x_{k}, y_{j} \in K$. Ferner sei $(i_{m}, \dots, i_{n})$ eine Umordnung von $(m, \dots, n)$. Dann gelten

  \begin{enumerate}
    \item $\sum\limits_{k=m}^{n} x_{i_{k}}= \sum\limits_{k=m}^{n} x_{k}$ und $\prod\limits_{k=m}^{n} x_{i_{k}} = \prod\limits_{k=m}^{n}x_{k}$,
    \item $\left(\sum\limits_{k=m}^{n}x_{k}\right)\left(\sum\limits_{j=m'}^{n'}y_{j}\right) = \sum\limits_{k=m}^{n}\sum\limits_{j=m'}^{n'} x_{k}y_{j}$.
  \end{enumerate}
\end{lemma}

Vielfache und Potenzen werden folgendermaßen erklärt:

\begin{definition}
  Für $n \in \mathbb{N}_{0}$ und $x \in K$ setzen wir

  \begin{align*}
    n * x := \sum_{k=1}^{n}x = \underbrace{x + \dots + x}_{\text{$n$-mal}}~&;~x^{n}:= \prod_{k=1}^{n}x = \underbrace{x \cdots x}_{\text{$n$-mal}}\\
    (-n)x := -nx  ~&;~x^{-n}=\left(x^{-1}\right)^{n} \text{ ($x \neq 0$) }
  \end{align*}
\end{definition}

\begin{remark}
  \begin{enumerate}[label=(\roman*)]
    \item $0*x=0$, $x^{0}=1$ in Übereinstimmung mit der Definition der leeren Summe beziehungsweise des leeren Produkts.
    \item $n \in K$ ist möglich (endliche Körper!), insofern lässt sich die Definition des Vielfachen nicht aus den Axiomen folgern.
    \item Es gelten die üblichen Rechenregeln:
      \begin{minipage}{0.5\linewidth}
        \begin{itemize}
          \item (n + m) x = nx + mx
          \item n (x + y) = nx + ny
          \item n (m x) = (n m) x
        \end{itemize}
      \end{minipage}%
      \begin{minipage}{0.5\linewidth}
        \begin{itemize}
          \item $x^{n+m} = x^{n}x^{m}$
          \item ${(xy)}^{n}=x^{n}y^{n}$
          \item $x^{n*m} = \left(x^{m}\right)^{n}$
        \end{itemize}
      \end{minipage}
  \end{enumerate}
\end{remark}

(Auch dies ohne Beweis.)

Es sollen nun zwei einfache, aber wichtige Identitäten gezeigt werden, die in jedem Körper gelten:

\begin{theorem}{(geometrische Summenformel)}
  Es sei $x \in K \setminus \{1\}$ und $n \in \mathbb{N}_{0}$. Dann gilt

  \[
    \sum_{k=0}^{n}x^{k}=\frac{x^{n+1}-1}{x-1}
  \]
\end{theorem}

\begin{proof}
  \begin{align*}
    (x-1) \sum_{k=0}^{n}x^{k} = x^{n+1} &+ x^{n} + \dots + x\\
                                    &- x^{n} - \dots - x - 1 = x^{n+1} - 1
  \end{align*}

  Da $x \neq 1 : \Rightarrow$ Beh.
\end{proof}

\begin{theorem}{(Binomischer Lehrsatz)}
  Es sei $x \in K$ und $n \in \mathbb{N}_{0}$. Dann gilt

  \[
    {(1+x)}^{n} = \sum_{k=0}^{n}\binom{n}{k} x^{k}
  \]
\end{theorem}

\begin{proof}
  Induktion über $n$, beginnend mit

  \begin{itemize}
    \item[$n = 0$:] ${(1+x)}^{0}=1=\sum_{k=0}^{0}\binom{0}{k}x^{0}=x^{0}$.
    \item[$n \to n+1$:]
      \begin{align*}
        {(1 + x)}^{n+1} &= {(1 + x)}^{n} + (1+x)\\
        (I.V.)~~~ &= \left(\sum_{k=0}^{n}\binom{n}{k}x^{k}\right) (1+x)\\
                        &= \sum_{k=0}^{n}\binom{n}{k}x^{k} + \sum_{k=0}^{n}\binom{n}{k}x^{k+1}\\
                        &= \sum_{k=0}^{n+1}\binom{n}{k}x^{k} + \sum_{k=1}^{n+1}\binom{n}{k-1}x^{k}\\
                        &= \sum_{k=0}^{n+1}\left(\underbrace{\binom{n}{k} + \binom{n}{k-1}}_{=\binom{n+1}{k}}\right)x^{k}\\
                        &= \sum_{k=0}^{n+1}\binom{n+1}{k}x^{k}
      \end{align*}
  \end{itemize}
\end{proof}

\newpage
\begin{deduction}\hfill
  \begin{enumerate}
    \item Für $a, b \in K$ ist $(a + b)^{n} = \sum\limits_{k=0}^{n}\binom{n}{k}a^{n-k}b^{k}$.

      Das ist klar für $a=0$ und für $a \neq 0$ haben wir:

      \begin{align*}
        {(a+b)}^{n} &= a^{n}{(1 + \frac{b}{a})}^{n}\\
                    &\underset{\text{Satz 3, } x=\frac{b}{a}}{=} a^{n} \sum_{k=0}^{n} \binom{n}{k} {\left(\frac{b}{a}\right)}^{k}\\
        &= \sum_{k=0}^{n} \binom{n}{k} a^{n-k} b^{k}
      \end{align*}
    \item $\sum\limits_{k=0}^{n} \binom{n}{k} = 2^{n}$, $\sum\limits_{k=0}^{n} \binom{n}{k} {(-1)^{k}}=
      \begin{cases}
        1 \text{ für $n=0$}\\
        0 \text{ für $n \geq 1$}
      \end{cases}$

      (Setze $x=1$ bzw. $x=-1$ in Satz 3!)
    \item $\#M = n \Rightarrow \#\mathcal{P}(M) = 2^{n}$

      \begin{align*}
        \text{Denn: } \#\mathcal{P}(M) &= \sum\limits_{k=0}^{n} \#\{N \subset M : \# N = k\} \\
        &\underset{\text{Bsp. aus 1.2}}{=} \sum_{k=0}^{n} \binom{n}{k} \underset{\text{2.}}{=} 2^{n}
      \end{align*}
  \end{enumerate}
\end{deduction}
