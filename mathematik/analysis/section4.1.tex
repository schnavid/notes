\paragraph{Bisher:}

Funktion/Abbildung: $f: X \to Y$ mit beliebigem Definitionsbereich $X$ und ebenfalls beliebigem Zielbereich $Y$.

\paragraph{Im folgenden:}

Funktionen in engerem Sinn, d.h. $X, Y \subset \mathbb{C}$

\section{Punktweise und gleichmäßige Stetigkeit: Definitionen und Folgenkriterien}

Stetigkeit ist ein zentraler Begriff der Analysis. Viele Existenzfragen lassen sich erst und allein mit Hilfe des Konzepts der Stetifkeit klären, z.B.

\begin{itemize}
    \item Existenz von Nullstellen, etwa: Gibt es $z \in \mathbb{C}$ mit $\cos(z) = 0$? oder: mit $P(z) = 0$, wenn $P$ ein Polynom höheren Grades ist?
    \item Existenz von Fixpunkten: Das sind Lösungen der Gleichung $f(z) = z$. Durch Betrachtung von $g(z) = f(z) - z$ reduziert sich das auf die Frage nach den Nullstellen.
    \item Existenz von Lösungen gewisser Optimierungsaufgaben. Die Existenz folgt in vielen Fällen aus der Stetigkeit der zu optimierenden Funktion.
\end{itemize}


\paragraph{Vorverständnis / Schulwissen}

Eine Funktion $f: \mathbb{R} \supset X \to Y \subset \mathbb{R}$ ist stetig, wenn man ihren Graphen ohne abzusetzen zeichnen kann.

Dier Begriff von Stetigkeit ist zwar richtig, aber unzureichend, z.B. für Funktionen

$f: \mathbb{C} \to \mathbb{C}$ (hier ist $G_f = \{(x, f(x)): x \in \mathbb{C}\} \subset \mathbb{C}^2 \cong \mathbb{R}^2)$,

oder $f: \mathbb{Q} \to \mathbb{R}$ (wie will man die ``Lücken'' von $\mathbb{Q}$ zeichnerisch darstellen?)

\begin{definition}{Stetigkeit}
    Eine Funktion $f: \mathbb{C} \supset X \to \mathbb{C}$ heißt \emph{stetig} im Punkt $x_0 \in X$, wenn gilt:

    \[ \forall \varepsilon > 0 \exists \delta > 0 \text{, sodass } \forall x \in X \text{ mit } |x - x_0| < \delta \text{ gilt, dass } |f(x) - f(x_0)| < \varepsilon \]

    $f$ heißt stetig in $X$, wenn $f$ in jedem $x_0 \in X$ stetig ist. Veranschaulichung im Spezialfall $x, f(x) \subset \mathbb{R}$:
\end{definition}

{\Large GRAPH}

Die definierende Eigenschaft sagt jetzt aus:

Wie schmal auch immer der $\varepsilon$-Streifen um $f(x_0)$ vorgegeben wird, so läßt sich stets ein $\delta = \delta(x_0, \varepsilon) > 0$ finden, sodass die gesamte Umgebung $(x_0 - \delta, x_o + \delta) \cap X$ von $x_0$ noch in diesem Streifen bzw.\ das Intervall $(f(x_0) - \varepsilon, f(x_0) + \varepsilon)$ abgebildet wird.

Wenn $X \subset \mathbb{R}$ ein Intervall und $f$ reellwertig ist, haben wir also Übereinstimmung mit dem ``naiven'' Begriff der Stetigkeit. Mit Hilfe des Begriffs ``Umgebung'' können wir den Stetigkeitsbegriff noch etwas prägnanter fassen:

\begin{definition}{$\varepsilon$-Umgebung}
    Es seien $\varepsilon > 0$. $X \subset \mathbb{C}$ und $x_0 \in X$. Dann heißt

    \[ U_\varepsilon (x_0) := \{ z \in X: |z - x_0| < \varepsilon \} \]

    eine \emph{$\varepsilon$-Umgebung} von $x_0$ in $X$.

    Damit lautet die definierende Eigenschaft der Stetigkeit einer Funktion $f: X \to \mathbb{C}$ in $x_0 \in X$:

    \[ \forall \varepsilon > 0 \exists \delta > 0 \text{ sodass } f(U_\delta(x_0)) \subset U_\varepsilon(f(x_0)). \]
\end{definition}

\begin{example}
    \begin{enumerate}
        \item Sind $a,b \in \mathbb{C}$ fest, so heißt $f: \mathbb{C} \to \mathbb{C}, z \mapsto f(z) = az + b$ eine affin-lineare Funktion.

            Ist jetzt $\varepsilon>0$ vorgegeben, so wählen wir $\delta = \frac{\varepsilon}{|a|}$ ($\delta > 0$ beliebig, falls $a=0$) und erhalten für $z, w \in \mathbb{C}$ mit $|z - w| < \delta$:

            \begin{align*}
                |f(z) - f(w)| &= |az + b - aw - b| = |a(z-w)|\\
                            &< |a|\delta = \varepsilon.
            \end{align*}

            $f$ ist also auf ganz $\mathbb{C}$ stetig.
        \item $f: \mathbb{C} \to \mathbb{C}: z \mapsto f(z) = z^{2}$.

            Sind jetzt $x_{0} \in \mathbb{C}$ fixiert und $\varepsilon > 0$ vorgegeben,
            so wählen wir $\delta = \min\left(1, \frac{\varepsilon}{1 + 2
                |x_{0}|}\right)$. Dann ist für $z \in \mathbb{C}$ mit $|z - x_{0}| <
            \delta$

            \begin{align*}
                \left|f(z) - f(x_{0})\right| &= |z - x_{0}| |z + x_{0}| \leq |z - x_{0}|
                                            |z-x_{0}+2x_{0}|\\
            &\leq |z - x_{0}|(1 + 2 |x_{0}|) < \delta(1 + 2 |x_{0}|) \leq \varepsilon.
            \end{align*}

            Also ist $f$ stetig in $x_{0}$.

            Beachte: Wahl von $\delta$ in (1) unabhängig von $x_{0}$. Dies ist in (2)
            nicht mehr möglih.

            Die folgenden Beispiele zeigen die Möglichkeiten einer Funktion $f$ auf, in
            einem  speziellem Punkt unstetig zu sein:
        \item $f: \mathbb{C} \to \mathbb{C}, z \mapsto f(z) = \begin{cases} 1 & \text{ für } z=0 \\ 0 & \text{ sonst} \end{cases}$, ist $x_0 = 0$ ist eine sogenannte hebbare Unstetigkeit (durch Abänderung in einem Punkt erhält man eine stetige Funktion)

        \item $f: \mathbb{R} \to \mathbb{R}, x \mapsto f(x) = \begin{cases} x & \text{ für } x \leq 0 \\ x + 1 & \text{ für } x > 0 \end{cases}$

            ``Sprungstelle'' im Nullpunkt
        \item $f: \mathbb{R} \to \mathbb{R}, x \mapsto f(x) = \begin{cases} 0 & \text{ für } x=0 \\ \sin(\frac{1}{x}) & \text{ für } x \neq 0 \end{cases}$.

            Hier liegt im Nullpunkt eine sog.\ wesentliche Unstetifkeitsstelle vor, d.h. $\lim\limits_{n \to \infty} f\left(\frac{\pm 1}{n}\right)$ existiert nicht.
        \item $f: \mathbb{C} \to \mathbb{C}, z \mapsto f(z) = \begin{cases} \frac{1}{z} & \text{ für } z \neq 0 \\ 0 & \text{ für } z=0 \end{cases}$ ist unstetig im Nullpunkt. Hier liegt eine sogenannte ``Unendlichkeitsstelle'' vor.

            Die Stetigkeit einer Funktion hängt nicht nur von der Zuordnungsvorschrift ab, sondern wird auch wesentlich bestimmt durch den Definitionsbereich. So ist z.B. \emph{jede} Funktion

            \[f: \mathbb{N} \to \mathbb{C}\]

            stetig (wähle $\delta = \frac{1}{2}$!).

            Noch überraschender ist vielleicht das folgende Bsp.

        \item $f: \mathbb{Q} \to \mathbb{C}, x \mapsto f(x) = \begin{cases} 1 & \text{ für } x > \sqrt{2} \\ 0 & \text{ für } x < \sqrt{2} \end{cases}$.

            Diese Funktion ist stetig in jedem $x_0 \in \mathbb{Q}$. Wählen wir $\delta = \abs{x_0 - \sqrt{2}}$, so ist $\abs{f(x) - f(x_0)} = 0$ für jedes $x$ mit $\abs{x - x_0} < \delta$. Die Stetigkeitsbedingung ist also erfüllt.

            Die Beispiele unstetiger Funktionen (3) bis (6) waren alle nur in einem einzelnen Punkt unstetig. Das ist keineswegs typisch.

        \item $f: \mathbb{R} \to \mathbb{R}, x \mapsto f(x) = \begin{cases} 1 & \text{ für } x \in \mathbb{Q} \\ 0 & \text{ für } x \in \mathbb{R} \setminus \mathbb{Q} \end{cases}$ ist in keinem Punkt des Definitionsbereichs stetig.

            Begründung: Ist $x_0$ fixiert und $\varepsilon = \frac{1}{2}$ vorgegeben, so befindet sich in jedem Intervall $(x_0 - \delta, x_0 + \delta)$ ein $x$ mit $\abs{f(x) - f(x_0)} = 1 > \varepsilon$.

            ($\mathbb{Q}$ und auch $\mathbb{R} \setminus \mathbb{Q}$ liegen dicht in $\mathbb{R}$!)
    \end{enumerate}
\end{example}

Kommen wir noch einmal zurück auf den Unterschied zwischen den Beispielen (1) und (2). Im ersten Fall war es möglich, $\delta$ unabhängig von $x_0$ zu wählen, sodass die charakterisierende Bedingung der Stetigkeit erfüllt ist, im zweiten Fall nicht. Dies führt zu folgender Verschärfung des Stetigkeitsbegriffs:

\begin{definition}{Gleichmäßige Stetigkeit}
    Eine Funktion $f: \mathbb{C} \supset X \to \mathbb{C}$ heißt \emph{gleichmäßig stetig}, wenn zu jedem $\varepsilon > 0$ ein $\delta > 0$ existiert, sodass $\abs{f(z) - f(w)} < \varepsilon$ gilt für alle $z, w \in X$ mit $\abs{z - w} < \delta$.
\end{definition}

Die affin-lineare Funktion aus (1) ist also gleichmäßig stetig, die auf ganz $\mathbb{C}$ definierte Funktion $f(z) = z^2$ nicht. Eine Klasse gleichmäßig stetiger Funktionen ist die folgende:

\begin{definition}
    Eine Funktion $f: \mathbb{C} \supset X \to \mathbb{C}$ heißt \emph{Lipschitz-stetig} mit Lipschitzkonstante $L$, falls für alle $z, w \in X$ die Ungleichung

    \[\abs{f(z) - f(w)} \leq L\abs{z - w}\]

    gilt.
\end{definition}

\begin{remark}
    Jede Lipschitz-stetige Funktion ist gleichmäßig stetig. Zu $\varepsilon > 0$ wählt man $\delta = \frac{\varepsilon}{L}$. Dann ist für $z, w \in X$ mit $\abs{z - w} < \delta$:

    \[\abs{f(z) - f(w)} \leq L \abs{z - w} < L \cdot \delta = \varepsilon.\]
\end{remark}

\begin{example}
    Affin-lineare Funktionen; $f(z) = \overline{z}$; $f(z) = \Re(z)$; $f(z) = \Im(z)$; $f(z) = \abs{z}$, hierfür beachte man $\abs{\abs{z} - \abs{w}} \leq \abs{z - w}$.
\end{example}

Wir können nun durch direkte Rechnung leicht einsehen, dass Polynome und Potenzreihen stetig sind (genauer gesagt: stetige Funktionen definieren).

Gleichmäßige Stetigkeit ist hier im allgemeinen nicht zu erwarten, wie das Beispiel (W) oben bereits gezeigt hat.

\begin{theorem}
    Es sei $P(z) = \sum\limits_{n=0}^\infty a_n z^n$ eine Potenzreihe mit Konvergenzradius $R$. Dann ist

    \[P: K_R(0) \to \mathbb{C}, z \mapsto P(z)\]

    stetig und die Einschränkung

    \[P {|}_{\overline{K_r(0)}} : \underbrace{\{z \in \mathbb{C} : \abs{z} \leq r\}}_{K_r(0)} \to \mathbb{C}\]

    für jedes $r \in \left[0, R\right)$ Lipschitz- und damit gleichmäßig stetig.
\end{theorem}

\begin{proof}
    Es ist nur die zweite Aussage zu zeigen. Dazu seien $r < R$ und $z, w \in \overline{K_r(0)}$ vorgegeben. Dann ist

    \begin{align*}
        P(z) - P(w) &= \sum_{n=0}^\infty a_n z^n - \sum_{n-0}^\infty a_n w^n = \sum_{n=0}^\infty a_n (z^n - w^n) \\
                    &= (z-w) \cdot \sum_{n=0}^\infty a_n \cdot \sum_{k=0}^{n-1} z^{n-1-k} w^k \quad\text{(Geometrische Summenformel!)}
    \end{align*}

    wg. $\abs{z},\abs{w} \leq r$ folgt

    \begin{align*}
        \abs{P(z) - P(w)} &\leq \abs{z-w} \cdot \sum_{n=0}^\infty \abs{a_n} \cdot \sum_{k=0}^{n-1} \abs{z^{n-1-k} w^k} \\
                          &\leq \abs{z-w} \cdot \underbrace{\sum_{n=1}^\infty \abs{a_n} n \cdot r^n}_{=: L(r) < \infty}
    \end{align*}

    da $\limsup\limits_{n \to \infty} \sqrt[n]{n \abs{a_n}} = \limsup\limits_{n \to \infty}\sqrt[n]{\abs{a_n}}$
\end{proof}

\begin{deduction}
    Die Funktionen $\exp, \sin, \cos: \mathbb{C} \to \mathbb{C}$ sind stetig, ebenso Polynome $P: \mathbb{C} \to \mathbb{C}$.
\end{deduction}

Die $\varepsilon$-$\delta$-Definition der Stetigkeit ist wichtig für Beweiszwecke und ließ sich gut handhaben bei dem Lipschitzstetigen Funktionen. Der Beweis der Stetigkeit z.B. der rationalen Funktionen mit $\varepsilon$ und $\delta$ erweist sich hingegen bereits in einfachen Fällen als recht mühsam.

\paragraph{Übung} Man zeige mit der $\varepsilon$-$\delta$-Definition, dass $f: \mathbb{C}^* \to \mathbb{C}, z \mapsto f(z) = \frac{1}{z}$ stetig, aber nicht gleichmäßig stetig ist!

In solchen Fällen ist es einfacher, das nachstehende Folgenkriterium für die Stetigkeit zu verwenden.

\begin{theorem}
    Eine Funktion $f: \mathbb{C} \supset X \to \mathbb{C}$ ist genau dann stetig in $x_0 \in X$, wenn für alle Folgen $(x_n)$ in $X$ mit $\lim\limits_{n \to \infty} = x_0$ gilt, dass auch

    \[\lim\limits_{n \to \infty} f(x_n) = f(x_0).\]
\end{theorem}

\begin{proof}
    ``$\Rightarrow$'' Sei $f$ stetig in $x_0$ und $\varepsilon > 0$ vorgegeben. Dann existiert $\delta > 0$ mit $\abs{f(x) - f(x_0)} < \varepsilon$ für alle $x \in X$ mit $\abs{x - x_0} < \delta$. Ist dann $(x_n)$ eine Folge in $X$ mit $\lim\limits_{n \to \infty} x_n = x_0$, so existiert $N = N(\delta) \in \mathbb{N}$, sodass $\abs{x_n - x_0} < \delta \forall n \geq N$. Für $n \geq N$ gilt demnach $\abs{f(x_n) - f(x_0)} < \varepsilon$, also $\lim\limits_{n \to \infty} f(x_n) = f(x_0)$.

    ``$\Leftarrow$'' Ist $f$ unstetig in $x_0 \in X$, so gilt $\exists \varepsilon_0 > 0$, sodass $\forall \delta < 0$ ein $x = x(\delta) \in X$ existiert mit $\abs{x - x_0} < \delta$ und $\abs{f(x) - f(x_0)} \geq \varepsilon_0$. Setzen wir $\delta = \frac{1}{n}$, so erhalten wir eine Folge $(x_n)$ in $X$ mit $\abs{x_n - x_0} < \frac{1}{n}$ und $\abs{f(x_n) - f(x_0)} \geq \varepsilon_0$. Also $\lim\limits_{n \to \infty} x_n = x_0$ aber $(f(x_n))_n$ konvergiert nicht gegen $f(x_0)$.
\end{proof}

Aus den Rechenregeln für Grenzwerte ergibt sich:

\begin{deduction}
    \begin{enumerate}[label=(\roman*)]
        \item Sind $f,g: X \to \mathbb{C}$ stetig, $\lambda, \mu \in \mathbb{C}$, so sind auch $\lambda f + \mu g$ uns $f \cdot g$ stetig.

        \item Sind $f, g: X \to \mathbb{C}$ stetig, $N = \{z \in X: g(z) = 0\}$, so ist auch $\frac{f}{g} : X \setminus N \to \mathbb{C}$ stetig. Insbesondere sind alle rationalen Funktionen stetig in ihrem Definitionsbereich.

        \item Sind $f: X \to \mathbb{C}$ und $g: \widetilde{X} \to \mathbb{C}$ stetige Funktionen, sodass $g(\widetilde{X}) \subset X$ ist, so ist auch

            \[f \circ g: \widetilde{X} \to \mathbb{C}\]

        stetig.
    \end{enumerate}
\end{deduction}

Ebenso wie im Fall der Stetigkeit gibt es ein Folgenkriterium, welches die Überprüfung der gleichmäßigen Stetigkeit erleichtert:

\begin{theorem}
    Eine Funktion $f: \mathbb{C} \supset X \to \mathbb{C}$ ist genau dann gleichmäßig stetig, wenn für alle Folgenpaare $(x_n)$ und $(y_n)$ in $X$ mit $\lim\limits_{n \to \infty} (x_n - y_n) = 0$ gilt, dass $\lim\limits_{n \to \infty} f(x_n) - f(y_n) = 0$.
\end{theorem}

\begin{proof}
    ``$\Rightarrow$'' Sei $f$ gleichmäßig stetig und $\varepsilon > 0$ vorgegeben. Dann existiert $\delta > 0$, sodass $\abs{f(z) - f(w)} < \varepsilon$ für alle $z, w \in X$ mit $\abs{z - w} < \delta$. Ferner existiert zu jedem Folgenpaar $(x_n)$, $(y_n)$ wie oben ein $N = N(\delta) \in \mathbb{N}$, sodass $\abs{x_n - y_n} < \delta$ für alle $n \geq N$. Für diese $n$ ist dann auch $\abs{f(x_n) - f(y_n)} < \varepsilon$.

    ``$\Leftarrow$'' Ist $f$ nicht gleichmäßig stetig, so gilt: $\exists \varepsilon_0 > 0$, sodass $\forall \delta > 0$ ein $x = x(\delta)$ und ein $y = y(\delta)$ existieren mit $\abs{x - y} < \delta$ und $\abs{f(x) - f(y)} \geq \varepsilon_0$ mit $\delta = \frac{1}{n}$ erhalten wir Folgen $(x_n)$ und $(y_n)$, sodass $\abs{x_n - y_n} < \frac{1}{n}$, aber $\abs{f(x_n) - f(y_n)} \geq \varepsilon_0$. Letzteres bedeutet, dass $(f(x_n) - f(y_n))_n$ nicht gegen Null konvergiert.
\end{proof}

\begin{application}
    \begin{enumerate}
        \item $f: \mathbb{C} \setminus \{0\} \to \mathbb{C}, z \mapsto \frac{1}{z}$ ist nicht gleichmäßig stetig.

            Zum Nachweis wählen wir die Folgenpaare $x_n = \frac{1}{n}$ und $y_n = \frac{1}{2n}$, sodass $x_n - y_n = \frac{1}{2n} \to 0 (n \to \infty)$. Andererseits ist $f(x_n) - f(y_n) = n - 2n = -n \to - \infty (n \to \infty)$.

            Satz 3 liefert also die Behauptung.
        \item $f: \mathbb{Q} \to \mathbb{R}, f(x) = \begin{cases} 1 & \text{ für } x > \sqrt{2} \\ 0 & \text{ für } x < \sqrt{2} \end{cases}$

            Dazu wählen wir Folgen $(x_n)$, $(y_n)$ in $\mathbb{Q}$ mit $\lim\limits_{n \to \infty} x_n = \sqrt{2} = \lim\limits_{n \to \infty} y_n$ und $x_n < \sqrt{2} < y_n$.

            Dafür gilt $f(y_n) - f(x_n) = 1$, also auch $\lim\limits_{n \to \infty} f(y_n) - f(x_n) = 1 \neq 0$, $f$ ist also nach Satz 3 nicht gleichmäßig stetig.
    \end{enumerate}

    Diese Beispiele zeigen, dass die gleichmäßige Stetigkeit im allgemeinen eine echt stärkere Eigenschaft ist als die bloße Stetigkeit. Unter einer bestimmten Zusatzvoraussetzung an den Definitionsbereich fallen jdeoch beide Begriffe zusammen:
\end{application}

\begin{definition}{(Abgeschlossenheit, Kompaktheit)}
    \begin{enumerate}
        \item Eine Teilmenge $A \subset \mathbb{C}$ heißt \emph{abgeschlossen}, falls für jede konvergente Folge $(x_n)$ in $A$ mit $\lim\limits_{n \to \infty} x_n = a \in \mathbb{C}$ bereits gilt, dass $a \in A$.
        
        \item Eine Teilmenge $K \subset \mathbb{C}$ heißt \emph{kompakt}, wenn sie beschränkt und abgeschlossen ist.

            Beispiel: $\overline{K_r(a)} := \{ z \in \mathbb{C} : \abs{z - a} \leq r\}$ und $[a, b]$ sind kompakt. Hingegen ist $[a, \infty)$ zwar abgeschlossen, aber nicht beschränkt und daher auch nicht kompakt.
    \end{enumerate}
\end{definition}

\begin{theorem}
    Es sei $K \subset \mathbb{C}$ kompakt und $f: K \to \mathbb{C}$ stetig. Dann
    ist $f$ bereits gleichmäßig stetig.
\end{theorem}

\begin{proof}
    Nehmen wir an, $f$ sei nicht gleichmäßig stetig, so existiert ein Folgenpaar
    $(x_{n})$, $(y_{n})$ mit $\lim\limits_{n \to \infty} x_{n} y_{n} = 0$ und
    $\limsup\limits_{n \to \infty} \abs{f(x_{n}) - f(y_{n})} > 0$. Für eine
    Teilfolge, die wieder mit $(x_{n})$ bzw $(y_{n})$ bezeichnet sei, bedeutet
    dies: Es existiert $\varepsilon_{0} > 0$, sodass $\abs{f(x_{n}) - f(y_{n})}
    \geq \varepsilon_{0} \forall n$ und $\lim\limits_{n \to \infty} x_{n} -
    y_{n} = 0$.

    Da $K$ kompakt: $\exists \text{TF} (x_{n_{k}}), (y_{n_{k}})$ und $a \in K$
    mit $\lim\limits_{k \to \infty} x_{n_{k}} = \lim_{k \to \infty} y_{n_{k}} =
    a$.

    Mit der Stetigkeit von $f$ folgt

    \[ 0 = \abs{f(a) - f(a)} = \lim\limits_{k \to \infty} \abs{f(x_{n_{k}}) - f(y_{n_{k}})},\]

    im Widerspruch zu $\abs{f(x_{n}) - f(y_{n})} \geq \varepsilon_{0} \forall n
    \in \mathbb{N}$.
\end{proof}

Stetige Funktionen mit kompaktem Definitionsbereich haben darüber hinaus die
folgende Eigenschaft:

\begin{theorem}
  Es sei $K \subset \mathbb{C}$ kompakt und $f: K \to \mathbb{C}$ stetig. Dann
  ist auch $f(K)$ kompakt.
\end{theorem}

\begin{proof}
    Sei $(y_{n})$ eine Folge in $f(K)$. Dann existiert eine Folge $(x_{n})$ in
    $K$ mit $f(x_{n}) = y_{n}$. Da $K$ beschränkt ist, existiert nach
    Bolzano-Weierstraß eine Teilfolge $(x_{n_{k}})_{k}$ von $(x_{n})$ und ein
    $x_{0} \in \mathbb{C}$ mit $\lim\limits_{k \to \infty} x_{n_{k}} = x_{0} \in
    \mathbb{C}$. Da $K$ abgeschlossen ist, gilt sogar $x_{0} \in K$. Mit der
    Stetigkeit von $f$ folgt $\lim\limits_{k \to \infty} y_{n_{k}} =
    \lim\limits_{k \to \infty} f(x_{n_{k}}) = f(x_{0}) =: y_{0} \in f(K)$. Die
    Folge $(y_{n})$ besitzt also eine in $f(K)$ konvergente Teilfolge.

    Abgeschlossenheit: Gilt $\lim\limits_{n \to \infty} y_{n} = y \in
    \mathbb{C}$, so ist $y = \lim\limits_{k \to \infty} y_{n_{k}} = y_{0} \in
    f(K)$.

    Beschränktheit: Wäre $f(K)$ unbeschränkt, gäbe es eine Folge $(y_{n})$ in
    $f(K)$ mit $\abs{y_{n} - y_{m}} \geq 1 \forall n, m \in \mathbb{N}$ mit $n
    \neq m$. Widerspruch zur Existenz einer konvergenten Teilfolge.
\end{proof}
