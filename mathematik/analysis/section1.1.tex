\section{Mengen und Abbildungen}

Von fundamentaler Bedeutung sowohl für die Analysis wie auch für die Algebra ist der Begriff der Menge. Er wurde 1895 von Georg Cantor folgendermaßen eingeführt:

\begin{definition}{(Menge, Element)}
  ``Unter einer Menge verstehen wir jede Zusammenfassung $M$ von bestimmten wohlunterschiedenen Objekten in unserer Anschauung oder unseres Denkens (welche die Elemente von $M$ genannt werden) zu einem Ganzen.'' \cite{Cantor:1895}
\end{definition}

Dies ist keine exakte Definition, denn sie enthält Begriffe wie ``Zusammenfassung'' und ``Objekt'', die nicht genauer festgelegt sind. Ferner führt sie zu Widersprüchen, wie wir bald anhand eines einfachen Beispiels sehen werden. Um zumindest die größten Schwierigkeiten aus dem Weg zu räumen, fügen wir hinzu:

\begin{addition}
  Hierbei muß prinzipiell entscheidbar sein, ob ein Element zu einer Menge $M$ gehört oder nicht.
\end{addition}

\begin{bez}
  Wir schreiben $m \in M$ (gelesen: ``$m$ Element $M$''), wenn das Objekt $m$ zur Menge $M$ gehört, anderenfalls $m \notin M$.
\end{bez}

Es gibt zwei Möglichkeiten der Beschreibung von Mengen:

\paragraph{1. Durch Aufzählung}

\[
  M_1 = \{ 2, 4, 6, 8 \} ~~ oder ~~ M_2 = \{ 7, X, \Delta, ! \}
\]

es können also durchaus verschiedenartige Objekte zu einer Menge zusammen\-ge\-fasst werden. Auf die Reihenfolge kommt es hierbei nicht an und in der Regel wird jedes Objekt nur einmal genannt (beachte: ``wohlunterschiedlich'' in Cantors Definition!). Wir haben also

\[
  M_1 = \{ 4, 8, 6, 2 \} = \{ 2, 4, 2, 6, 8 \}
\]

Diese Art der Beschreibung ist in erster Linie geeignet für endliche Mengen, das sind Mengen mit endlich vielen Elementen. Aber auch

\[
  M_3 = \{ 2, 4, 6, 8, \dots \}
\]

ist eine zulässige Beschreibung der Menge aller geraden natürlichen Zahlen. Es kommt dabei darauf an, dass ein Bildungsgesetz eindeutig erkennbar ist.

\paragraph{2. Durch Angabe einer chakterisierenden Eigenschaft E(x)}

\[
  \text{allgemein in der Form}~M = \{ x : E(x) \},
\]

was man lesen würde als ``Menge aller Objekte x mit der Eigenschaft E''. Z.B. haben wir

\[
  M_1 = \{ x : \text{$x$ ist eine gerade natürliche Zahl, die kleiner ist als 10} \}
\]

oder

\[
  M_3 = \{ x : \text{$x$ ist eine gerade natürliche Zahl} \}.
\]

\emph{Vorsicht!} Nicht jede Angabe einer charakterisierenden Eigenschaft führt zu einer wohldefinierten Menge.

Bsp. (``Russel'sche Unmenge''): Die Menge

\[
  M_R := \{ x: \text{$x$ ist Menge und $x \notin x$} \}
\]

aller Mengen, die sich selbst als Element nicht enthalten ist zweifellos eine ``Zusammenfassung von Objekten unseres Denkens'', genügt also der Cantor\-'schen Mengen\-definition. Aber: Gehört $M_R$ als Element zu $M_R$, d.h. Gilt $M_R \in M_R$?

Nehmen wir dies an, folgt sofort das Gegenteil:

\[
  M_R \in M_R = \{ x : x \notin x \} \Rightarrow M_R \notin M_R
\]

\[
  \text{Umgekehrt:}~ M_R \notin M_R = \{ x: x \notin x \} \Rightarrow M_R \in M_R.
\]

Die Frage ``$M_R \in M_R$?'' ist also \emph{nicht entscheidbar}.

Dies motiviert den oben formulierten Zusatz zur Cantor'schen Definition.

Hingegen ist

\[
  M_4 = \{ x : \text{$x$ ist eine Primzahl} \}
\]

eine wohldefinierte Menge, auch wenn heute (noch) niemand entscheiden kann, ob z.B. $2^{99991}-1$ eine Primzahl ist oder nicht. Es ist prinzipiell entscheidbar, $M_4$ genügt damit dem Zusatz zur Definition. Die Menge aller Primzahlen ist wohldefiniert.

Das Beispiel der Russel'schen Unmenge zeigt: Probleme mit dem sogenannten ``naiven'' Cantor'schen Mengenbegriff treten dann auf, wenn man Mengen von Mengen untersucht. Hat man es mit Mengen gleichartiger Objekte (z.B. mit Zahlenmengen) zu tun, so treten derartige Schwierigkeiten nicht auf.

Beispiele von Zahlenmengen, die in der Analysis \RN{1} von Bedeutung sind:

\begin{itemize}
\item $\mathbb{N}= \{1,2,3,\dots\}$ \hfill (natürliche Zahlen)
\item $\mathbb{N}_0= \{0,1,2,\dots\}$ \hfill (natürliche Zahlen mit 0)
\item $\mathbb{Z} = \{0, \pm 1, \pm 2, \dots\}$ \hfill (ganze Zahlen)
\item $\mathbb{Q} = \{\frac{p}{q} : p \in \mathbb{Z}, q \in \mathbb{N} \}$ \hfill (rationale Zahlen)
\item $\mathbb{R} = \{a + \frac{a_{1}}{10^{1}} + \frac{a_{2}}{10^{2}} + \dots : a \in \mathbb{Z}, a_1, a_2,\dots \in \{0,\dots,9 \} \}$ \hfill (reelle Zahlen)
\end{itemize}

Intervalle: Für $a,b\in\mathbb{R}$ mit $a<b$ definiert man

\vspace{1em}\hspace{1em} $[a,b] := \{x \in \mathbb{R} : a \leq x \leq b\}$ \hfill (abgeschlossen)

\vspace{1em}\hspace{1em} $(a,b) = ~]a,b[~ := \{x \in \mathbb{R} : a < x < b \}$ \hfill (offen)

\vspace{1em}\hspace{1em} $\left.
\begin{tabular}{@{}c@{}}
$\left[a,b\right) := \{ x \in \mathbb{R} : a \leq x < b \}$ \\
$\left(a,b\right] := \{ x \in \mathbb{R} : a < x \leq b$
\end{tabular}
\hspace{0.5em}\right\}$
\hfill (halboffen)

\vspace{1em}sowie uneigentliche ($\estimate$ ``unendlich ausgedehnte'') Intervalle wie z.B.

\vspace{1em}\hspace{1em} $\left[a,\infty\right):=\{ x \in \mathbb{R} : x \geq a \}$\hspace{1em}oder

\vspace{1em}\hspace{1em} $(-\infty,b) := \{ x \in \mathbb{R} : x < b \}$

\begin{remark}\hfill
  \begin{enumerate}
  \item[$i$] Das Symbol $\infty$ für ``unendlich'' ist \emph{keine reelle Zahl}! Die Rechenregeln für reelle Zahlen sind für dieses Symbol \emph{nicht} anwendbar!
  \item[$ii$.] Die Ordnungsrelationen $ < $ und $\leq$ zwischen zwei reellen Zahlen werden wir später genauer fassen.
  \end{enumerate}
\end{remark}

Bei dieser Auszählung von Zahlenmengen, insbesondere bei den Intervallen haben wir in gewisser Weise den folgenden Begriff der Teilmenge vorweggenommen:

\begin{definition}{Teilmenge, Mengengleichheit und leere Menge}
  \begin{enumerate}
  \item[($i$)] Eine Menge $M_1$ heißt Teilmenge einer Menge $M_2$, falls alle Elemente von $M_1$ in $M_2$ enthalten sind.

    \[
      \text{Schreibweise: } M_1\subset M_2,~\text{falls gilt: } x \in M_1 \Rightarrow x \in M_2
    \]
  \item[($ii$)] Zwei Mengen $M_1$ und $M_2$ heißen gleich, wenn sie die gleichen Elemente enthalten, Kurz:

    \[
      M_1 = M_2 :\iff M_1 \subset M_2~\text{und}~M_2 \subset M_1
    \]
    
  \item[($iii$)] Die leere Menge ist diejenige Menge, welche kein Element enthält. Sie wird mit $\emptyset$ (oder $\{~\}$) bezeichnet.
  \end{enumerate}
\end{definition}

\begin{remark}
  Für jede Menge $M$ gilt $\emptyset \subset M$.
\end{remark}

\begin{definition}{Mengenverknüpfungen}
  $M_1$ und $M_2$ seien Mengen.

  Dann heißen:

  \begin{enumerate}
  \item[($i$)] $M_1 \cup M_2 := \{ x : x \in M_1 \text{ oder } x \in M_2 \}$ die \emph{Vereinigung}
  \item[($ii$)] $M_1 \cap M_2 := \{ x : x \in M_1 \text{ und } x \in M_2 \}$ der \emph{(Durch-)Schnitt}
  \item[($iii$)] $M_1 \setminus M_2 := \{ x : x \in M_1 \text{ und } x \notin M_2 \}$ die \emph{Differenz}
  \item[($iv$)] $M_1 \Delta M_2 := (M_1 \setminus M_2) \cup (M_2 \setminus M_1)$ die \emph{symmetrische Differenz}
  \end{enumerate}

  der Mengen $M_1$ und $M_2$.
\end{definition}

\begin{remark}
  Ohne den Begriff der leeren Menge hätten wir den Durchschnitt $M_1 \cap M_2$ nicht für beliebige Mengen $M_1$ und $M_2$ definieren können. Zwei Mengen mit der Eigenschaft $M_1 \cap M_2 = \emptyset$ nennt man \emph{disjunkt}.
\end{remark}

Diese Mengenoperationen können durch sogenannte \emph{Venn-Diagramme} veranschaulicht werden:

\begin{center}
  \begin{tikzpicture}
    \fill[darkgray] (3.0, 1.0) circle [radius=0.75];
    \fill[darkgray] (4.0, 1.0) circle [radius=0.75];
    \draw[black] (3.0, 1.0) circle [radius=0.75];
    \draw[black] (4.0, 1.0) circle [radius=0.75];
    \node at (0.2, 1.0) {($i$) $M_1 \cup M_2$};
  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}
    \begin{scope}
      \clip (3.0, 1.0) circle [radius=0.75];
      \fill[darkgray] (4.0, 1.0) circle [radius=0.75];
    \end{scope}
    \draw[black] (3.0, 1.0) circle [radius=0.75];
    \draw[black] (4.0, 1.0) circle [radius=0.75];
    \node at (0.2, 1.0) {($ii$) $M_1 \cap M_2$};
  \end{tikzpicture}
  \vskip 2em
  \begin{tikzpicture}
    \begin{scope}
      \clip (3.0, 1.0) circle [radius=0.75];
      \fill[darkgray, even odd rule] (3.0, 1.0) circle [radius=0.75]
                                     (4.0, 1.0) circle [radius=0.75];
    \end{scope}
    \draw[black] (3.0, 1.0) circle [radius=0.75];
    \draw[black] (4.0, 1.0) circle [radius=0.75];
    \node at (0.2, 1.0) {($iii$) $M_1 \setminus M_2$};
  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}
    \fill[darkgray, even odd rule] (3.0, 1.0) circle [radius=0.75]
                                   (4.0, 1.0) circle [radius=0.75];
    \draw[black] (3.0, 1.0) circle [radius=0.75];
    \draw[black] (4.0, 1.0) circle [radius=0.75];
    \node at (0.2, 1.0) {($iv$) $M_1 \Delta M_2$};
  \end{tikzpicture}
\end{center}

Solche Diagramme sind oft nützlich, haben aber keine Beweiskraft.

\begin{theorem}{Rechenregeln für Mengenverknüpfungen}
  $M_1$, $M_2$ und $M_3$ seien Mengen. Dann gelten:

  \begin{enumerate}
  \item[($i$)] $M_1 \cup M_2 = M_2 \cup M_1;~ M_1 \cap M_2 = M_2 \cap M_1$ \hfill (Kommutativität) 
  \item[($ii$)] $M_1 \cup (M_2 \cup M_3) = (M_1 \cup M_2) \cup M_3$, desgl. für $\cap$ \hfill (Assoziativität)
  \item[($iii$)] $\left.
    \begin{tabular}{@{}c@{}}
    \( M_1 \cap ( M_2 \cup M_3 ) = (M_1 \cap M_2) \cup (M_1 \cap M_3) \) \\
    \( M_1 \cup (M_2 \cap M_3) = (M_1 \cup M_2) \cap (M_1 \cup M_3) \)
    \end{tabular}
    \hspace{0.5em}\right\}$
    \hfill (Distributivität)

  \end{enumerate}
\end{theorem}

Exemplarisch soll der Beweis des ersten Distributivgesetzes durchgeführt werden:

\begin{proof}
\begin{align*}
  x \in M_{1} \cap ( M_{2} \cup M_{3} ) & \Leftrightarrow x \in M_{1} ~und~ x \in M_{2} \cup M_{3} \\
  & \Leftrightarrow x \in M_{1} ~und~ ( x \in M_{2} ~oder~ x \in M_{3} ) \\
  & \Leftrightarrow (x \in M_{1} ~und~ x \in M_{2}) ~oder~ ( x \in M_{1} ~und~ x \in M_{3} ) \\
  & \Leftrightarrow x \in M_{1} \cap M_{2} ~oder~ x \in M_{1} \cap M_{3} \\
  & \Leftrightarrow x \in (M_{1} \cap M_{2}) \cup (M_{1} \cap M_{3})
\end{align*}

Also enthalten $M_{1} \cap (M_{2} \cup M_{3})$ und $(M_{1} \cap M_{2}) \cup (M_{1} \cap M_{3})$ dieselben Elemente und sind daher gleich.
\end{proof}

Neben Mengen von Zahlen bzw. allgemeiner Mengen gleichartiger Objekte aus einer wohldefinierten Grundmenge werden wir aber bereits in der Analysis I Mengen von Mengen betrachten.

\begin{enumerate}
  \item[($i$)] Approximation einer reellen Zahl $x$ durch eine Folge von Intervallen: $I_{1} \supset I_{2} \supset I_{3} \supset \dots \ni x$.

    Hier betrachtet man also eine Menge von Intervallen. Wenn im Durchschnitt aller Intervalle nur die Zahl $x$ übrig bleibt, spricht man von einer \emph{``Intervallschachtelung''}.

  \item[($ii$)] Approximative Berechnung von Flächen bei der Integration

    \begin{tikzpicture}
      \draw[->] (-0.2,0) -- (4,0) node[right] {$x$};
      \draw[->] (0,-0.2) -- (0,3) node[above] {$y$};
      \draw[scale=0.1,domain=0:40,smooth,variable=\x,black] plot ({\x},{1.5 * sin(8 * \x) + 20});
    \end{tikzpicture}
    \begin{minipage}[b]{0.25\linewidth}
      \begin{center}
        wird approximiert durch endliche Vereinigung von Rechtecken
      \end{center}
    \end{minipage}
    \begin{tikzpicture}
      \draw[->] (-0.2,0) -- (4,0) node[right] {$x$};
      \draw[->] (0,-0.2) -- (0,3) node[above] {$y$};
      \draw[scale=0.1,domain=0:40,smooth,variable=\x,black] plot ({\x},{1.5 * sin(8 * \x) + 20});
      \node[text width=7.5em] at (2, 1.5) {Bitte vorstellen, dass hier eine Approximation des Integrals dieser Funktion mittels Rechtecken eingezeichnet ist...};
    \end{tikzpicture}
\end{enumerate}

Auch hier werden Mengen von Mengen (bzw. Folgen von Mengen) zur Näherungsweisen Berechnung benutzt. Wir haben anhand der Russell'schen Unmenge gesehen, dass gerade solche Mengen von Mengen zu Widersprüchen führen können!

\emph{Ausweg}: Man zeichnet eine wohldefinierte Grundmenge $X$ aus und betrachtet Mengen von Teilmengen $X$, sog. Mengensysteme.

\begin{definition}{(Potenzmenge, Mengensystem)}
  Es sei $X$ eine Menge. Dann heißt (die Menge aller Teilmengen von $X$)

  \[
    \mathcal{P}(X) := \{ M : M \subset X \}
  \]

  die \emph{Potenzmenge} von $X$. Eine Teilmenge $\mathcal{M}\subset\mathcal{P}(X),~\mathcal{M}\neq\emptyset$ heißt ein \emph{Mengensystem} auf $X$.
\end{definition}

\paragraph{Bsp.}

\begin{enumerate}
  \item[($i$)] $X = \{1,2,3\}$. Dann ist $\mathcal{P}(X) = \{\emptyset,\{1\},\{2\},\{3\},\{1,2\},\{2,3\},\{3,1\},\{1,2,3\}\}$.
  \item[($ii$)] $X=\emptyset \Rightarrow \mathcal{P}(X) = \{\emptyset\}~ (\neq \emptyset!)$
\end{enumerate}

\emph{Übung}: Wieviele Elemente hat $\mathcal{P}(X)$, wenn $X$ $N$ Elemente besitzt? - Diese Frage werden wir in einer der nächsten Sitzungen beantworten können.

Wenn eine Grundmenge ausgezeichnet ist, kann man die Komplemente ihrer Teilmengen definieren:

\begin{definition}
  $X$ sei eine Menge und $M \subset X$ eine Teilmenge. Dann heißt $M^{C} := X \setminus M$ das \emph{Komplement von $M$ in $X$}.
\end{definition}

Bisher haben wir nur endliche Vereinigungen und Durchschnitte betrachtet. Dies können wir jetzt in der folgenen Weise verallgemeinern:

\begin{definition}
  Es seien $X$ eine Menge und $\mathcal{M}$ ein Mengensystem auf $X$. Dann setzen wir

  \[
    \bigcup_{M \in \mathcal{M}}M := \{x \in X: \text{ es gibt ein } M \in \mathcal{M} \text{ mit } x \in M\}
  \]

  und

  \[
    \bigcap_{M \in \mathcal{M}}M := \{x \in X: x \in M \text{ für alle } M \in \mathcal{M}\}
  \]
\end{definition}

\begin{bez}
  Ist $\mathcal{M} = \{M_{i} : i \in I\}$ mit einer sogenannten Indexmenge $I$, so schreibt man $\bigcup_{i \in I}M_{i} := \bigcup_{M \in \mathcal{M}}M$ bzw. $\bigcap_{i\in I}M_{i} := \bigcap_{M \in \mathcal{M}}M$. Am häufigsten verwendet man $\mathbb{N}$ als Indexmenge, aber auch ``größere'' Indexmengen $I$ sind mitunter erforderlich.
\end{bez}

Der Zusammenhang zwischen Durchschnitt und Vereinigung einerseits und der Komplementbildung andererseits wird durch die de Morgan'schen Regeln hergestellt:

\begin{theorem}{(de Morgan)}
  Es sei $\mathcal{M}$ ein Mengensystem auf einer Menge $X$. Dann gelten:

  \begin{center}
  \begin{minipage}[b]{0.4\linewidth}
    \[
      (i)~ {\left(\bigcup_{M \in \mathcal{M}} M\right)}^{C} = \bigcap_{M \in \mathcal{M}} M^{C}
    \]
  \end{minipage};\begin{minipage}[b]{0.4\linewidth}
    \[
      (ii)~ {\left(\bigcap_{M \in \mathcal{M}} M\right)}^{C} = \bigcup_{M \in \mathcal{M}} M^{C}
    \]
  \end{minipage}
  \end{center}

  (Bew. als ÜA!)
\end{theorem}

Eine weitere wichtige Konstruktion neuer Mengen aus bekannten erhält man mit dem ``kartesische Produkt'', benannt nach René Descartes (auch: Renatus Cartesius, 1596 - 1650), dem Begründer der analytischen Geometrie.

\begin{definition}{(Kartesisches Produkt)}
  $X$ und $Y$ seien Mengen. Dann heißt

  \[
    X \times Y := \{ (x, y) : x \in X, y \in Y \}
  \]

  das \emph{kartesische Produkt} von $X$ und $Y$. Seine Elemente werden als \emph{geordenete Paare} bezeichnet.
\end{definition}

\begin{remark}
  \begin{enumerate}
    \item[($i$)] Geordnet bedeutet, dass im allgemeinen $(x, y) \neq (y, x)$ im Gegensatz zur Aufzählung von Mengen kommt es hier also auf die Reihenfolge an!
    \item[($ii$)] Es gilt $(x, y) = (x', y') \Leftrightarrow x = x' \text{ und } y = y'$.
    \item[($iii$)] Verallgemeinerung: Sind $X_{1},X_{2},\dots,X_{n}$ Mengen, so heißt

      \[
        \bigtimes_{i=1}^{n} X_{i} := \left\{(x_{1}, \dots, x_{n}): x_{i} \in X_{i} \text{ für alle } i \in \{1, \dots, n\}\right\}
      \]

      das kartesische Produkt von $X_{1}$ bis $X_{n}$. Seine Elemente werden als $n$-Tupel (im Fall $n=3$ als Tripel).
  \end{enumerate}
\end{remark}


Wir kommen jetzt zum zentralen Begriff der Abbildung oder Funktion:

\begin{definition}{(Abbildung / Funktion)\footnote{In dieser Vorlesung verwenden wir diese Begriffe synonym.}}
  Gegeben seien zwei Mengen $X$ und $Y$. Unter einer \emph{Abbildung} oder \emph{Funktion} $f$ von $X$ nach $Y$ versteht man eine Vorschrift, die jedem Element $x \in X$ genau ein Element $y = f(x) \in Y$ zuordnet.
\end{definition}

\begin{bez}
  \begin{enumerate}
    \item[($i$)] Die Menge $X$ in dieser Def. heißt der \emph{Definitionsbereich} der Abbildung $f$, die Menge $Y$ ihr \emph{Zielbereich} (im allgemeinen $\neq$ Wertebereich!, s.u.)
    \item[($ii$)] Eine Abbildung wird erst vollständig charakterisiert durch Angabe von Definitionsbereich, Zielbereich und Zuordnungsvorschrift, üblicherweise in der Form

       \[
         f: X \rightarrow Y, x \mapsto f(x) \text{= $\dots$ Zuordnungsvorschrift}
       \]

      Abbildungen mit gleicher Zuordnungsvorschrift aber unterschiedlichen Definitions- und/oder Zielbereich können sich in wesentlichen Eigenschaften unterscheiden!
    \item[($iii$)] Die Teilmenge $G_{f} := \{ (x, f(x)): x \in X \}$ von $X \times Y$ heißt der \emph{Graph} der Abbildung f.
    \item[($iv$)] Kritisch betrachtet ist die oben gegebene Definition insofern unvollständig, als der Begriff Abbildung durch den nicht präzisierten Begriff der Zuordnung erklärt wird. Dies lässt sich vermeiden, indem man nicht zwischen der Abbildung und ihrem Graphen unterscheidet und zur Definition das kartesische Produkt heranzieht.
  \end{enumerate}
\end{bez}

\begin{definition}{(Abbildung, 2. Versuch)}
  Eine Abbildung $f: X \mapsto Y$ ist eine Teilmenge $G_{f} \subset X \times Y$, sodass zu jedem $x \in X$ genau ein $y \in Y$ mit $(x, y) \in G_{f}$ existiert. Dieses Element $y \in Y$ wird als $f(x)$ bezeichnet.
\end{definition}

(Mit dieser Definition wird der Begriff der Abbildung logisch einwandfrei auf den Mengenbegriff zurückgeführt, sie ist jedoch umständlich zu handhaben. Wir benutzen daher zumeist die erstgenannte Definition.)

\begin{definition}{Bild und Urbild}
  Es sei $f: X \mapsto Y$ eine Abbildung, $M\subset X$ und $N \subset Y$. Dann heißen

  \begin{enumerate}
    \item[($i$)] $f(M) := \{f(x):x \in M\}$ das \emph{Bild} von $M$ unter $f$ und
    \item[($ii$)] $f^{-1}(N) := \{x: f(x) \in N\}$ das \emph{Urbild} von $N$ unter $f$.
  \end{enumerate}
\end{definition}

Speziell: $f(x)$ heißt der Wertebereich oder das Bild von $f$. Im allgemeinen ist der Wertebereich eine echte Teilmenge des Zielbereichs, d.h. wir haben $f(x) \subsetneqq Y$

Wie vertragen sich Bild und Urbild mit Vereinigung und Durchschnitt?

\begin{theorem}
  Es sei $f : X \rightarrow Y$ eine Abbildung. $\mathcal{M} \subset \mathcal{P}(X)$ und $\mathcal{N}\subset \mathcal{P}(Y)$ seien Mengensysteme auf $X$ bzw. $Y$.

  Dann gelten:

  \begin{enumerate}
    \item $f(\bigcup_{M\in\mathcal{M}}M)=\bigcup_{M\in\mathcal{M}}f(M)$; $ f(\bigcap_{M\in\mathcal{M}}M)\subset\bigcap_{M\in\mathcal{M}}f(M)$;
    \item $f^{-1}(\bigcup_{N\in\mathcal{N}}N)=\bigcup_{N\in\mathcal{N}}f^{-1}(N)$; $f^{-1}(\bigcap_{N\in\mathcal{N}}N)=\bigcap_{N\in\mathcal{N}}f^{-1}(N)$,
  \end{enumerate}
\end{theorem}

\begin{remark}
  Im 2. Teil von 1. gilt im allgemeinen \emph{nicht} Gleichheit.
\end{remark}

Bsp: $\mathcal{M} = \{M_{1}, M_{2}\}$, $M_{1} \cap M_{2} = \emptyset \Rightarrow f(\bigcap_{M\in\mathcal{M}}M)=f(\emptyset)=\emptyset$.

Ist f konstant, also $f(x) = y_{0}$ für alle $x \in X$, so gilt $\bigcap_{M \in \mathcal{M}}f(M) = {y_{0}}$.

\begin{proof}
Exemplarisch wird der erste Teil von 1. beweisen, den Beweis von 2. dikutieren wir in den Übungen.

\begin{align*}
  y \in f(\bigcup_{M\in\mathcal{M}}M) &\Leftrightarrow \exists x \in \bigcup_{M\in\mathcal{M}}M \text{ mit } f(x)=y \\
                                      &\Leftrightarrow \exists M_{0} \in \mathcal{M}, x \in M_{0} \text{ mit } f(x)=y \\
                                      &\Leftrightarrow \exists M_{0} \in \mathcal{M} \text{ mit } y \in f(M_{0}) \\
  &\Leftrightarrow y \in \bigcup_{M\in\mathcal{M}}f(M)
\end{align*}
\end{proof}

\begin{bez}
  Neben dem sogenannten ``Existenzquantor'' $\exists$ verwendet man den ``Allquantor'' $\forall$. Er bedeutet ``für alle''. Weitere logische Symbole sind: $\land$ für ``und'' und $\lor$ für ``oder'' ($\lor$ von lat. vel = oder; oder ist hier \emph{nicht} ausschließlich zu verstehen, also \emph{nicht} im Sinne von entweder-oder).
\end{bez}

\begin{definition}
  Eine Abbildung $f: X \rightarrow Y$ heißt

  \begin{enumerate}
    \item \emph{injektiv}, falls für $x_{1},x_{2}\in X$ gilt: $f(x_{1})=f(x_{2}) \Rightarrow x_{1}=x_{2}$;
    \item \emph{surjektiv}, wenn zu jedem $y \in Y$ ein $x \in X$ existiert, sodass $y = f(x)$;
    \item \emph{bijektiv}, falls $f$ injektiv und surjektiv ist.
  \end{enumerate}
\end{definition}

Bsp: Diese Eigenschaften hängen wesentlich vom Definitions- und Zielbereich einer Funktion ab.

\[
  f: X \rightarrow Y,~ x \mapsto f(x)=x^{2}
\]

\begin{enumerate}
  \item[(a)] $X=Y=\mathbb{R}$: weder injektiv, noch surjektiv;
  \item[(b)] $X=\mathbb{R},Y=\left[0, \infty\right)$: surjektiv, nicht injektiv;
  \item[(c)] $X=\left[0,\infty\right),Y=\mathbb{R}$: injektiv, nicht surjektiv;
  \item[(d)] $X=Y=\left[0,\infty\right)$: bijektiv.
\end{enumerate}

\begin{definition}{(Verknüpfung / Komposition von Abbildungen)}
  Es seien $X,Y,Z$ Mengen, $g:X \rightarrow Y$ und $f: Y \rightarrow Z$ Abbildungen. Dann ist ihre Verknüpfung (Verkettung, Komposition) $f \circ g$ (gelesen ``$f$ nach $g$'') definiert durch

  \[
    f \circ g : X \rightarrow Z, x \mapsto f \circ g (x) := f(g(x))
  \]

  \begin{center}
  Skizze:
  \begin{minipage}[c]{0.5\linewidth}
    \begin{tikzpicture}[scale=2]
    \node at (0, 0) {$X$};
    \node at (1.5, 0) {$Y$};
    \node at (3, 0) {$Z$};
    \draw[->] (0.25, 0) -- (1.25, 0);
    \draw[->] (1.75, 0) -- (2.75, 0);
    \node at (0.75, 0.25) {$g$};
    \node at (2.25, 0.25) {$f$};
    \draw[->] (0.1, -0.1) to [out=325,in=180] (1.5, -0.5) node[below]{$f \circ g$} to [out=0,in=215] (2.9,-0.1);
  \end{tikzpicture}
  \end{minipage}
  \end{center}

\end{definition}

Ist $f: X \rightarrow Y$ eine Bijektion, so gibt es zu jedem $y \in Y$ genau ein $x \in X$ für das gilt $f(x)=y$. Also können wir jedem $y \in Y$ dasjenige $x \in X$ zuordnen, für das $f(x)=y$ gilt. Auf diese Weise wird eine Abbildung von $Y$ nach $X$ festgelegt, die wir als Umkehrabbildung, Umkehrfunktion oder Inverse Abbildung bezeichnen:

\begin{definition}{(Inverse Abbildung)}
  Es sei $f: X \rightarrow Y$ bijektiv. Die Abbildung

  \[
    f^{-1}: Y \rightarrow X,~ y \mapsto f^{-1}(y),
  \]

  definiert durch $f^{-1}(y)=x$, falls $f(x)=y$, heißt die Inverse (oder Umkehrabbildung) von $f$.
\end{definition}

\begin{remark}
  \begin{itemize}
    \item Nicht zu verwechseln mit dem Urbild von Mengen!
    \item $f \circ f^{-1} = \text{Id}_{y}$, $f^{-1} \circ f = \text{Id}_{x}$\footnote{Id := identische Abbildung auf $Y$ bzw auf $X$}
  \end{itemize}
\end{remark}
