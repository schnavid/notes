\chapter{Eigenwerte und Eigenvektoren}\label{ch:20}

\paragraph{Stichworte:} EWe, EVen, Spektrum, Lineare Unabhängigkeit von EVen zu verschiedenen EWen, Diagonalisierbarkeit, charakteristisches Polynom, Spur, Eigenraum, Existenz von EWen

Nach Satz~\ref{sec:16.19} gibt es zu jeder lin. Abb. $f: V \to W$ mit $\rg(f) = r$, $V, W$ endlichdimensional, Basen $B \subseteq V$, $C \subseteq W$, sodass $\prescript{}{C}[f]_B = \left[\begin{array}{c|c}
            I_r & 0 \\ \hline
            0 & 0
    \end{array}\right]$ gilt. Speziell gilt dies auch für Endomorphismen, wenn $V = W$ ist, aber dann müssen wir i.a.\ verschiedene Basen $B$ und $C$ wählen, um diese Darstellung zu erreichen. Es stellt sich die Frage, welche ``einfachen'' Darstellungen noch möglich sind, wenn wir im Bild- und Urbildexemplar von $V$ \emph{dieselbe} Basis verwenden möchten. Wir nennen solche einfachen Darstellungen \emph{Normalformen}. Dafür treffen wir für das Folgende die

    \section{Vereinbarung}\label{sec:20.1}

Ab jetzt verstehen wir unter einer Matrixdarstellung eines Endos stets eine solche, bei der in Bild und Urbild \emph{dieselbe Basis} gewählt ist. Verschiedene Matrixdarstellungen $A_1, A_2$ desselben Endos $f$ sind dann ähnlich, d.h.\ es gibt eine invertierbare Matrix $S$ mit $A_2 = S^{-1} A_1 S$.

Betrachten wir die Diagonalmatrix $A = \begin{pmatrix}
    \lambda_1 & & & 0 \\
    & \lambda_2 & & \\
    & & \ddots & \\
    0 & & & \lambda_n
\end{pmatrix} \in K^{n \times n}$

$\left(\text{d.h. } \alpha_{ij} = \begin{cases}
    \lambda_i & j=i\\
    0 & j \neq i
\end{cases}\right)$ und den zugehörigen Endo $f: K^n \to K^n, x \mapsto Ax$.

Die Diagonalelemente seien alle $\neq 0$. Hätte $f$ eine Normalform wie im Rangdarstellungssatz~\ref{sec:16.19} mit derselben Basis, so müsste $A$ zu $I_n$ ähnlich sein, d.h. $I_n = S^{-1} A S \Leftrightarrow A = S I_n S^{-1} = S S^{-1} = I_n$, was i.a. falsch ist. Somit kann eine Diagonalmatrix nicht auf ``einfachere'' Gestalt gebracht werden. Aber: $f$ hat eine andere wichtige Eigenschaft: $f(e_i) = Ae_i = \lambda_i e_i$ für die Einheitsvektoren! Zu $f$ existieren also Vektoren $v \neq o$, die durch $f$ in ein Vielfaches von sich übergehen (letzteres ist für $v=o$ trivial und in diesem Fall uninteressant).

\section{Eigenwerte und Eigenvektoren}\label{sec:20.2}

\begin{definition}{EW / EV}
    \begin{enumerate}
        \item Sei $V$ ein $K$-$VR$, $f: V \to V$ Endo

            Ein Skalar $\lambda \in K$ heißt \emph{Eigenwert (von $f$)}, falls ein Vektor $v \in V, v \neq o$ existiert mit \emph{$f(v) = \lambda v$}.

            Jeder solche Vektor $v$ heißt \emph{Eigenvektor (von $f$) zum Eigenwert $\lambda$}.
        \item Da für eine quadratische Matrix $A \in K^{n \times n}$ ein Endo $K^n \to K^n, x \mapsto Ax$ festgelegt wird, werden über diesen \emph{Eigenwerte und Eigenvektoren einer Matrix $A$} erklärt. (D.h. $\lambda$ ist EW zum EV $v \neq o$ der Matrix $A$, falls $Av = \lambda v$.)
        \item Die Menge aller Eigenwerte von $f$ heißt \emph{Spektrum} von $f$, analog \emph{Spektrum} von $A$.
    \end{enumerate}
\end{definition}

\section{Bemerkung}\label{sec:20.3}

\begin{remark}
    \begin{enumerate}
        \item Abkürzungen: EW = Eigenwert, EV = Eigenvektor
        \item engl. Bezeichnungen: eigen value = Eigenwert, eigen vector = Eigenvektor
        \item \emph{Anschaulich}: $v$ ist ein Vektor, den $f$ ``in dieselbe Richtun'' abbildet: das Bild $f(v)$ ist das $\lambda$-fache von $v$.

            \begin{tikzpicture}
                \draw[->] (0,0) -- (1,1);
                \node[anchor=east] at (0.5,0.5) {v};
                \draw[->] (0.1, -0.1) -- (2.1, 1.9);
                \node[anchor=west] at (1.1,0.9) {f(v)};
            \end{tikzpicture}
        \item Man könnte $L(v)$ auch als Fixrichtung bezeichnung, da $L(v) = L(f(v)) = f(L(v))$.
        \item Mit $\alpha \neq 0$ ist natürlich mit $v$ auch $\alpha v$ Eigenvektor zum selben Eigenwert.
        \item Ist $f: V \to W$ linear, kein Endo, macht $f(v) = \lambda v$ keinen Sinn, da $\lambda v \notin W$.
        \item Der Nullvektor $o$ ist (per Definition) kein EV! Niemals! ($\lambda \cdot o = o$ gilt für \emph{alle} $\lambda$)
        \item Ein \emph{Fixpunkt von $f$} ist ein $x \in V$ mit $f(x) = x$, d.h. für $x \neq 0$ ein EV zum EW $\lambda = 1$.
        \item Alle $x \in \ker f, x \neq o$, sind EVen zum EW $\lambda = 0$, da $f(x) = o = o \cdot x$ gilt.
    \end{enumerate}
\end{remark}

\section{Korollar}\label{sec:20.4}

\begin{corollary}
    Ähnliche Matrizen haben dieselben EWe, d.h. ($A, B \in K^{n \times n}$ ähnlich, $\exists \lambda \in K \exists v \in K^n, v \neq o: B v = \lambda v$) $\Rightarrow A w = \lambda w$ für ein $w \in K^n, w \neq o$.
\end{corollary}

\begin{proof}
    Sei $A = S^{-1} B S$ mit $S \in K^{n \times n}$ invertierbar. Aus $Bv = \lambda v$ folgt $A S^{-1} v = (\underbrace{S^{-1}B S}_A S^{-1}) v = S^{-1}Bv = S^{-1}\lambda v = \lambda S^{-1} v$, d.h. \emph{$S^{-1} v$} ist EV von $A$ zum EW $\lambda$.
\end{proof}

\section{Beispiel}\label{sec:20.5}

\begin{example}
    $A = \begin{pmatrix}
        0 & -3 \\
        2 & 5
    \end{pmatrix}$ hat dieselben EWe wie $B = \begin{pmatrix}
        2 & 0 \\
        0 & 3
    \end{pmatrix}$, nämlich $\lambda_1 = 2$ und $\lambda_2 = 3$, denn $\underbrace{\begin{pmatrix}
        3 & -1 \\
        -2 & 1
    \end{pmatrix}}_{S^{-1}} \cdot \underbrace{\begin{pmatrix}
        2 & 0 \\
        0 & 3
    \end{pmatrix}}_B \cdot \underbrace{\begin{pmatrix}
        1 & 1 \\
        2 & 3
    \end{pmatrix}}_S = \begin{pmatrix}
        3 & -1 \\
        -2 & 1
    \end{pmatrix} \cdot \begin{pmatrix}
        2 & 2 \\
        6 & 9
    \end{pmatrix} = \begin{pmatrix}
        0 & -1 \\
        2 & 5
    \end{pmatrix}$, EVen: $S^{-1} e_1 = \begin{pmatrix}
        3 \\ -2
    \end{pmatrix}$, $S^{-1} e_2 = \begin{pmatrix}
        -1 \\ 1
    \end{pmatrix}$. Probe:

    \[\begin{pmatrix}
        0 & -3 \\
        2 & 5
    \end{pmatrix} \cdot \begin{pmatrix}
        3 \\ 2
    \end{pmatrix} = \begin{pmatrix}
        6 \\ -4
    \end{pmatrix} = 2 \cdot \begin{pmatrix}
        3 \\ -2
    \end{pmatrix} \quad \checkmark,\]

    \[\begin{pmatrix}
        0 & -3 \\
        2 & 5
    \end{pmatrix} \cdot \begin{pmatrix}
        -1 \\ 1
    \end{pmatrix} = \begin{pmatrix}
        -3 \\ 3
    \end{pmatrix} = 3 \cdot \begin{pmatrix}
        -1 \\ 1
    \end{pmatrix} \quad \checkmark,\]
\end{example}

\paragraph{Frage:} Aber wie bekommt man $S$? Durch Bestimmung der EVen, vgl. Bsp. \emph{20.26}.

\section{Eigenvektoren und lineare Abhängigkeit}\label{sec:20.6}

Eine wichtige Eigenschaft: EVen zu verschiedenen EWen sind linear unabhängig.

\begin{theorem}
    Sei $V$ ein $K$-VR, $f: V \to V$ Endo, $\lambda_1, \dots, \lambda_k$ p.w.v. EWe von $f$ mit EVen $v_1, \dots, v_k$. \hfill (Analog für Matrizen)
\end{theorem}

\begin{proof}
    Vollständige Induktion nach $k$: \emph{$k=1$}: klar, da $v_1 \neq 0$ nach Def. ``EV''.

    \emph{$k \to k + 1$}: Sei $\alpha_1 v_1 + \dots + \alpha_{k+1}v_{k+1}=0$ mit $\alpha_1, \dots, \alpha_{k+1} \in K$. Nach Anwendung von $f$

    \begin{align*}
        \text{folgt } & o = f(o) = f(\alpha_1 v_1 + \dots + \alpha_{k+1} v_{k+1}) = \alpha_1 f(v_1) + \dots + \alpha_{k+1} f(v_{k+1}), \\
        \text{also } & o = \alpha_1\lambda_1 v_1 + \dots + \alpha_{k+1} \lambda_{k+1} v_{k+1}.
    \end{align*}

    Andererseits gilt $0 = \lambda_1 o = \lambda_1 \alpha_1 v_1 + \lambda_1 \alpha_2 v_2 + \dots + \lambda_1 \alpha_{k+1} v_{k+1}$, von der vorigen Gleichung abziehen liefert $(\lambda_2 - \lambda_1) \alpha_2 v_2 + \dots + (\lambda_{k+1} - \lambda_1) \alpha_{k+1} v_{k+1} = o$.

    Nach Induktionsvoraussetzung sind $v_2, \dots, v_{k+1}$ linear unabhängig, es folgt $(\lambda_2 - \lambda_1) \alpha_2 = \dots = (\lambda_{k+1} - \lambda_1) \alpha_{k+1} = o$. Da die $\lambda_i$ p.w.v.\ folgt $\alpha_2 = \dots = \alpha_{k+1} = 0$, $\underbrace{\alpha_1 v_1 = 0}_{\Rightarrow \alpha_1 = 0}$.
\end{proof}

\section{Korollar}\label{sec:20.7}

\begin{corollary}
    Jeder Endo eines $n$-dim. $K$-VRs $V$ hat $\leq n$ viele EWe.

    (Analog für jede Matrix $A \in K^{n \times n}$)
\end{corollary}

\begin{proof}
    Wegen \emph{Satz 20.6} und der Tatsache, dass in $V$ maximal $n$ Vektoren lin. unabhängig sind.
\end{proof}

\section{Korollar}\label{sec:20.8}

\begin{corollary}
    Besitzt ein Endo $f$ eines $n$-dim. $K$-VRs $V$ genau $n$ verschiedene EWe $\lambda_1,\dots,\lambda_n$, so bildet eine Familie $(v_1, \dots, v_n)$ zugehöriger EVen eine Basis von $V$. Bezüglich einer solchen Basis hat $f$ die Matrixdarstellung $\begin{pmatrix}
        \lambda_1 & & 0 \\
                  & \ddots & \\
        0 & & \lambda_n
    \end{pmatrix}$ als Diagonalmatrix.
\end{corollary}

\section{Bemerkung}\label{sec:20.9}

\begin{remark}
    Die Identität $id_V$ (alle $x \neq 0$ sind EVen zum EW $1$) zeigt aber, dass durchaus eine Basis aus EVen existieren kann, ohne dass $n$ verschiedene EWe vorhanden sind. Hat ein Endo $f$ eine Basis aus EVen, kann dieser als Diagonalmatrix dargestellt werden:
\end{remark}

\section{Satz}\label{sec:20.10}

\begin{theorem}
    Zu einem Endo $f$ sei $(v_1, \dots, v_n)$ eine Basis aus EVen von $f$ zu EWen $\lambda_1, \dots, \lambda_n$ (nicht notwendig verschieden). Dann hat $f$ bzgl. dieser Basis die Matrixdarstellung

    \[ A = \diag(\lambda_1, \dots, \lambda_n) := \begin{pmatrix}
        \lambda_1 &   & 0 \\
            & \ddots & \\
        0   &   & \lambda_n
    \end{pmatrix} \text{ mit } (\alpha_{ij}) = \begin{cases}
        \lambda_i, & i=j,\\
        0, & \text{sonst}
    \end{cases}\]
\end{theorem}

\begin{proof}
    Die Einträge $\alpha_{ij}$ von $A$ sind die Koeffizienten der Darstellung $f(v_j) = \alpha_{1j} v_{1} + \dots + \alpha_{nj} v_n$, wobei $f(v_j) = \lambda_j v_j$ zeigt, dass $\alpha_{ij} = \begin{cases}
        \lambda_i, & i=j, \\
        0, & \text{sont}
    \end{cases}$ gilt.
\end{proof}

\section{Diagonalisierbar}\label{sec:20.11}

Wie führen den folgenden Begriff ein.

\begin{definition}{(diagonalisierbar)}
    Besitzt ein Endo $f$ eine Matrixdarstellung als (endliche) Diagonalmatrix, d.h. $\exists$ Basis $B$ mit $\prescript{}{B}[f]_B = \diag(\lambda_1,\dots,\lambda_n)$ für $\lambda_1,\dots,\lambda_n \in K$, so heißt $f$ \emph{diagonalisierbar}.
\end{definition}

\section{Satz}\label{sec:20.12}

Satz~\ref{sec:20.10} lässt sich umkehren:

\begin{theorem}
    Jeder diagonalisierbare Endo $f$ von $V$ mit $\dim V = n$ besitzt eine Familie $(v_1, \dots, v_n)$ von Eigenvektoren, die eine Basis von $V$ bilden.
\end{theorem}

\begin{proof}
    Da $f$ diagonalisierbar, existiert ein Isomorphismus $g: V \to K^n$ so, dass $f':= g \circ f \circ g^{-1}$ bezüglich $\varepsilon = (e_1, \dots, e_n)$ die Matrixdarstellung

    \[ A = \diag(\lambda_1,\dots,\lambda_n) = \begin{pmatrix}
        \lambda_1 & & 0\\
                  & \ddots & \\
        0 & & \lambda_n
    \end{pmatrix} \text{ hat.}\]

    Dann ist aber $f'(e_i) = A e_i = \lambda_i e_i$, und mit $v_i := g^{-1}(e_i)$ ist dann $f(v_i) = (g^{-1} \circ \underbrace{g \circ f \circ g^{-1}}_{f'})(e_i) = (g^{-1} \circ f')(e_i) = g^{-1}(f'(e_i)) = g^{-1}(\lambda_i e_i) = \lambda_i g^{-1}(e_i) = \lambda_i v_i$, also sind die $v_i$ EVen und als Bilder der Einheitsvektoren unter dem Isomorphismus $g^{-1}$ eine Basis.
\end{proof}

\section{Zusammenfassung}\label{sec:20.13}

\begin{itemize}
    \item Ein Endo $f$ von $V$, $\dim V = n$, ist genau dann diagonalisierbar wenn es eine Basis aus EVen von $f$ gibt.
    \item Ein Endo $f$ von $V$, $\dim V = n$, ist sicher dann diagonalisierbar, wenn es zu ihm $n$ verschiedene EWe gibt.
    \item Keineswegs sind alle Endos in endlich dimensionalen VRen diagonalisierbar, vgl.\ \emph{L21}.
\end{itemize}

\section{Zur (rechnerischen) Bestimmung von EW / EV}\label{sec:20.14}

\paragraph{Ausgangssituation:} Sei $V$ ein $K$-VR, $\dim V = n$, $f: V \to V$ Endo. Sei $A := \prescript{}{B}[f]_B$ und betrachten Koordinatenvektor $K_B(v) \in K^n$ bzgl.\ Basis $B$. Dann ist die Untersuchung von $f(v) = \lambda v$ gleichwergig mit der von $A \cdot w = \lambda w$ und $w = K_B(v): \xymatrix{V \ar[r]^f \ar[d]^{K_B} & V \ar[d]_{K_B} \\ K^n \ar[r]_A & K^n}$. Es genügt also, EW / EV von quadratischen Matrizen zu berechnen!

(Die EWe sind dieselben $v = K_B^{-1}(w)$ erhält man die EVen in $V$.)

\section{Charakterisierung von EWen}\label{sec:20.15}

\begin{theorem}
    Sei $A \in K^{n \times n}$. Dann sind äquivalent:

    \begin{enumerate}
        \item $\lambda \in K$ ist EW von $A$
        \item es existiert ein $v \in K^n$, $v \neq o$: $A v = \lambda v$
        \item es existiert ein $v \in K^n$, $v \neq o$: $(A - \lambda \cdot I_n) v = o$, $I_n = \text{$n$-te Einheitsmatrix}$
        \item das homogene LGS $(A - \lambda \cdot I_n) \cdot v = 0$ hat eine nichttriviale Lsg. (in $v$)
        \item $\rg(A - \lambda I_n) < n$
        \item $\det (A - \lambda I_n) = 0$
    \end{enumerate}
\end{theorem}

\begin{proof}
    $(1) \Leftrightarrow (2)$ laut Def.,

    $(2) \Leftrightarrow (3)$: Umformen,

    $(3) \Leftrightarrow (4)$: Formulierung mit LGS,

    $(4) \Leftrightarrow (5)$: laut Korollar \emph{16.16} mit $b = o$,

    $(5) \Leftrightarrow (6)$: laut Satz \emph{19.3.(3)}.
\end{proof}

\section{Bemerkung}\label{sec:20.16}

$v$ in $(2)-(4)$ ist EV zu $\lambda$, d.h. \emph{die EVen zu $\lambda$ sind die Elemente aus $\ker(A - \lambda I_n) \setminus \{o\}$.} Die Charakterisierung (6) von EWen ist rechnerisch machbar: Fasst man $\lambda$ als Unbestimmte in dem Ausdruck $\det(A - \lambda I_n)$ auf, d.h. bildet $\det(A - T \cdot I_n)$, so ergibt dies ein Polynom $\in K[T]$ laut Laplace-Entwicklungssatz: $\det (A - T \cdot I_n) = \det((\alpha_{ij} - T \cdot \delta_{ij})_{ij}) = \sum\limits_{i=1}^n (\alpha_{ij} - T \cdot \delta_{ij}) \cdot \det A_{ij}$, und sind die $\det A_{ij}$ Polynome, so auch induktib $\det (A - T \cdot I_n)$.

Ein jeder EW ist dann Nullstelle dieses Polynoms (beachten: $\det$ ist für Matrizen über $K[T]$ anstelle $K$ auch sinnvoll, sodass $\det(A - T \cdot I_n) \in K[T]$ ebenso Laplace-entwickelbar).

\section{Definition}\label{sec:20.17}

\begin{definition}
    Das Polynom $X_A(T) := \det(A - T \cdot I_n)$ heißt \emph{charakteristisches Polynom} von $A$, \emph{Satz 20.15} zeigt:


    \begin{center}
        {\emph{Die Nullstellen des charakteristischen Polynoms sind genau die EWe von $A$.}}

        (In Formeln: $X_A(\lambda) = 0 \Leftrightarrow \exists v \neq 0: A v = \lambda v$)
    \end{center}
\end{definition}

\section{Bemerkung}\label{sec:20.18}

\begin{remark}
    \begin{enumerate}
        \item Mühelos lässt sich Satz~\ref{sec:20.15} für einen Endomorphismus $f: V \to V$ übertragen:
            
            \begin{align*}
                \lambda \in K \in W \text{ von } f \Leftrightarrow&\exists v \neq o: f(v) = \lambda v\\
                \Leftrightarrow&\exists v \neq 0 : (f - \lambda \cdot \id_V) v = o \\
                \Leftrightarrow& \ker(f - \lambda \id_V) \neq \{o\}\\
                \Leftrightarrow&\underbrace{\dim\ker(f - \lambda \id_V)}_{= n - \dim\im(f - \lambda \id_V) \text{ laut \emph{Rangsatz}}} > 0\\
                \Leftrightarrow&\rg(f-\lambda \id_V) < n.
            \end{align*}
        \item Die Rangbedingung $\rg(f - \lambda \id_V) < n$ ist äquivalent zu $\det(f - \lambda \id_V) = 0$ nach \emph{18.11}. Ist $F = \prescript{}{B} [f]_B$ irgendeine Matrixdarstellung von $f$ (laut~\ref{sec:20.1}), so ist auch $F - \lambda I_n$ eine Matrixdarstellung von $f - \lambda \id_V: \prescript{}{B}[ f - \lambda \id_V ]_B = \prescript{}{B}[f]_B - \lambda \cdot \prescript{}{B}[\id_V]B = F - \lambda \cdot I_n$ unter Verwendung von~\ref{sec:15.5}. Nach~\ref{sec:19.5} ist dann $\det (f - \lambda \id_V) = \det(F - \lambda \cdot I_n)$.
    \end{enumerate}
\end{remark}

Damit ist das charakteristische Polynom eines Endos wohldefiniert, d.h. unabh. von $B$:

\section{Definition}\label{sec:20.19}

\begin{definition}
    Sei $f: V \to V$ ein Endo, $\dim V = n, F \in K^{n \times n}$ (irgend)eine Matrixdarstellung von $f$. Dann heißt $X_f(T) := \det(F - T \cdot I_n)$ \emph{charakteristisches Polynom von $f$}.
\end{definition}

Weiter folgt:

\section{Satz}\label{sec:20.20}

\begin{theorem}
    Ähnliche Matrizen haben dasselbe charakteristische Polynom (Schreibe analog $X_A (T), A \in K^{n \times n}$).
\end{theorem}

\begin{proof}
    Eine zu $A$ ähnliche Matrix ist auch Matrixdarstellung von $f_A: K^n \to K^n, x \mapsto Ax$, und hat wegen~\ref{sec:20.18}.2 dasselbe charakteristische Polynom. Oder direkt argumentiert:

    \begin{align*}
        A = S^{-1} B S \Leftrightarrow& \det(A - T \cdot I_n)\\
        =& \det\left(S^{-1} B S - \underbrace{T \cdot I_n}_{= S^{-1} \cdot T \cdot I_n \cdot S}\right)\\
        =& \det(S^{-1}) \det(B - T \cdot I_n) \det S\\
        =& \det(B - T \cdot I_n). \quad\qedhere
    \end{align*}
\end{proof}

Das charakteristische Polynom ist auch an sich interessant:

\section{Satz}\label{sec:20.21}

\begin{theorem}
Der Grad des charakteristischen Polynoms $X_f(T)$ eines Endos $f: V \to V$, wenn $n = \dim V$ ist, beträgt $n$. Sein Leitkoeffizient vor $T^n$ ist ${(-1)}^n$, der Absolutkoeffizient vor $T^0$ ist $\det f$, und sein Koeffizient vor $T^{n -1}$ ist $(-1)^{n-1}$ mal der \emph{Spur von $f$}, wo $\Spur(f) = \sum\limits_{i = 1}^n \alpha_{ii}$, falls $A = {(\alpha_{ij})}_{i,j} = \prescript{}{B}[f]_B$ ist.
\end{theorem}

\section{Korollar}\label{sec:20.22}

\begin{corollary}
    Die Spur ähnlicher Matrizen ist gleich. (Damit ist $\Spur(f) := \Spur(A)$ wohldefiniert)

    (Dass die Determinante ähnlicher Matrizen gleich ist, ist schon aus Satz \emph{19.3(6)} bekannt.)
\end{corollary}

\begin{proof}
    Wegen Satz \emph{20.20} ist der Koeffizient vor $T^{n-1}$ derselbe, jetzt Satz \emph{20.21}.
\end{proof}

\section{Beweis von Satz~\ref{sec:20.21}}\label{sec:20.23}

\begin{proof}
    Die Laplace-Entwicklung von

    \[B := A - T I_n = \begin{pmatrix}
        \alpha_{11} - T & \alpha_{12} & \dots & \alpha_{1n} \\
        \alpha_{21} & \alpha_{22} - T & \dots & \alpha_{2n} \\
        \vdots & \vdots & & \vdots \\
        \alpha_{n1} & \alpha_{n2} & \dots & \alpha_{nn} - T
    \end{pmatrix}\]

    nach Zeile $i = 1$ liefert

    \begin{align*}
         &\sum_{j=1}^n {(-1)}^{1 + j} \beta_{1j} \det B_{1j}\\
        =&\underbrace{{(-1)}^{1+1}}_{=+1} (\alpha_{11} - T) \cdot \underbrace{\det(A_{11} - T \cdot I_{n-1})}_{\underbrace{=}_{\text{Ind. Vor.}} {(-1)}^{n-1} \cdot T^{n-1} + {(-1)}^{n-2} \Spur(A_{11}) \cdot T^{n-1} + \dots}\\
         &+ \sum_{j=2}^n {(-1)}^{1 + j} \alpha_{1j} \cdot \overbrace{\det B_{1j}}^{\text{Terme vom Grad $\leq n - 2$}}\\
        =&{(-1)}^n T^n + \alpha_{11} \cdot {(-1)}^{n-1} T^{n-1}\\
         &+ {(-1)}^{n-1} (\alpha_{22} + \dots + \alpha_{nn}) T^{n-1}\\
         &+ \dots \quad\text{Terme kleineren Grades,}
    \end{align*}

    also folgt $\deg(A - T I_n) = n$, Leitkoeffizient ist ${(-1)}^n$, Koeffizient vor $T^{n-1}$ ist ${(-1)}^{n-1} \Spur(A)$.

    Einsetzen von $\lambda = 0$ in das charakteristische Polynom liefert den Absolutkoeffizienten $X_f(0) = \det(f - 0 \cdot \id_V) = \det f$.
\end{proof}

Zuletzt fassen wir die Menge der Eigenvektoren zum Eigenwert $\lambda$ mit $o$ zusammen:

\section{Definition}\label{sec:20.24}

\begin{definition}
    Die Menge $E_\lambda := \{v \in V | f(v) = \lambda v\} = \ker (f - \lambda \id_V)$ heißt \emph{Eigenraum} von $f$ zum Eigenwert $\lambda$.
\end{definition}

\section{Bemerkung}\label{sec:20.25}

\begin{remark}
    \begin{itemize}
        \item Als Kern eines Endos ist $E_\lambda$ ein Untervektorraum von $V$.
        \item Anschaulich: jeder Eigenraum wird mit $f$ in sich abgebildet:

            Es gilt $f(E_\lambda) \subseteq E_\lambda$ (d.h. $E_\lambda$ ist \emph{$f$-invariant}).
    \end{itemize}
\end{remark}

Der Eigenraum $E_\lambda = \ker(A - \lambda \cdot I_n)$ ist der Lösungsraum des homogenen LGS $(A - \lambda \cdot I_n) \cdot v = 0$.

Mit den Lösungen dieses LGS, etwa mit dem Gauß-Eliminationsverfahren, werden die Eigenvektoren bestimmt.

\section{Beispiel}\label{sec:20.26}

Sei $A = \begin{pmatrix}
    -1 & 2 & 2 \\
    2 & 2 & 2 \\
    -3 & 6 & 6
\end{pmatrix} \in \mathbb{R}^{3 \times 3}$. Das charakteristische Polynom lautet

\[X_A(T) = \det(A - T \cdot I_3) = \det\begin{pmatrix}
    -1-T & 2 & 2\\
    2 & 2-T & 2\\
    -3 & 6 & 6-T
\end{pmatrix}.\]

Subtraktion der dritten Spalte von der zweiten zeigt $X_A(T) = \det\begin{pmatrix}
    -(1+T) & 0 & 2\\
    2 & -T & 2\\
    -3 & T & -6-T
\end{pmatrix}$, und Entwickeln nach der zweiten Spalte liefert

\begin{align*}
    X_A(T) =&\quad {(-1)}^{2+2} \cdot (-T) \cdot \left(-(1+T) \cdot (-6-T) - (-3) \cdot 2\right)\\
            &+ {(-1)}^{3+2} \cdot T \cdot (-(1+T) \cdot 2 - 2 \cdot 2)\\
    =& -T \cdot ((1+T)(6+T) + 6) - T \cdot (-(1+T) \cdot 2 - 4)\\
    =& -T \cdot (6 + T + 6T + T^2 + 6 - 2 - 2T - 4)\\
    =& -T \cdot (T^2 + 5T + 6) = -T \cdot (T+2)(T+3).
\end{align*}

Die Nullstellen sind $\lambda_1 = 0$, $\lambda_2 = -2$, $\lambda_3 = -3$, das sind hier die EWe, laut Korollar~\ref{sec:20.8} bilden zugehörige EVen eine Basis $(v_1, v_2, v_3)$ des $\mathbb{R}^3$, sodass also $A$ diagonalisierbar ist, d.h. ähnlich zu $\left(\begin{smallmatrix}
0 & 0 & 0\\
0 & -2 & 0\\
0 & 0 & -3\\
\end{smallmatrix}\right)$.

Bestimmung der Eigenvektoren:

\begin{itemize}
    \item $\lambda_1 = 0: \left(\begin{smallmatrix}
                -1 & 2 & 2\\
                2 & 2 & 2\\
                -3 & -6 & -6
            \end{smallmatrix}\right) \cdot x = o$ hat Lösungsmenge $L(v_1)$ mit $v_1 := \left(\begin{smallmatrix}
                0\\1\\-1
        \end{smallmatrix}\right)$.
    \item $\lambda_2 = -2: \left(\begin{smallmatrix}
                1 & 2 & 2\\
                2 & 4 & 2\\
                -3 & -6 & -4
            \end{smallmatrix}\right) \cdot x = o$ hat Lösungsmenge $L(v_2)$ mit $v_2 := \left(\begin{smallmatrix}
                2\\-1\\0
        \end{smallmatrix}\right)$.
    \item $\lambda_3 = -3: \left(\begin{smallmatrix}
                2 & 2 & 2\\
                2 & 5 & 2\\
                -3 & -6 & -3
            \end{smallmatrix}\right) \cdot x = o$ hat Lösungsmenge $L(v_3)$ mit $v_3 := \left(\begin{smallmatrix}
                1\\0\\-1
        \end{smallmatrix}\right)$.
\end{itemize}

Die Eigenräume sind genau diese Lösungsmengen: $E_{\lambda_i} = L(v_i), i=1,2,3$.

Bilden wir die Matrix $S: (v_1 | v_2 | v_3)$ mit den EVen als Spalten so ist $S$ invertierbar, weil $(v_1, v_2, v_3)$ eine Basis ist.

Nun ist $S^{-1} A S = \left(\begin{smallmatrix}
    \lambda_1 & 0 & 0\\
    0 & \lambda_2 & 0\\
    0 & 0 & \lambda_3
\end{smallmatrix}\right) = \diag(\lambda_1, \lambda_2, \lambda_3)$,

denn wegen $v_i = S e_i, i = 1, 2, 3$, gilt

\[S^{-1} A S e_i = S^{-1} A v_i = S^{-1} \lambda_i v_i = \lambda_i S^{-1} S e_i = \lambda_i e_i.\]

Wann hat ein Endo / eine quadratische Matrix garantiert mindestens einen EW?

Wenn das charakteristische Polynom mindestens eine Nullstelle hat:

\section{Satz}\label{sec:20.27}

\begin{theorem}
    Jeder Endo $f$ zu einem endlich dimensionalen VR über $\mathbb{C}$ (oder sonst einem algebraisch abgeschlossenem Körper) besitzt mindestens einen EW. Ebenso, Wenn der Körper $\mathbb{R}$ ist und $n$ eine ungerade natürliche Zahl.
\end{theorem}

\begin{proof}
    Satz \emph{8.32} (der Fundamentalsatz der Algebra, Körper mit dieser Eigenschaft heißen \emph{algebraisch abgeschlossen}), bzw. Zwischenwertsatz für reelle Polynome ungeraden Grades, da $\deg X_f(T) = n$ nach Satz \emph{20.21}
\end{proof}
