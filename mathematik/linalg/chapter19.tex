\chapter{Determinante einer Matrix}\label{ch:19}

\paragraph{Stichworte:} Determinante einer Matrix, Entwicklungssatz von Laplace, Streichungsmatrizen, vereinfachtes Gaußeliminationsverfahren, algebraisches Komplement, Cramersche Regel

Mit dem Begriff der Determinante eines Endomorphismus kommen wir direkt zur Determinante einer Matrix als Determinante der zugehörigen Matrixabbildung:

\section{Determinante einer Matrix}\label{sec:19.1}

\begin{definition}{Determinante einer Matrix}
    Eine quadratische Matrix $A \in K^{n \times n}$ definiert eindeutig einen Endomorphismus $f_A: K^n \to K^n, x \mapsto Ax$.

    Dann heißt $det A:= det f_A$ die \emph{Determinante von $A$}.
\end{definition}

\section{$\Delta$ bei Matrizen}\label{sec:19.2}

\begin{theorem}
    Für jede Matrix $A \in K^{n \times n}$ mit Spalten $a_1, \ldots, a_n$ gilt $\det A = \Delta(a_1, a_2, \ldots, a_n)$
\end{theorem}

\begin{proof}
    $\det A = \det f_a \underbrace{=}_{\text{18.10}} \Delta(f_A(e_1), \ldots, f_A(e_n)) = \Delta(Ae_1, \ldots, Ae_n) = \Delta (a_1, \ldots, a_n)$, denn Sie wissen ja: die Spalten sind die Bilder der Einheitsvektoren.
\end{proof}

Wir erhalten die folgenden Eigenschaften:

\section{Eigenschaften:}\label{sec:19.3}


\begin{theorem}
    Für $A, B \in K^{n \times n}$ und die Einheitsmatrix $I_n \in K^{n\times n}$ gelten

    \begin{enumerate}
        \item $\det I_n = 1$
        \item $\det(A \cdot B) = \det(A) \cdot \det(B)$
        \item $\det A = 0 \Leftrightarrow \rg(A) < n$
        \item $\det A \neq 0 \Leftrightarrow \rg(A) = n \Leftrightarrow A \text{ invertierbar}$, vgl \emph{16.17}
        \item $A$ invertierbar $\Rightarrow \det(A^{-1}) = \frac{1}{\det A}$
        \item $B$ invertierbar $\Rightarrow \det(B^{-1} A B) = \det A$, d.h. ähnliche Matrizen haben die gleiche Determinante
    \end{enumerate}
\end{theorem}

\begin{proof}
    Der Beweis ist mit Satz \emph{18.11} schon geführt; er überträgt sich direkt auf Matrizen. Insb. für (6) können wir den Beweis analog umschreiben zu $\det(B^{-1} A B) = \det(B^{-1}) \cdot \det A \cdot \det B = \frac{1}{\det B} \cdot \det A \cdot \det B = \det A$.
\end{proof}

\section{Bemerkung}\label{sec:19.4}

\begin{remark}
    Mit die wichtigste Bedeutung von $\det A$ bzw. $\det f$ ist die Erkennung von invertierbaren Matrizen bzw. Isomorphismen, denn wir haben alsoL

    Für Endo $f: V \to V$ gilt: $\det f \neq 0 \Leftrightarrow f$ Isomorphismus,

    für $A \in K^{n \times n}$ gilt: $\det A \neq 0 \Leftrightarrow$ Mult. mit A ist Isomorphismus $\Leftrightarrow$ invertierbar.
\end{remark}

Wir beachten den Zusammenhang zwischen der Determinante eines Endomorphismus $f: V \to V$ und den Matrixdarstellungen von $f$:

Alle Abb.matrizen $\prescript{}{C}{[f]}_B$ zu verschiedenen Basen $B$ sind ähnlich zu $A = \prescript{}{\varepsilon}{[f]}_\varepsilon$ nach \emph{17.9}. Also zeigt \emph{19.3(6)}:

\section{Korollar}\label{sec:19.5}

\begin{corollary}
    Für einen Endomorphismus $f: V \to V, \dim V = n$, und irgendeine Basis $B$ von $f$ gilt $\det f = \det \prescript{}{B}{[f]}_B$.

\end{corollary}

Würde man $\det f$ so definieren wie in \emph{19.5}, ist dies also wohldefiniert, da von der Basiswahl für $B$ unabhängig.

\section{Beispiel}\label{sec:19.6}

\begin{example}
    Ist $A = \begin{pmatrix}
        a & b \\ c & d
    \end{pmatrix} \in K^{2 \times 2}$ eine $2 \times 2$-Matrix, ist $\det A = \det\left(\begin{pmatrix}
        a \\ c
    \end{pmatrix}, \begin{pmatrix}
        b \\ d
\end{pmatrix}\right) = ad - bc$ laut Konstruktion \emph{18.7}, Fall $n=2$.
\end{example}

\section{Bemerkung}\label{sec:19.7}

\begin{remark}
    Für eine lin. Abb. $f: V \to W$, selbst wenn $\dim V = \dim W = n$ ist, kann \emph{kein $\det f$} definiert werden: Werden Basen $B, C$ in $V, W$ gewählt, kann zwar $\det \prescript{}{C}{[f]}_B$ berechnet werden, bei Basiswechsel zu anderen Basen erhalten wir aber eine dazu äquivalente Matrix $R^{-1} \prescript{}{C}{[f]}_B S$, wobei $\det R \neq \det S$ (mit $B \neq C$)
\end{remark}

\begin{example}
    $f: \mathbb{R}^2 \to \mathbb{R}^2, \begin{pmatrix}
        x \\ y
    \end{pmatrix} = \begin{pmatrix}
    1 & 3 \\ 2 & 4
    \end{pmatrix} \begin{pmatrix}
        x \\ y
    \end{pmatrix}, B = \varepsilon, C = \left(\begin{pmatrix}
        0 \\ 1
    \end{pmatrix}, \begin{pmatrix}
        1 \\ 0
\end{pmatrix}\right): \prescript{}{C}{[f]}_\varepsilon = \prescript{}{C}{[id]}_\varepsilon \cdot \underbrace{\prescript{}{\varepsilon}{[f]}_\varepsilon}_{\left(\begin{smallmatrix}1 & 3 \\ 2 & 4\end{smallmatrix}\right)} \cdot \underbrace{\prescript{}{\varepsilon}{[\id]}_\varepsilon}_{I_2}$ mit BWmatrix $\prescript{}{C}{[\id]}_\varepsilon = \begin{pmatrix}2 & 4 \\ 1 & 3\end{pmatrix}$,

\[\text{da } f\begin{pmatrix}1\\0\end{pmatrix} = \begin{pmatrix}1\\2\end{pmatrix} = \emph{2} \cdot \begin{pmatrix}0\\1\end{pmatrix} + \emph{1} \cdot \begin{pmatrix}1\\0\end{pmatrix} \]

\[\text{und } f\begin{pmatrix}0\\1\end{pmatrix} = \begin{pmatrix}3\\4\end{pmatrix} = \emph{4} \cdot \begin{pmatrix}0\\1\end{pmatrix} \text{, also} \]

\[\prescript{}{C}{[f]}_\varepsilon = \begin{pmatrix}2 & 4 \\ 1 & 3\end{pmatrix} \cdot \begin{pmatrix}1 & 3 \\ 2 & 4\end{pmatrix} = \begin{pmatrix}10 & 22 \\ 7 & 15\end{pmatrix}, \det\begin{pmatrix}10 & 22 \\ 7 & 15\end{pmatrix} = 10 \cdot 15 - 7 \cdot 22 = -4, \]

mit anderer Determinante als $\prescript{}{C}{[f]}_\varepsilon = \begin{pmatrix}1 & 3 \\ 2 & 4\end{pmatrix}: \det\begin{pmatrix}1 & 3 \\ 2 & 4\end{pmatrix} = 4 - 6 = -2$.
    
\end{example}

Jetzt liefert uns Satz \emph{18.8} eine Rekursionsformel zur Berechnung von $\det A$:

\section{Entwicklungssatz von Laplace}\label{sec:19.8}

\begin{theorem}{(\emph{Entwicklungssatz von Laplace, Entwicklung nach der $i$-ten Zeile:})}
    Für eine Matrix $A = (\alpha_{k0}) \in K^{n \times n}$ und Indizes $i, j \in \{1, \ldots, n\}$ bezeichne $A_ij \in K^{(n-1) \times (n-1)}$ diejenige Matrix, die aus $A$ durch Streichen der \emph{$i$-ten Zeile} und der $j$-ten Spalte entsteht. Damit gilt für jedes $i = 1, \ldots, n$:

    \begin{center}
        \boxed{\det A = \sum_{j=1}^n (-1)^{i+j} \alpha_{ij} \det A_{ij}.}
    \end{center}
\end{theorem}

\begin{proof}
    wegen \emph{Satz 19.2} vergewissern wir uns, dass diese Formel genau die aus \emph{Satz 18.8} ist.
\end{proof}

\begin{remark}
    Man nennt die $A_{ij}$ \emph{Streichungsmatrizen} und die Streichungsmatrizendeterminanten $\det A_{ij}$ auch \emph{$(n-1)$-reihige Unterdeterminanten}.
\end{remark}

\section{Beispiel}\label{sec:19.9}

\begin{example}
    \begin{itemize}
        \item $\det \begin{pmatrix}1 & 2 \\ 3 & 4\end{pmatrix} = {(-1)}^{1+1} \cdot 1 \cdot 4 + {(-1)}^{1+2} \cdot 2 \cdot 3 = 4 - 6 = -2$ für $i=1$.

            $i=2$: ${(-1)}^{2+1} \cdot 3 \cdot 2 + {(-1)}^{2+2} \cdot 4 \cdot 1 = -6 + 4 = -2$.
        \item $\det \begin{pmatrix}1 & 4 & 7\\2 & 5 & 8\\3 & 6 & 10\end{pmatrix} \underbrace{=}_{i=1} {(-1)}^{1+1} \cdot 1 \cdot \underbrace{\det\begin{pmatrix}5 & 8 \\6 & 10\end{pmatrix}}_{5 \cdot 10 - 6 \cdot 8 = 2} + {(-1)}^{1+2} \cdot 4 \cdot \underbrace{\det \begin{pmatrix}2 & 8 \\ 3 & 10\end{pmatrix}}_{2 \cdot 10 - 3 \cdot 8 = -4} + {(-1)}^{1+3} \cdot 7 \cdot \underbrace{\begin{pmatrix}2 & 5 \\ 3 & 6 \end{pmatrix}}_{2 \cdot 6 - 3 \cdot 5 = -3} = \emph{-3}$.
    \end{itemize}
\end{example}

Für die Laplace-Entwicklung von $\det A$ können auch die Spalten genommen werden:

\section{Laplace und Spalten}\label{sec:19.10}

\begin{theorem}
    Für $A \in K^{n \times n}$ mit Spalten $a_1, \ldots, a_n$ und Zeilen $z_1, \ldots, z_n$ gilt

    \[ \det A = \Delta(a_1, \ldots, a_n) = \Delta (z_1, \ldots z_n). \]
\end{theorem}

\begin{proof}
    Nach Satz~\ref{sec:19.2} ist $\det A = \Delta (a_1, \ldots, a_n)$.  Genau so gut können wir $\det A$ als Funktion der Zeilen $z_1, \ldots, z_n$ auffassen, sei diese mit $\Delta'(z_1, \ldots, z_n) := \det A$ bezeichnet. (D.h. setze die Vektoren $z_1, \ldots, z_n$ als Zeilen einer Matrix A zusammen, und bilde $\det A$.) Nach Satz~\ref{sec:19.8} haben wir also $\Delta'(z_1, \ldots z_n) = \sum\limits_{j=1}^n (-1)^{i+j} \alpha_{ij} \det A_{ij}$. Dieser Ausdruck ist offenbar linear in der $i$-ten Zeile. Da $i$ beliebig war, folgt:

    \begin{enumerate}
        \item $\Delta'(z_1, \ldots, z_n) = \det A$ ist linear in jeder Zeile von A.

            Sind die Zeilen von $A$ linear abhängig, so ist $\rg (A) < n$ und damit $\det A = 0.$ Also:
        \item Sind die Zeilen von $A$ linear abhängig, gilt $\Delta'(z_1, \ldots, z_n) = 0$.

            Weiter entsteht durch Einsetzen der Einheitsvektoren als Zeilen die Matrix $I_n$, also:
        \item $\Delta'(e_1, \ldots, e_n) = 1$.
    \end{enumerate}

    Somit ist $\Delta' = \Delta$ \emph{die} normierte Determinantenfunktion nach Satz~\ref{sec:18.2}.
\end{proof}

\section{Beispiel}\label{sec:19.11}

\begin{example}
    Im Bsp.~\ref{sec:19.9}: Entwickeln nach Spalte 2 liefert

    \begin{align*}
        \det\begin{pmatrix}1 & 4 & 7\\2 & 5 & 8\\3 & 6 & 10\end{pmatrix} \underbrace{=}_{j=2} {(-1)}^{1+2} &\cdot 4 \cdot \underbrace{\det\begin{pmatrix}
            2 & 8 \\ 3 & 10
            \end{pmatrix}}_{2 \cdot 10 - 3 \cdot 8 = -4} + {(-1)}^{2+2}\\ 
                             &\cdot 5 \cdot \underbrace{\det\begin{pmatrix}
            1 & 7 \\ 3 & 10
            \end{pmatrix}}_{1 \cdot 10 - 3 \cdot 7 = -11} + {(-1)}^{3+2}\\
                             &\cdot 6 \cdot \underbrace{\det\begin{pmatrix}
            1 & 7 \\ 2 & 8
        \end{pmatrix}}_{1 \cdot 8 - 2 \cdot 7 = -6} = \emph{-3}.
    \end{align*}
\end{example}

\begin{remark}
    Es gibt \emph{ausschließlich für $3 \times 3$} die sog. \emph{Regel von Sarrus} zur $\det$-Berechnung. Wir behandeln diese nicht.
\end{remark}

\section{Nützliche Rechenregeln}\label{sec:19.12}

Wir fassen nützliche Rechenregeln zur Berechnung von $\det A$ zusammen.

\begin{theorem}
    Für eine Matrix $A = (\alpha_{kl}) \in K^{n \times n}$ gilt:

    \begin{enumerate}
        \item Ist $A^T$ die transponierte Matrix zu $A$, gilt $\det A^T = \det A$.
        \item Statt nach einer Zeile kann auch nach einer Spalte Laplace-entwickelt werden: Es gilt für alle $j = 1, \ldots, n$ die Formel $\det A = \sum\limits_{i=1}^n (-1)^{i + j} \alpha_{ij} \det A_{ij}$.
        \item $\det A$ ist linear in jeder Spalte und jeder Zeile von $A$, insb. $\det(\lambda A) = \lambda^n \det A$.
        \item Hat $A$ zwei gleiche Spalten oder zwei gleiche Zeilen, so ist $\det A = 0$.
        \item Addiert man zu einer Spalte ein Vielfaches einer anderen Spalten so ändert sich $\det A$ nicht. Analog für Zeilen.
        \item Vertauscht man zwei Spalten oder zwei Zeilen, so multipliziert sich die Determinante mit $-1$.
        \item Die Determinante einer Dreiecksmatrix (vgl \emph{14.9}) ist gleich dem Produkt der Diagonaleinträge, d.h. $\det \begin{pmatrix}
                \alpha_{11} & \cdots & \alpha_{1n} \\
                 & \ddots & \vdots \\
                0 & & \alpha_{nn}
            \end{pmatrix} = \alpha_{11} \cdot \alpha_{22} \cdots \alpha_{nn}$.
    \end{enumerate}
\end{theorem}

\begin{proof}
    \begin{enumerate}[label=Zu \arabic*. ]
        \item Die Spalten $z_i$ von $A^T$ sind die (transponierten) Zeilen $a_i^T$ von $A$, also ist die Beh. klar nach Satz~\ref{sec:19.10}
        \item folgt aus 1.) und dem Laplace-Entwicklungssatz~\ref{sec:19.8}
        \item[Zu 3. - 6.] folgt aus Satz~\ref{sec:18.2}, Definition~\ref{sec:18.3} zusammen mit Lemma~\ref{sec:18.4}
        \item[Zu 7.] Ist $A = (\alpha_{kl})$ eine obere Dreiecksmatrix, so ist $\alpha_{kl} = 0$ für $k > l$.

            Entwickeln nach der 1. Spalte liefert $\det A = \sum\limits_{i=1}^n (-1)^{i+1} \alpha_{i1} \underbrace{\det A_{i1}}_{= 0 \text{ für } i > 1} = \alpha_{11} \det A_{11}$.

            Da $A_{11}$ selbst wieder obere Dreiecksmatrix ist, ist also induktiv schon $\det A_{11} = \alpha_{22} \cdot \alpha_{33} \cdots \alpha_{nn}$, es folgt $\det A = \prod\limits_{i=1}^n \alpha_{ii}$.
    \end{enumerate}
\end{proof}

\section{Verfahren}\label{sec:19.13}

Dieser Satz~\ref{sec:19.12} liefert ein \emph{Verfahren zur expliziten Berechnung der Determinanten} einer Matrix $A = (\alpha_{ij}) \in K^{n \times n}$. Wir gehen dafür ähnlich vor wie bei der Gauß-Elimination, um die Matrix auf obere Dreiecksform zu bringen, ohne den Wert der Determinante zu ändern.

\paragraph{1. Schrittt} \begin{itemize}
    \item Ist die 1. Spalte von $A$ der Nullvektor $o$, sind die Spalten lin. abh. und $\det A = 0$.
    \item Ist die 1. Spalte $\neq 0$, nehmen wir \OE $\alpha_{11} \neq 0$ an (notfalls Zeilen austauschen und \emph{mal $(-1)$}!).
\end{itemize}

Dann kann man von der $2$-ten, $3$-ten, $\ldots$, $n$-ten Zeile solche Vielfache der $1$-ten Zeile abziehen, dass in der 1. Spalte abgesehen von $\alpha_{11}$, sämtliche Eintrage $= 0$.

Dabei qndert sich der Wert der Determinanten nicht. Die so erhaltene Matrix hat die Form: ... mit $\alpha'_{11} \neq 0$ und nicht näher interessierenden Einträgen $*$.

Ferner haben wir $\det A = (-1)^r \det A' \underbrace{=}_{\text{Laplace-Entwicklung}} (-1)^r \alpha'_{11} \det A'_{11}$, 

\[r = \begin{cases}
    1 & \text{, falls zu Beginn zwei Zeilen vertauscht wurden,}\\
    0 & \text{, sonst.}
\end{cases}\]

\paragraph{2. Schritt} Für $A'_{11}$ argumentiere weiter wie eben im 1. Schritt.

\begin{itemize}
    \item Ist die 1. Spalte von $A$\dots
    \item Ansonsten formen wir $A'$ um wir im 1. Schritt, erhalten anstelle $A$ dann

        \[A'' = \left(
            \begin{array}{ccc}
                \alpha'_{11} & * & \cdots * \\ \cline{1-1}
                \multicolumn{1}{c|}{0} & \alpha'_{22} & * \cdots * \\ \cline{2-3}
                0 & 0 & \\
                \vdots & \vdots & A''_{22} \\
                0 & 0 &
            \end{array}\right)\]

            und $\det A = {(-1)}^r \alpha'_{11} \alpha'_{22} \det A''_{22}$, wobei $r$ die $\#$ bisher vertauschte Zeilen angibt.
\end{itemize}

Durch Iteration dieser Schlusswese erhalten wir das

\section{Rezept zur Berechnung einer Determinantem}\label{sec:19.14}

Führe das folgende \emph{vereinfachte Gauß-Eliminationsverfahren} an der Matrix $A$ durch: Bringe die Matrix mit Zeilenumformungen $(C')$ auf Dreiecksgestalt, Stufenränder werden \emph{nicht} auf $1$ gebracht. Ist $C$ dann die erhaltene Dreiecksmatrix (das kann ja auch eine untere $\Delta$-Matrix sein), so ist $\det A = {(-1)}^r \det C$, wobei $r$ die Anzahl der durchgeführten Zeilenvertauschungen ist. Die Determinante von $C$ berechnet sich dann noch als Produkt der Diagonaleinträge von $C$.

\section{Determinante von Kästchenmatrizen}\label{sec:19.15}

Nützlich zur Berechnung sind Rechenregeln, wenn $A$ in besonderer Form vorliegt, z.B.

\begin{theorem}{(Determinante von Kästchenmatrizen)}
    Ist $A = \left(
        \begin{array}{c|c}
            B & O \\ \hline
            C & I_{n-m}
    \end{array}\right)$ mit Matrizen $B \in K^{m \times m}$, $C \in K^{(n-m) \times m}$, $D \in K^{(n-m) \times (n-m)}$ und der Nullmatrix $O \in K^{m \times (n-m)}$, so gilt $\det A = \det B \cdot \det D$.
\end{theorem}

\begin{proof}
    Haben $A = \begin{bmatrix}
        B & 0 \\ C & I_{n-m}
    \end{bmatrix} \cdot \begin{bmatrix}
        I_m & O \\ O & D
    \end{bmatrix}$, dann $\det$ anwenden und Satz \emph{19.3(2)}.
\end{proof}

\section{Bemerkung}\label{sec:19.16}

\begin{remark}
    Der Satz gilt natürlich ebenso für $A = \left(
        \begin{array}{c|c}
            B & C \\ \hline
            O & D
    \end{array}\right)$. Nach Betrachtung von $\det A^T$.
\end{remark}

\section{Anwendungen der Determinantentheorie}\label{sec:19.17}

\begin{definition}{Algebraisches Komplement}
    Sei $A = (a_{kl}) \in K^{n \times n}$ und $A_{ij}$ die Streichungsmatrix zu Zeile $i$, Spalte $j$, so heißt \emph{$\theta_{ij}(A) := {(-1)}^{i+j} \det A_{ij}$} das \emph{algebraische Komplement von $\alpha_{ij}$}.
\end{definition}

\section{Satz}\label{sec:19.18}

\begin{theorem}
    Sei $A = (\alpha_{kl}) \in K^{n \times n}$, $x = {(\xi_1, \ldots, \xi_n)}^T \in K^n$ ein Vektor und $A_{j,x}$ sei die Matrix, die aus $A$ durch Ersetzen der $j$-ten Spalte durch $x$ entsteht. Dann gilt $\det A_{j,x} = \sum\limits_{i=1}^n \xi_i \theta_{ij} (A)$. \hfill (Analoges gilt für die Zeilen.)
\end{theorem}

\begin{proof}
    Entwickle $\det A_{j,x}$ laut \ref{sec:19.12}(2) nach der $j$-ten Spalte.
\end{proof}

\section{Lemma}\label{sec:19.19}

\begin{lemma}
    Ist $A = (\alpha_{k,l}) \in K^{n \times n}$, $a_k$ die $k$-te Spalte von $A$, so ist für $j=1,\ldots,n$: $\sum\limits_{i=1}^n \alpha_{ik} \theta_{ij}(A) = \begin{cases}
        \det A, j=k,\\
        0, \text{ sonst}
    \end{cases}$ \hfill (Analoges für Zeilen)
\end{lemma}

\begin{proof}
    Setze in~\ref{sec:19.18} $x=a_k$ ein und erhalte $\det A_{j,a_k}$, wo man $A_{j, a_k}$ erhält, indem man in $A$ die $j$-te Spalte durch die $k$-te Spalte ersetzt. Für $j=k$ tut sich dabei nichts und erhält $A$ selbst, für $j \neq k$ erhält man eine Matrix mit zwei gleichen Spalten, deren Determinante $=0$ ist.
\end{proof}

Damit kommen wir zur

\section{Geschlossene Darstellung der Inversen einer Matrix}\label{sec:19.20}

\begin{theorem}
    Ist $A \in K^{n \times n}$ invertierbar und $\theta_{ij}$ das algebraische Komplement, so ist

    \begin{center}
        \boxed{A^{-1} = \frac{1}{\det A} \cdot {(\theta_{ij})}^T},
    \end{center}

    d.h. nimmt man $\theta_{ij}$ als Matrixeintrag in Zeile $i$, Spalte $j$, bildet davon die transponierte Matrix und multipliziert mit $\frac{1}{\det A}$, erhält man $A^{-1}$.
\end{theorem}

\section{Beispiel}\label{sec:19.21}

\begin{example}
    Hatten $\begin{pmatrix}
            a & b \\ c & d
        \end{pmatrix}^{-1} = \frac{1}{\det A} \cdot \underbrace{\begin{pmatrix}
            d & -b \\ -c & a
            \end{pmatrix}}_{\left(\begin{smallmatrix}
            \theta_{11} & \theta_{12} \\ \theta_{21} & \theta_{22}
\end{smallmatrix}\right)^T}$, wo 

    \[\theta_{11} = {(-1)}^{1+1} \cdot \det(d) = d,\]
    \[\theta_{12} = {(-1)}^{1+2} \cdot \det(c) = -c,\] 
    \[\theta_{21} = {(-1)}^{2+1} \cdot \det(b) = -b,\]
    \[\theta_{22} = {(-1)}^{2+2} \cdot \det(a) = a.\]

    Jetzt $3 \times 3$-Matrix:

    \[\begin{pmatrix}
        a & b & c\\d & e & f\\g & h & i
    \end{pmatrix}^{-1} = \frac{1}{\det A} \cdot \begin{pmatrix}
        \theta_{11} & \theta_{21} & \theta_{31}\\
        \theta_{12} & \theta_{22} & \theta_{32}\\
        \theta_{13} & \theta_{23} & \theta_{33}
    \end{pmatrix} \text{ mit} \]

    \[ \theta_{11} = {(-1)}^{1+1} \cdot \det\begin{pmatrix}
        e & f \\ h & i
    \end{pmatrix}, \]
    \[ \theta_{12} = {(-1)}^{1+2} \cdot \det\begin{pmatrix}
        d & f \\ g & i
    \end{pmatrix}, \text{ usw.}\]
\end{example}

\section{Lösen eines LGS mit Determinantentheorie im Fall $n=m$}\label{sec:19.22}

\paragraph{Cramersche Regel:} Ist $\det A \neq 0$, so ist die Lösung $x$ von $Ax = b, A \in K^{n \times n}$ gegeben durch $x = \begin{pmatrix}
    \xi_1 \\ \vdots \\ \xi_n
\end{pmatrix}$ mit \emph{$\xi_i = \frac{1}{\det A} \cdot \det A_{i,b}$} für $i=1, \ldots, n$.

\emph{Aber:} Konkretes Berechnen der Lösung damit ungeeignet / zu aufwendig, weil Determinantenberechnung zu aufwendig! Lieber Gaußelim. in der Praxis nehmen!

\begin{proof}
    Mit~\ref{sec:19.20} folgt $x = A^{-1} b = \frac{1}{\det A} \cdot (\theta_{ij})^T \cdot b$, und ist $\xi_l \in K$ die $l$-te Komponente der Lösung $x$, ist $\xi_l \cdot \det A$ also gleich der $l$-ten Komponente des Vektors $(\theta_{ij})_{ij}^T \cdot b = (\theta_{ij})_{ij} \cdot b$,

    nämlich $\sum\limits_{j=1}^n \theta_{jl} \beta_j = \det A_{l, b}$ wegen Satz \emph{19.18}.

    Jetzt $\xi_l \det A = \det A_{l, b}$ nach $\xi_l$ auflösen.
\end{proof}

\section{Beispiel}\label{sec:19.23}

\begin{example}
    Das LGS $Ax = b$ mit $A = \begin{pmatrix}
        1 & 2 \\ 3 & 4
    \end{pmatrix}$, $b = \begin{pmatrix}
        5 \\ 6
    \end{pmatrix}$, $\det A = 1 \cdot 4 - 3 \cdot 2 = -2$ hat den Lösungsvektor $x = \begin{pmatrix}
        \xi_1 \\ \xi_2
        \end{pmatrix}$ mit 

        \[ \xi_1 = \frac{\det {\left(\begin{smallmatrix} 5 & 2 \\ 6 & 4 \end{smallmatrix}\right)}}{-2} = \frac{1}{2} \cdot (5 \cdot 4 - 6 \cdot 2) = -\frac{1}{2} \cdot 8 = -4, \]

        \[ \xi_2 = \frac{\det {\left(\begin{smallmatrix} 1 & 5 \\ 3 & 6 \end{smallmatrix}\right)}}{-2} = \frac{1}{2} \cdot (1 \cdot 6 - 3 \cdot 5) = \frac{9}{2}, \]

        Probe:

        \[ 1 \cdot (-4) + 2 \cdot \frac{9}{2} = 5 \checkmark \]
        \[ 3 \cdot (-4) + 4 \cdot \frac{9}{2} = 6 \checkmark \]
\end{example}

\section{Elementaregeometrische Anwendung}\label{sec:19.24}

Sind $a, b \in K^2$, $a \neq b$, so liegt ein $x \in K^2$ genau dann auf der Geraden durch $a$ und $b$, wenn gilt: $\det \begin{pmatrix}
    x & a & b \\ 1 & 1 & 1
\end{pmatrix} = 0$.

\begin{proof}
    \begin{itemize}
        \item Gilt $x = \alpha a + \beta b$ mit $\alpha + \beta = 1$, sind $\begin{pmatrix}
            x \\ 1
        \end{pmatrix},\begin{pmatrix}
            a \\ 1
        \end{pmatrix},\begin{pmatrix}
            b \\ 1
        \end{pmatrix} \in K^2$ linear abhängig, also $\det(\dots) = 0$.
        \item Gilt $\det \begin{pmatrix}
    x & a & b \\ 1 & 1 & 1
\end{pmatrix} = 0$, sind die Vektoren linear abhängig, d.h. $\exists \xi, \alpha, \beta \in K$, nicht alle $=0$, mit $\xi x = \alpha a + \beta b$ und $\xi = \alpha + \beta$. Falls $\xi = 0$, wäre $a = b ~\lightning$. Man darf \OE $\xi = 1$ annehmen.
    \end{itemize}
\end{proof}
