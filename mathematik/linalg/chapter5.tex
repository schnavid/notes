\chapter{Relationen}

\paragraph{Stichworte} Kartesisches Produkt, Relation, Eigenschaften von Relationen, Äquivalenzrelation und -klasse, Quotientenmenge, Beispiele, Def. Abbildung/Funktion

\section{Mengen und Paare}

Die Elemente einer Menge haben keine bestimmte Reihenfolge,

\[
  \text{z.B. } \{1,2\} = \{2,1\} = \{2,1,1\} = \{2,1,2,2\}
\]

Man möchte aber auch eine Reihung von Elementen (die im Sinne der Mengenleere auch selbst wieder Mengen sind) haben, speziell \emph{Paare} von Mengen $(x, y)$ betrachten können, wo $x$ die erste Menge (= 1. Eintrag des Paars) und $y$ die zweite Menge (= 2. Eintrag des Paars) sein soll.

Dabei soll $(x, y) = (u, v)$ gena dann gelten, wenn $x = u$ und $y = v$ gilt.

Dies wird mit der Kuratowski-Konstruktion gelöst:

\begin{center}
  \fbox{\begin{minipage}{\textwidth}
      Wir setzen $(x, y) := \{ \{x\}, \{x, y\}\}$.

      Denn $(x, y) = (u, v)$ bedeutet dann ja $\{\{x\}, \{x, y\}\} = \{\{u\}, \{u,v\}\}$.

      Daraus folgen wir:
      \begin{itemize}
        \item Ist $\{x\} = \{u\}$, so ist $x=u$ und $\{x, y\} = \{u, v\} = \{x, v\}$, also $v = y$.
        \item Ist $\{x, y\} = \{u\}$, so ist $u = x = y$ und $\{x, y\} = \{u\} = \{x\}$, also die Menge einelementig, also $v = u = x = y$.
      \end{itemize}

      Man muss sich diese Konstruktion nicht merken.

      Für uns ist nur wichtig, dass man in der Sprache der Mengen Paare $(x, y)$ mit der gewünschten Eigenschaft $(x, y) = (u, v) \Leftrightarrow x = u \land y = v$ bilden kann.
    \end{minipage}
  }
\end{center}

\section{Kartesisches Produkt}

\begin{definition}{Kartesisches Produkt}
  Sind $X$ und $Y$ Mengen, so besteht ihr \emph{Kartesisches Produkt} $X \times Y$ aus allen Paaren $(x, y)$ mit $x \in X$ und $y \in Y$, d.h.

  \hfill$X \times Y := \{(x, y); x \in X \land y \in Y\}$.\hfill(sprich ``$X$ kreuz $Y$'')

  Graphisch:

  ``Kartesisches Koordinatensystem''

  \begin{tikzpicture}
    \draw (0, 0.5) -- (0, 1.5) node[above]{$Y$};
    \draw (0.5, 0) -- (2.5, 0) node[right]{$X$};
    \fill[red] (0.5,0.5) rectangle (2.5, 1.5);
    \node[red] at (2.75, 1.75) {$X \times Y$};
  \end{tikzpicture}

  Bsp: $\mathbb{R} \times \mathbb{R}$ ist eine Ebene, deren Punkte zwei reelle Koordinaten haben.
\end{definition}

\section{Achtung}

Aus $X = X_{1} \cup X_{2}$ und $Y = Y_{1} \cup Y_{2}$ folgt noch lange nicht $X \times Y = (X_{1} \times Y_{1}) \cup (X_{2} \times Y_{2})$:

\begin{tikzpicture}
  \draw[<->] (-1.25, 3) -- (-1.25, 0);
  \node[anchor=east] at (-1.25, 1.5) {$Y$};
  \draw[<->] (-0.5, 3) -- (-0.5, 1);
  \node[anchor=east] at (-0.5, 2) {$Y_{1}$};
  \draw[<->] (5.5, 2) -- (5.5, 0);
  \node[anchor=west] at (5.5, 1) {$Y_{2}$};
  \draw[<->] (0, -0.5) -- (3, -0.5);
  \node[anchor=north] at (1, -0.5) {$X_{1}$};
  \draw[<->] (2, -0.75) -- (5, -0.75);
  \node[anchor=south] at (4, -0.75) {$X_{2}$};
  \draw[<->] (0, -1.25) -- (5, -1.25);
  \node[anchor=north] at (2.5, -1.25) {$X$};
  \draw[thick] (0, 0) rectangle (5, 3);
  \fill[fill=red] (0, 3) rectangle (3, 1);
  \fill[fill=red] (2, 2) rectangle (5, 0);
\end{tikzpicture}

Übung: Konstruieren Sie hierfür ein nichtgraphisches formal richtiges Gegenbeispiel!

\section{Mehrfache Produkte}

Für drei Mengen $X_{1}, X_{2}, X_{3}$ haben wir

\begin{align*}
  \left(X_{1} \times X_{2}\right) \times X_{3} &= \left\{ \left(\left(x_{1}, x_{2}\right), x_{3}\right) | x_{1} \in X_{1}, x_{2} \in X_{2}, x_{3} \in X_{3} \right\}\\
  \text{sowie } X_{1} \times \left(X_{2} \times X_{3}\right) &= \left\{ \left(x_{1}, \left(x_{2}, x_{3}\right)\right) | x_{1} \in X_{1}, x_{2} \in X_{2}, x_{3} \in X_{3} \right\}\\
\end{align*}

Das ist zunächst ein formaler Unterschied, letztlich ist es aber egal, wie herum gruppiert wird, die Reihenfolge von $x_{1}, x_{2}, x_{3}$ steht fest. Wir nennen deswegen etwa $(x_{1}, x_{2}, x_{3}) := ((x_{1}, x_{2}), x_{3})$ ein \emph{Tripel} und bezeichnen in diesem Sinne mit $X_{1} \times X_{2} \times X_{3} := \left\{ \left(x_{1}, x_{2}, x_{3}\right) | x_{1} \in X_{1}, x_{2} \in X_{2}, x_{3} \in X_{3} \right\}$ die Menge der \emph{Tripel}. Analog erhält man mit 4 Mengen \emph{Quadrupel} usw.

Das heißt über die Rekursion $(x_{1}, x_{2}, \dots, x_{n-1}, x_{n}) := ((x_{1}, \dots, x_{n-1}), x_{n})$ für $n\in\mathbb{N}$ können wir \emph{$n$-Tupel} definieren.

Die Gleichheit zweier $n$-Tupel gilt dann ebenfalls Komponentenweise.

Wir nennen $X_{1} \times \dots \times X_{n}$ das \emph{Kartesische Produkt} der Mengen $X_{1}, \dots, X_{n}$, schreiben auch $\prod\limits_{i=1}^{n}X_{i}$ dafür, gelegentlich wird $\bigtimes\limits_{i=1}^{n}X_{i}$ dafür geschrieben.

Im Falle $X_{1} = \dots = X_{n} = X$ wird auch $X^{n}$ geschrieben, also z.B. $\mathbb{R}^{2} = \mathbb{R} \times \mathbb{R}$, $\mathbb{R}^{3} = \mathbb{R} \times \mathbb{R} \times \mathbb{R}$ usw.

\section{Kartesisches Produkt und Relationen}

Mit dem kartesischen Produkt können nicht nur (mengentheoretisch) neue mathematische Objekte gebildet werden, andererseits dient es zur Definition des \emph{Relationsbegriffs} (ein spezialfall einer Relation ist die Abbildung/Funktion, vgl. 5.19).

\section{$n$-stellige Relation}

\begin{definition}{$n$-stellige Relation}
  Jede Teilmenge $R \subseteq X_{1} \times \dots \times X_{n}$ von Mengen $X_{1}, \dots, X_{n}$ heißt \emph{$n$-stellige Relation}.
\end{definition}

Von besonderer Bedeutung sind die zweistelligen Relationen $R \subseteq X \times Y$.

\section{Beispiel}

$X=\{1,2\}$, $Y=\{1,3,5\}$, $R := \{(1,3), (1,5), (2,3), (2,5)\}$ ist die Relation ``$<$'', dabei ist $x < y \Leftrightarrow (x,y) \in R$. (identifiziere $R$ mit $<$)

Konvention: Schreiben \emph{$x R y$} für die Aussage $(x, y) \in R$.

\section{Beispiele für Relationen}

Beispiele für Relationen sind ``$\leq$'' und ``$=$'' in $\mathbb{N}$, ``$\subseteq$'' in Mengensystemen, ``$\bot$'' (Senkrechtstellen) in der Menge aller Geraden, ``$/$'' (teilen) in $\mathbb{Z}$, \dots

Im folgenden steht $R$ im abstrakten Sinne für so eine Relation (``Vergleich'') von Elementen einer menge $X$ mit denen einer anderen Menge $Y$ (laut Definition wie oben). Oft ist $X=Y$, dann sagt man, es liegt eine zweistellige Relation in $X(=Y)$/ auf $X$ vor.

\section{Eigenschaften von Relationen $R \subseteq X \times Y$}

\begin{enumerate}[label=(\arabic*)]
  \item (F1) $R$ \emph{linkstotal} $:\Leftrightarrow \forall x\in X \exists y \in Y: x R y$
  \item $R$ \emph{rechtstotal} $:\Leftrightarrow \forall y \in Y \exists x \in X : x R y$
  \item $R$ \emph{bitotal} $:\Leftrightarrow \text{(1)} \land \text{(2)}$
  \item $R$ \emph{linkseindeutig} $:\Leftrightarrow \forall x \in X ~\forall y \in Y ~\forall u \in X : xRy \land uRy \Rightarrow x = u$
  \item (F2) $R$ \emph{rechtseindeutig} $:\Leftrightarrow \forall x \in X ~\forall y \in Y ~\forall v \in Y : xRy \land xRv \Rightarrow y = v$
  \item $R$ \emph{eineindeutig} $:\Leftrightarrow \text{(4)} \land \text{(5)}$
\end{enumerate}

\section{Eigenschaften von Relationen $R \subseteq X \times X$}

\begin{enumerate}[label=(\arabic*)]
  \setcounter{enumi}{6}
  \item (Ä1) $R$ \emph{reflexiv} $:\Leftrightarrow \forall x \in X : x R x$

    $R$ enthält die ``Diagonale'' $\{(x, x); x \in X\}$
  \item (Ä2) $R$ \emph{symmetrisch} $:\Leftrightarrow \forall x, y \in X: xRy \Rightarrow y Rx$

    $R$ ist symmetrisch zur ``Diagonalen''
  \item $R$ \emph{asymmetrisch} $:\Leftrightarrow \forall x, y \in X : xRy \Rightarrow \lnot(yRx)$

    $R$ ohne Diagonale und ohne symmetrische Paare
  \item $R$ \emph{antisymmetrisch/identitiv} $\Leftrightarrow \forall x, y \in X: xRy \land yRx \Rightarrow x=y$

    Symmetrische Paare hat R nur auf der Diagonalen
  \item $R$ \emph{konnex/linear} $:\Leftrightarrow \forall x, y \in X: xRy \lor yRx$

    Je zwei Elemente können verglichen werden
  \item (Ä3) $R$ \emph{transitiv} $:\Leftrightarrow \forall x, y, z \in X : xRy \land yRz \Rightarrow xRz$
\end{enumerate}

\section{Besondere Arten von Relationen}

\begin{itemize}
  \item (7), (8), (12) definiert eine \emph{Äquivalenzrelation}
  \item (1), (5) definiert eine \emph{Abbildung/Funktion}
  \item (7), (10), (12) definiert eine (Halb)ordnung
  \item (7), (10), (12), (11) definiert eine \emph{Anordnung/lineare Ordnung}
  \item Ist $X$ eine Menge mit einer (Halb)ordnungsrelation ``$\leq$'', dann heißt $m \in X$ ein \emph{maximales Element} von $X$, falls $\forall x \in X : m \leq x \Rightarrow x = m$
\end{itemize}

\section{Äquivalenzrelationen und Äquivalenzklassen}

\begin{definition}{Äquivalenzrelation}
  Sei $X$ eine Menge. Eine Relation $R \subseteq X \times X$ in $X$ heißt eine \emph{Äquivalenzrelation} (hier kurz \emph{Ä-Rel}), wenn sie:

  \begin{align*}
    &\text{\emph{reflexiv}}  && \text{(Ä1)}: \forall x \in X : xRx\\
    &\text{\emph{symmetrisch}} && \text{(Ä2)}: \forall x, y \in X : xRy \Rightarrow yRx\\
    \text{und } &\text{\emph{transitiv}} && \text{(Ä3)}: \forall x, y, z \in X : xRy \land yRz \Rightarrow xRz
  \end{align*} ist.
\end{definition}

Übliche Zeichen für Ä-Relationen sind $\sim, \simeq, \equiv, \cong, \approx$, etc. (die in bestimmten Kontexten aber meist auch spezielle Bedeutungen haben). Wir wollen hier $\sim$ (sprich ``Tilde'') für eine Ä-Relation schreiben, also $x \sim y$ für $(x, y) \in R$.

\section{Beispiele}

\begin{enumerate}
  \item ``$=$'' Gleichheit bei Mengen, Zahlen, Geraden, \dots
  \item ``ist gleich alt wie'' in der Menge der Menschen
  \item ``ist im gleichen Bierkasten'' in der Menge der Bierflaschen
  \item ``ist parallel zu'' in der Menge der Geraden
  \item ``ist kongruent zu'' in der Menge der Strecken
  \item ``hat gleiche Parität wie'' in $\mathbb{N}_{0}$ ($n$, $m$ haben dieselbe Parität, wenn sie beide gerade oder beide ungerade sind)
  \item ``hat dieselbe Richtung und Länge'' in der Menge der Pfeile in einer Ebene
  \item ``hat dieselbe Krümmung wie'' in der Menge der Bananen
\end{enumerate}

\section{Äquivalenzklassen}

Jede Ä-Rel. $\sim$ in $X$ zerlegt $X$ in paarweise disjunkte, nichtleere Teilmengen, auch Klasseneinteilung genannt:

\begin{definition}{Äquivalenzklasse}
  Die Menge aller zu $X \in X$ bzgl. $\sim$ in Relation stehende Elemente von $X$ heißt \emph{Äquivalenzklasse} und wird mit $[x]$ bezeichnet, d.h. $[x] := \{y \in X | y \sim x\}$

  Andere Notationen: $\llbracket x \rrbracket$, $\bar{x}$, \dots
\end{definition}

\begin{remark}
  \begin{enumerate}
    \item $[x]$ ist demnach die Äquivalenzklasse, die $x$ enthält: $x \in [x]$, sodass $[x] \neq \emptyset$ folgt (denn $\sim$ ist reflexiv)
    \item Es gilt: $\forall x, y \in X: x \sim y \Leftrightarrow [x] = [y]$ \hfill (obwohl hier $x \neq y$ gelten kann!)

      ``$\Rightarrow$'': Sei $x \sim y$. Zu ``$\subseteq$'': ist $z \in [x]$, folgt $z \sim x$, mit $x \sim y$ folgt aus der Transitivität $z \sim y$, also ist $z \in [y]$. Zu ``$\supseteq$'' analog.

      ``$\Leftarrow$'': Sei $[x] = [y]$. Dann ist $x \in [x] = [y]$, also $x \sim y$.

    \item Es gilt: $\forall x, y, z \in X: z \in [x] \land z \in [y] \Rightarrow [x] = [y]$. \hfill (Da $x \sim z \sim y$, also $x \sim y$)

      \begin{align*}
        \text{Umformuliert: } & (\forall z \in [x] \land z \in [y] \Rightarrow [x] = [y])\\
        \Leftrightarrow & \lnot (\exists z \in X: z \in [x] \cap [y] \land [x] \neq [y])\\
        \Leftrightarrow & \lnot ([x] \cap [y] \neq \emptyset \land [x] \neq [y])\\
        \Leftrightarrow & ([x] \neq [y] \Rightarrow [x] \cap [y] = \emptyset)
      \end{align*}

      Zwei verschiedene Ä-Klassen sind also \emph{disjunkt}, d.h. ihr Durchschnitt ist $\emptyset$. (Anders ausgedrückt: Jedes Element $z$ gehört zu genau einer Ä-Klasse)

    \item Die Vereinigung aller Ä-Klassen ergibt $X$, d.h. $X=\bigcup\limits_{x \in X}[x]$

      ``$\subseteq$'': Ist $x \in X$, folgt $x \in [x]$, also $x \in \text{r.S.}$, ``$\supseteq$'': klar

    \item Obige Überlegungen zeigen: $x \sim y \Leftrightarrow [x] = [y] \Leftrightarrow [x] \cap [y] \neq \emptyset$.
  \end{enumerate}
\end{remark}

\section{Quotientenmengen}

Eine Ä-Rel. erzeugt also eine neue interessante Menge, die Menge aller Ä-Klassen:

\begin{definition}{Quotientenmenge}
  Sei $\sim$ eine Ä-Rel. in $X$. Dann heißt

  \[
    X/\sim := \{[x] | x \in X\} \text{(sprich ``$X$ modulo $\sim$'' auch ``$X$ durch $\sim$'')}
  \]

  die \emph{Quotientenmenge} von $X$ nach $\sim$.

  \begin{itemize}
    \item Ein (jedes) Element $y \in [x]$ heißt \emph{Repräsentant} der Klasse [x].
    \item Eine Menge $R \subseteq X$ heißt \emph{vollständiges Repräsentantensystem} von $X/\sim$, Wenn $R$ genau ein Element jeder Klasse von $X/\sim$ enthält.
  \end{itemize}
\end{definition}

\section{Beispiele}

In den obigen Beispielen 5.13 1.-8. haben wir:

\begin{enumerate}
  \item Ä-Kl.: Die für jedes $x$ zugehörige Ä-Klasse ist $[x] = \{x\}$.

    Also: $X/\sim = \{\{x\} | x \in X\}$ könnte mit $X$ identifiziert werden, denn wir kriegen nichts neues! Weiter: Nur $R=X$ ist vollständig Repräsentantensystem.
  \item Alle Menschen mit gleichem Lebensjahr bilden jeweils eine Ä-Klasse.
  \item Die Bierkästen, zusammen mit der Menge der Bierflaschen, die in keinem Kasten sind, sind genau die Ä-Klassen.

    Letztlich erzeugt jede Partition $X = \bigcup\limits_{i=1}^{n}X_{i}$ (alle $X_{i} \neq \emptyset$, und $\forall i \neq j: X_{i} \cap X_{j} = \emptyset$) von $X$ eine Ä-Rel. $\sim$ in $X$ durch $x \sim y :\Leftrightarrow \exists i \in \{1, \dots, n\}: x \in X_{i} \land y \in X_{i}$
  \item Alle Geraden mit gleicher Richtung gehören zu einer Ä-Klasse. Man kann sagen, über die Parallelität / Ä-Klassenbildung kann ``Richtung'' erklärt werden.
  \item Alle Strecken mit gleicher Länge gehören zu einer Ä-Klasse. Man kann sagen, über die Kongruenz / Ä-Klassenbilfung kann ``Länge'' erklärt werden.

    (Alle zum ``Urmeter'' gleich langen Stäbe sind 1m lang per Definition)
  \item Haben $\mathbb{N}_{0}/\sim = \{G, U\}$, $G = \{2n | n \in \mathbb{N}_{0}\}$ die geraden und $U = \{2n + 1 | n \in \mathbb{N}_{0}\}$ die ungeraden Zahlen sind.

    Haben insb. $\mathbb{N}_{0} = G \dot{\cup} U$. Wie können mit Elementen von $\mathbb{N}_{0}/\sim$ ``rechnen'': erklären dies repräsentanten: $G + G := G$, $G + U := U + G := U$, $U + U = G$, $G * G := G$, $G * U := U * G := G$, $U * U = U$

    Haben ja: $G = [0]$, $U = [1]$,

    und $[0] + [0] := [0]$, aber auch $[4] + [6] := [10] = [0]$, \dots

    und $[0] + [1] := [1]$, aber auch $[4] + [7] := [11] = [1]$, \dots

    usw. Wir werden diese Idee später in L7 vertiefen.
  \item Die Ä-Klassen sind die Vektoren, welche jeweils durch einen Pfeil repräsentiert werden, der vom Ursprung aus beginnt. Diese repräsentierenden Pfeile werden durch ihren Endpunkt an der Pfeilspitze dargestellt, den wir mit einem Tupel angehen.
  \item Alle Bananen gleicher Krümmung gehören zu einer Ä-Klasse. Den Begriff ``Krümmung'' kann man mit Mitteln der Analysis präzisieren.
\end{enumerate}

\section{Relationen in $X \times Y$}

Wir möchten nun auch Relationen $R \subseteq X \times Y$ mit zwei unterschiedlichen Mengen untersuchen, also Elemente von $X$ mit denen von $Y$ ``vergleichen'', \.B. $R \subseteq \mathbb{N} \times \mathbb{R}$, $R \subseteq \{\text{Geraden}\} \times \{Vektoren\}$, $R \subseteq \{\text{Äpfel} \times \{\text{Birnen}\}\}$, \dots Wenn die beiden Mengen sehr unterschiedlich sind, würde man eher nicht mehr von ``Vergleich'', sondern von ``Zuordnung'' sprechen, sodass man auf diesem Wege zum Begriff der Abbildung kommt. Relationen stellen i.a. keine eindeutigen Beziehungen zwischen Mengen her d.h. einem Element in $X$ können durchaus mehrere Elemente aus $Y$ zugeordnet sein. Von einer Abbildung wird verlangt, dass sie die ganze Menge $X$ eindeutig in die Menge $Y$ abbilden: Jedem $x \in X$ wird genau ein $y \in Y$ zugeordnet.

\section{Abbildungen/Funktionen}

\begin{definition}{Abbildung}
  Eine linkstotale (F1), rechtseindeutige (F2) Relation $f \subseteq X \times Y$ heißt \emph{Abbildung} (auch: \emph{Funktion}, kurz Abb./Fkt.).

  Notation: Statt $xfy$ bzw. $(x,y) \in f$ schreibt man auch $x \mapsto y$ oder $x \mapsto f(x)$ mit $f(x) = y$. Statt $f \subseteq X \times Y$ wird $f: X \to Y$ oder $X \stackrel{f}{\to} Y$ geschrieben
\end{definition}

\begin{remark}
  Eine Abbildung ist - als Menge - dasselbe wir ihr Graph/Schaubild. Diese Definition mi Mengen muss man sich nicht merken, vielmehr ist ihre Eigenschaft wichtig, dass jedem $x\in X$ ein zugehöriges $y \in Y$ \emph{eindeutig} zugeordnet wird. Die Bezeichnung $f(x)$ macht die Abhängigkeit des Elements $y=f(x)$ von $x$ deutlich.
\end{remark}
