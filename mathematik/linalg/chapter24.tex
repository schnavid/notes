\chapter{Orthonormalbasen}\label{ch:24}

\paragraph{Stichworte} (B)O(N)S, O(N)B, Schmidt-Orthogonalisierung, orthogonales Komplement, Proximum, approximative Lösung eines unlösbaren LGS, adjungierte Matrix, Normaleglg.

\section{Orthogonalität}\label{sec:24.1}

Sei $V$ ein unitärer Raum und $\langle \cdot, \cdot \rangle$ ein Skalarprodukt auf $V$.

\begin{definition}
    \begin{itemize}
        \item $x, y \in V$ heißen \emph{orthogonal}, falls $\langle x, y \rangle = 0$. Dabei ist $x = o$ und / oder $y = o$ zugelassen. Schreiben: \emph{$x \perp y$}.
        \item $S \subseteq V$, so heißt $S^\perp = \{x \in V; \langle x, y \rangle = 0 \text{ für alle $y \in S$}\}$ das \emph{orthogonale Komplement} von $S$ in $V$.
        \item Ist $S \subseteq V$, so heißt $S$ \emph{Orthogonalsystem} von $V$ bzgl. $\langle \cdot,\cdot \rangle$ (\emph{OS}), falls $\forall x, y \in S, x \neq y: \langle x, y \rangle = 0$
        \item Ist $S \subseteq$ ein OS, so heißt $S$ \emph{Orthonormalsystem} (\emph{ONS}), falls $\forall x, \in S: \langle x, x \rangle = 1$.
        \item Ist $S \subseteq V$ Basis von $V$ und Orthogonalsystem, so heißt $S$ \emph{Orthonormalbasis}.
        \item Ist $S \subseteq V$ orthogonalbasis von $V$ mit $\langle x, x \rangle = 1$ für alle $x \in S$, so heißt $S$ \emph{Orthogonalbasis} (\emph{ONB}). (\enquote{normal} = \enquote{normiert})
    \end{itemize}
\end{definition}

\section{Bemerkung}\label{sec:24.2}

\begin{itemize}
    \item Zwei Familien $(x_i)_{i \in I}$ und $(y_j)_{j \in I}$ in $V$ heißen \emph{Biorthonormalsystem} (\emph{BONS}), wenn für alle $i, j \in I$ gilt: $\langle x_i, y_j \rangle = \delta_{i,j} = \begin{cases} 1, & i = j, \\ 0, & i \neq j \end{cases}$
    \item Wir nennen sie \emph{Biorthogonalsystem} (\emph{BOS}), wenn

        \[\forall i, j \in I: \langle x_i, y_j \rangle \begin{cases} \neq 0, & i = j \\ = 0, & i \neq j, \end{cases}\]

        gilt.
    \item Eine Familie von Vektoren in $V$ bildet ein ONS, wenn sie mit sich selbst ein BONS bildet. (Sie bildet ein OS, wenn sie mit sich selbst ein BOS bildet.)
    \item im $\mathbb{R}^n$ gilt für das Standard-S.P. $\langle x, y \rangle = \sum\limits_{k=1}^n x_k y_k$, dass $\langle x, y \rangle = 0$ ist genau wenn $x$ und $y$ senkrecht / orthogonal aufeinander stehen (Kosinussatz~\ref{sec:23.7}).
    \item Werden manche der Vektoren eines OS mit Skalaren $\neq 0$ multipliziert, bleibt die so veränderte neue Vektormengeein OS.
    \item Ein Vektor $x$ mit $\langle x, x \rangle = 1$ heißt \emph{normiert}.
\end{itemize}

\section{Satz}\label{sec:24.3}

\begin{theorem}
    \begin{enumerate}[label=(\arabic*)]
        \item Bilden $(x_i)_{i \in I}, (y_j)_{j \in I}$ ein BOS in $V$, so sind je endlich viele der $x_i$ bzw. der $y_i$ linear unabhängig.
        \item In einem OS $(x_i)_{i \in I}$ sind je endlich viele Vektoren linear unabhängig.
    \end{enumerate}
\end{theorem}

\begin{proof}
    \begin{enumerate}[label=(\arabic*)]
        \item Sei $(x_1, \dots, x_n)$ und $(y_1, \dots, y_n)$ ein solcher endlicher Teil des BOS. Wir betrachten eine LK $o = \sum\limits_{i=1}^n \lambda_i x_i$ und bilden des S.P. mit $y_j$ zu einem festen $j$, so folgt $o = \langle o, y_j \rangle = \left\langle \sum\limits_{i=1}^n \lambda_i x_i, y_j \right\rangle = \sum\limits_{i=1}^n \lambda_i \underbrace{\langle x_i, y_j \rangle}_{=\delta_{i,j}} = \lambda_j$. Da $j$ beliebig war, folgt, dass $\lambda_1 = \dots = \lambda_n = 0$ ist, dann sind $x_1, \dots, x_n$ also linear unabhängig.
        \item Folgt aus (1), da ein ONS ein BONS mit sich selbst bildet. \hfill \qedhere
    \end{enumerate}
\end{proof}

\section[Schmidtsches Orthogonalisierungsverfahren]{Schmidtsches Orthogonalisierungsverfahren / Orthogonalisierung nach E. Schmidt}\label{sec:24.4}

Zur Existenz von ONSen gibt der folgende wichtige Satz eine positive Antwort in Form einer expliziten Konstruktion: \hfill (auch: \emph{Gram-Schmidt})

\begin{theorem}
    Sei $V$ ein unitärer Raum mit Skalarprodukt $\langle \cdot, \cdot \rangle$.

    Sei $(v_1, v_2, \dots)$ eine (endliche oder abzählbar unendliche) linear unabhängige Familie von Vektoren in $V$. Durch

    \[\begin{array}{|c c c c c c c c c c}
        b_1 & := & v_1, &&&&& \multicolumn{1}{c|}{}\\
        b_2 & := & v_2 & - & \frac{\langle v_2, b_1\rangle}{\langle b_1, b_1 \rangle} \cdot b_1 &&& \multicolumn{1}{c|}{} && \text{ dann $\langle b_2, b_1 \rangle = \dots = 0$}\\
        b_3 & := & v_3 & - & \frac{\langle v_3, b_1 \rangle}{\langle b_1, b_1 \rangle} \cdot b_1 & - & \frac{\langle v_3, b_2 \rangle}{\langle b_2, b_2 \rangle} \cdot b_2 & \multicolumn{1}{c|}{} && \text{ dann $\langle b_2, b_1 \rangle = \dots = 0$}\\ \cline{9-9}
            & \vdots &&&&&&& \multicolumn{1}{c|}{}\\
        b_n & := & v_n & - & \frac{\langle v_n, b_1 \rangle}{\langle b_1, b_1 \rangle} \cdot b_1 & - & \dots & - & \multicolumn{1}{c|}{\frac{\langle v_n, b_{n-1} \rangle}{\langle b_{n-1}, b_{n-1} \rangle} \cdot b_{n-1}} & \text{usw\dots}\\
            & \vdots &&&&&&& \multicolumn{1}{c|}{}\\
    \end{array}\]

    ist ein \emph{OS} $(b_1, b_2, \dots)$ von $V$ gegeben, für das $L(b_1, \dots, b_n) = L(v_1, \dots, v_n)$ für jede endliche Teilfamilie $(b_1, \dots, b_n)$ von $(b_1, b_2, \dots)$ gilt.

    Durch \emph{Normierung}, d.h. $c_n := \frac{b_n}{\|b_n\|}$ für alle $n$ ist dann ein \emph{ONS} $(c_1, c_2, \dots)$ von $V$ gegeben.
\end{theorem}

\section{Bemerkung}\label{sec:24.5}

\begin{itemize}
    \item Etwas kompakter geschrieben lautet die Rekursionsformel $b_1 := v_1$, und $b_{n+1} := v_{n+1} - \sum\limits_{i=1}^n \frac{\langle v_{n+1}, b_i \rangle}{\langle b_i, b_i \rangle} \cdot b_i$ für $n \geq 1$.
    \item Die Rekursion lässt sich gleich für die Familie $(c_1, c_2, \dots)$ aufschreiben als

        \[c_1 := \frac{v_1}{\|v_1\|},\]

        \[b_{n+1} := v_{n+1} - \sum\limits_{i=1}^n \langle v_{n+1}, c_i \rangle c_i,\] 

        \[c_{n+1} := \frac{b_{n+1}}{\|b_{n+1}\|} \left(\text{weil } \frac{1}{\langle b_i, b_i \rangle} = \frac{1}{\|b_i\|^2}\right).\]
\end{itemize}

\begin{proof}
    Haben zu zeigen:

    \begin{enumerate}
        \item $\forall n: b_n \neq o$,
        \item $\forall n, m, m < n: \langle b_n, b_m \rangle = 0$,
        \item $\forall n: L(b_1, \dots, b_n) = L(v_1, \dots, v_n)$.
    \end{enumerate}

    \emph{Vollständige Induktion nach $n$:} Alle diese Behauptungen gelten für $n = 1$ \checkmark.

    Induktionsschritt \emph{$n \to n + 1$}:

    \emph{Zu 1.}: Wegen Induktionsvoraussetzung ist $L(b_1, \dots, b_n) = L(v_1, \dots, v_n)$.

    Daraus folgt $\sum\limits_{i=1}^n \frac{\langle v_{n+1}, b_i\rangle}{\langle b_i, b_i\rangle} \cdot b_i \in L(b_1, \dots, b_n) = L(v_1, \dots, v_n)$. Da $(v_1, \dots, v_n, v_{n+1})$ linear unabhängig, ist somit $b_{n+1} \neq o$.

    \emph{Zu 2.}: Sei $m \leq n < n + 1$. Dann ist

    \begin{align*}
        \langle b_{n+1}, b_m \rangle &= \langle v_{n+1}, b_m \rangle - \sum_{i=1}^n \frac{\langle v_{n+1}, b_i\rangle}{\langle b_i, b_i \rangle} \cdot \underbrace{\langle b_i, b_m \rangle}_{=0 \text{ für } i \neq m}\\
                                     &= \langle v_{n+1}, b_m \rangle - \frac{\langle v_{n+1}, b_m \rangle}{\langle b_m, b_m \rangle} \cdot \langle b_m, b_m \rangle\\
                                     &= \langle v_{n+1}, b_m \rangle - \langle v_{n+1}, b_m \rangle = 0.
    \end{align*}

    \emph{Zu 3.} Wegen $L(b_1, \dots, b_n) = L(v_1, \dots, v_n)$ laut Induktionsvoraussetzung folgt $b_{n+1} \in L(b_1, \dots, b_n, v_{n+1}) = L(v_1, \dots, v_n, v_{n+1})$, also $L(b_1, \dots, b_n, b_{n+1}) \subseteq L(v_1, \dots, v_{n+1})$. Da die $(b_1, \dots, b_{n+1})$ als OS linear unabhängig laut~\ref{sec:24.3}(2), folgt die Gleichheit.
\end{proof}

\section{Bemerkungen}\label{sec:24.6}

\begin{remark}
    \begin{itemize}
        \item Das Schmidtsche Orthogonalisierungsverfahren macht aus einer Basis $(v_1, \dots, v_n)$ eines $n$-dimensionalen unitären Raums $V$ eine ONB $(c_1, \dots, c_n)$.
        \item Man kann damit auch jedes (abzählbare) Orthogonalsystem zu einer ONB ergänzen.
        \item Wenden wir den Satz~\ref{sec:24.4} gleich an:
    \end{itemize}
\end{remark}

\section{Satz}\label{sec:24.7}

\begin{theorem}
    Sei $V$ unitärer Raum mit S.P. $\langle \cdot, \cdot \rangle$, sei $S \subseteq V$ und $U$ ein UVR von $V$. Dann gilt:

    \begin{enumerate}[label=(\arabic*)]
        \item Ist $\dim V < \infty$, so hat $V$ bzgl. $\langle \cdot, \cdot \rangle$ eine Orthogonalbasis.
        \item $S^\perp$ ist UVR von $V$.
        \item Ist $\dim V < \infty$, so gilt $V = U \oplus U^\perp$, d.h. $U$ und $U^\perp$ sind komplementär.
    \end{enumerate}
\end{theorem}

\begin{proof}
    \begin{enumerate}[label=(\arabic*):]
        \item Man wähle irgendeine Basis von $V$ und orthogonalisiere.
        \item $o \in S^\perp$, mit $x, y \in S^\perp$, $\alpha, \beta \in K$, ist auch $\langle \alpha x + \beta y, z \rangle = \alpha \overbrace{\langle x, z \rangle}^{=0} + \beta \overbrace{\langle y, z \rangle}^{=0} = 0$ für alle $z \in S$, also $\alpha x + \beta y \in S^\perp$.
        \item Sei $(c_1, \dots, c_n)$ ONB von $U$, ergänze zu ONB $(c_1, \dots, c_m, c_{m+1}, \dots, c_n)$ von $V$. Dann setze $U_0 := L(c_{m+1}, \dots, c_n)$, also $U$ und $U_0$ komplementär, d.h. $U \oplus U_0 = V$. Weiter ist $\forall x \in U\ \forall y \in U_0: \langle x, y \rangle = 0$ aufgrund der Konstruktion, also $U_0 = U^\perp$.
    \end{enumerate}
\end{proof}

\section{Rechnen mit ONBen}\label{sec:24.8}

Mit einer ONB $(c_1, \dots, c_n)$ lässt sich besonders bequem rechnen:

\begin{theorem}
    Sei $V$ ein unitärer Raum mit S.P. $\langle \cdot, \cdot \rangle$, $(c_1, \dots, c_n)$ eine ONB von $V$. Dann:

    \begin{enumerate}[label=(\arabic*)]
        \item Ist $v = \sum\limits_{i=1}^n \lambda_i c_i$, $w = \sum\limits_{i=1}^n \mu_i c_i$, so ist $\langle v, w \rangle = \sum\limits_{i=1}^n \lambda_i \overline{\mu_i}$.
        \item Ist $v = \sum\limits_{i=1}^n \lambda_i c_i$, so ist $\|v\| = \left(\sum\limits_{i=1}^n \lambda_i \overline{\lambda_i}\right)^{\frac{1}{2}} = \left(\sum\limits_{i=1}^n \abs{\lambda_i}^2\right)^{\frac{1}{2}}$.
        \item Für jedes $v \in V$ ist $v = \sum\limits_{i=1}^n \langle v, c_i \rangle c_i$ und $\|v\| = \left(\sum\limits_{i=1}^n \abs{\langle v, c_i \rangle}^2\right)^{\frac{1}{2}}$.
    \end{enumerate}
\end{theorem}

\begin{proof}
    \begin{enumerate}[label=Zu (\arabic*):]
        \item 
            \begin{align*}
                \text{Haben } \langle v, w \rangle &= \left\langle \sum\limits_{i=1}^n \lambda_i c_i, \sum\limits_{j=1}^n \mu_j c_j \right\rangle\\
                                                   &= \sum\limits_{i=1}^n \sum\limits_{j=1}^n \lambda_i \overline{\mu_j} \underbrace{\langle c_i, c_j \rangle}_{=\delta_{i,j}}\\
                                                   &= \sum\limits_{i=1}^n \lambda_i \overline{\mu_i}.
            \end{align*}
        \item nimm $w = v$ in (1),

        \item Sei $w := v - \sum\limits_{i=1}^n \langle v, c_i \rangle c_i$. Dann ist für jedes $j$: $\langle w, c_j \rangle = \langle v, c_j \rangle - \sum\limits_{i=1}^n \langle v, c_i \rangle \underbrace{\langle c_i, c_j \rangle}_{=\delta_{i,j}} = \langle v, c_j \rangle - \langle v, c_j \rangle = 0$.

            Die $c_j$ bilden eine Basis von $V$.

            Damit hat jedes $z \in V$ die Form $z = \sum\limits_{j=1}^n \lambda_j c_j$. Damit folgt dann

            $\langle w, z \rangle = \langle w, \sum\limits_{j=1}^n \lambda_j c_j \rangle = \sum\limits_{j=1}^n \overline{\lambda_j} \underbrace{\langle w, c_j \rangle}_{=0} = 0$. Da dies für jeden Vektor $z \in V$ gilt, gilt dies auch für $z = w$, sodass $\langle w, w \rangle = 0$, also $w = 0$ folgt, da $\langle \cdot, \cdot \rangle$ positiv definiert ist. Der Zusatz zu $\|v\|$ folgt jetzt aus (2).
    \end{enumerate}
\end{proof}

\section{Proximum}\label{sec:24.9}

Eine wichtige Anwendung, die approximative Lösung von LGSen, möchten wir noch vorstellen.

\begin{theorem}
    Sei $V$ ein unitärer Raum mit S.P. $\langle \cdot, \cdot \rangle$. $U$ UVR von $V$, $\dim U = m$, $y \in V$. Dann existiert genau ein $y_0 \in U$, sodass $y - y_0 \in U^\perp$.

    Für $y_0$ gilt $\| y - y_0\| < \|y - x\|$ für jedes $x \in U$, $x \neq y_0$.

    Man nennt $y_0$ das \emph{Proximum zu $y$ in $U$}. (Auch: das \emph{auf $U$ gefällte Lot von $y$}.)

    \begin{center}
        \begin{tikzpicture}
            \draw (-1,0) -- (2,0);
            \draw (0,-0.5) -- (0,2);
            \node[below left] at (0,0) {$0$};
            \node[below] at (1,0) {$y_0$};
            \node[below] at (0.25, 0) {$x$};
            \draw[dashed] (0.25, 0) -- (1,1);
            \draw[dashed] (1,0) -- (1,1);
            \draw[fill=black] (1,1) circle(0.025);
            \draw[fill=black] (1,0) circle(0.025);
            \draw[fill=black] (0.25,0) circle(0.025);
            \node[right] at (1,1) {$y$};
            \node[right] at (2,0) {$U$};
            \node[above] at (0,2) {$U^\perp$};
        \end{tikzpicture}
    \end{center}
\end{theorem}

\begin{remark}
    Haben diese Konstruktion eine Lotes (falls $U = L(x)$) bereits in~\ref{sec:23.19} für geometrische Anwendungen benutzt.
\end{remark}

\begin{proof}
    \begin{itemize}
        \item Ist $y \in U$, sosetze $y_0 := y$, dann ist $y - y_0 = o$, und alle anderen Behauptungen sind klar.
        \item Sei also \OE $y \notin U$. Wähle irgendeine Basis $(v_1, \dots, v_m)$ von $U$, womit $(v_1, \dots, v_m, y)$ linear unabhängig sind. Orthogonalisiere dieses System nach Schmidt, Satz~\ref{sec:24.4}.

            Wir erhalten so ei ONS $(c_1, \dots, c_{m+1})$ mit $L(c_1, \dots, c_m) = L(v_1, \dots, v_m) = U$, $y \in L(c_1, \dots, c_{m+1})$. Dann hat $y$ eine Darstellung $y = \sum\limits_{i=1}^{m+1} \lambda_i c_i$.

            Wir setzen \emph{$y_0 := \sum\limits_{i=1}^m \lambda_i c_i$} und zeigen dafür die Behauptung.
        \item Nach Konstruktion ist $y_0 \in L(c_1, \dots, c_n) = U$. Ferner ist $y - y_0 = \lambda_{m+1} c_{m+1} \in U^\perp$, da ja $c_{m+1}$ zu allen Elementen einer Basis von $U$ orthogonal ist.
        \item Ist schließlich $y_0'$ ein weiterer solcher Vektor, so ist $y_0' - y_0 \in U$, da beide $\in U$. Ferner ist $y_0' - y_0 = (y - y_0) - (y - y_0') \in U^\perp$, da beide Differenzen $\in U^\perp$. Damit ist $y_0' - y_0 \in U \cap U^\perp$, und da die Räume komplementär sind nach~\ref{sec:24.7}(3), ist $y' - y_0 = o$.
        \item Schließlich gilt die Proximum-Eigenschaft: Für jedes $x \in U$ ist

            \begin{align*}
                \|y - x\|^2 &= \left\| (y - y_0) - (x - y_0) \right\|^2 = \left\langle (y-y_0) - (x-y_0), (y - y_0) - (x-y_0) \right\rangle\\
                            &= \|y - y_0\|^2 + \|x - y_0\|^2 - \langle y - y_0, x - y_0 \rangle - \langle x - y_0, y - y_0 \rangle.
            \end{align*}

            Bei den letzten beiden S.P.-Termen ist jeweils ein Argument in $U^\perp$, das andere in $U$, somit sind diese $=o$. Also ist

            \[\|y - x\|^2 = \|y - y_0\|^2 + \underbrace{\|x - y_0\|^2}_{\geq 0} \geq \|y - y_0\|^2,\]

            wobei \enquote{=} genau für $x = y_0$ eintritt.

            Obwohl wir die Konstruktion von $y_0$ von irgendeiner Basis von $U$ abhängig gemacht haben, ist $y_0$ also das Proximum, dadurch eindeutig bestimmt und somit unabhängig von der gewählten Basis von $U$. \hfill \qedhere
    \end{itemize}
\end{proof}

\section{Problem: Approximative Lösung eines LGS}\label{sec:24.10}

Mit diesem Satz können wir Probleme der \enquote{Ausgleichsrechnung} behandeln:

Gegeben sei das LGS $Ax = b$ mit $A \in K^{m\times m}$. Weiter sei

\begin{enumerate}
    \item $b \notin \im A$, d.h. das LGS ist \emph{nicht} lösbar,
    \item $n < m$ und $\rg A = n$, d.h. die Spalten $(a_1, \dots, a_n)$ von $A$ sind zwar linear unabhängig, bilden aber \emph{keine} Basis des $K^m$.
\end{enumerate}

\emph{Gesucht:} ein $x_0 \in K^n$, sodass $\| A x_0 - b \|$ minimal ist.

\emph{Lösung:} Verwende Satz~\ref{sec:24.9} mit $V := K^m$, $U := L(a_1, \dots, a_n)$, $y := b \notin U$ und konstruiere as eindeutig bestimmte Proximum $b_0$ zu $b$ in $U$.

\emph{Dieses $b_0$ tut's:} Wegen $U = \im A$ ist dann $b_0 \in \im A$, d.h. $Ax = b_0$ ist lösbar und da $A$ maximalen Rang hat, sogar eindeutig lösbar.

\[Ax = b_0 = Ax' \Rightarrow A(x - x') = o \Rightarrow x - x' = o,\]

\[\text{ da } \ker A = \emptyset \text{ wegen } n = \overbrace{\rg A}^n + \dim \ker A \Rightarrow \dim\ker A = 0\]

Die Lösung sei $x_0$. Dann ist $\| Ax_0 - b \| = \|b_0 - b\| < \|x - b\|$ für alle $x \in U = \im A$, $x \neq b_0$, d.h. $\| A x_0 - b \|$ ist minimal (und $A x_0 - b \perp U$).

\section{Adjungierte Matrix}\label{sec:24.11}

Zum praktischen Rechnen kann man wie folgt mit der adjungierten Matrix vorgehen.

\begin{definition}
    Sei $A \in K^{m \times m}$ eine Matrix mit Elementen aus $K \in \{ \mathbb{R}, \mathbb{C} \}$. Dann heißt $A^* \in K^{n \times M}$, $A^* := \overline{A}^T$ die \emph{adjungierte Matrix}. Man erhält sie, indem man $A$ transponiert und (nur im Falle $K = \mathbb{C}$) alle Elemente konjugiert komplex nimmt.
\end{definition}

\section{Lösung von~\ref{sec:24.10}}\label{sec:24.12}

\begin{theorem}
    Man bilde das LGS $A^* A x = A^* b$, die sogenannte \emph{Normalengleichung}. Dafür gilt:

    \begin{enumerate}
        \item Es hat für jedes $b \in \mathbb{C}^n$ eine eindeutig bestimmte Lösung $x_0$. Für dieses zu $b$ gehörige $x_0$ ist $\| A x_0 - b \|$ minimal.
        \item Falls $\rg(A) = n \leq m$ ist, ist $A^* A \in K^{n \times n}$ eine invertierbare Matrix.
    \end{enumerate}
\end{theorem}

\begin{proof}
    Seien $a_j^* = (\alpha_{j1}^*, \dots, \alpha_{jm}^*), j = 1, \dots, n$ die Zeilen von $A^*$,

    und seien $a_j = \begin{pmatrix} \alpha_{1j} \\ \vdots \\ \alpha_{mj} \end{pmatrix}, j = 1, \dots, n$ die Spalten von $A$, d.h. für alle $j = 1, \dots, n, i = 1, \dots, m$ gilt $\alpha_{ji}^* = \overline{\alpha_{ij}}$.

    Sei nun $v = \begin{pmatrix} v_1 \\ \vdots \\ v_m \end{pmatrix} \in \mathbb{C}^m$. Wir bilden das Matrix-Produkt $a_j^* \cdot v$ (Zeile $\times$ Spalte).

    Dann haben wir $a_j^* \cdot v = \sum\limits_{i=1}^m \alpha_{ji}^* \nu_i = \sum\limits_{i=1}^m \overline{\alpha_{ij}} \nu_i = \sum\limits_{i=1}^m \nu_i \overline{\alpha_{ij}} = \langle v, a_j \rangle$, das ist genau das Standard-S.P. im $\mathbb{C}^m$.

    Somit folgt $A^* v = \begin{pmatrix} \langle v, a_1 \rangle \\ \vdots \\ \langle v, a_n \rangle \end{pmatrix}$ für jedes $v \in \mathbb{C}^m$.

    Also gilt: \emph{$v \perp \im A \Leftrightarrow A^* v = o$.} $\oast$

    \emph{Zu (1):} Hatten in~\ref{sec:24.10} gesehen: Haben genau eine Lösung $x_0$, für die $Ax_0 - b \perp \im A$ gilt.

    \begin{itemize}
        \item Mit $b = Ax_0 = (Ax_0 - b)$ folgt $A^* b = A^* Ax_0 - A^*(Ax_0 - b) = A^* Ax_0$, da $Ax_0 - b \perp \im A$ (mit $\oast$), somit erfüllt $x_0$ die Normalengleichung, die ist somit lösbar.
        \item Ist $x$ irgendeine Lösung von $A^* Ax = A^* b$, so ist $o = A^* Ax - A^*b = A^* (Ax - b)$, somit $Ax - b \perp \im A$. Damit ist $Ax$ das Proximum zu $b$, eindeutig bestimmt, also $x = x_0$.
    \end{itemize}

    \emph{Zu (2):} $A^*A \in K^{n \times n}$ ist quadratisch. Da $A^* Ax = A^* b$ eindeutig lösbar, ist $A^* A$ invertierbar.
\end{proof}

\section{Beispiel für das Schmidt-Ortho\-go\-na\-li\-sie\-rungs\-verfahren:}\label{sec:24.13}

\begin{example}
    Im $\mathbb{R}^4$ mit Standard-S.P. sei

    \[v_1 := \begin{pmatrix} 1 \\ 1 \\ 1 \\ 1 \end{pmatrix}, v_2 := \begin{pmatrix} 3 \\ 3 \\ 1 \\ 1 \end{pmatrix}, v_3 := \begin{pmatrix} 5 \\ 3 \\ 3 \\ 1 \end{pmatrix}, v_4 := \begin{pmatrix} 8 \\ 4 \\ 2 \\ 2 \end{pmatrix}.\]

    \begin{itemize}
        \item Dann: $b_1 := v_1$, $\|b_1\| = \sqrt{4} = 2$, also $c_1 = \frac{1}{2} \begin{pmatrix} 1 \\ 1 \\ 1 \\ 1 \end{pmatrix} = \frac{1}{\|b_1\|} \cdot b_1$.
        \item Dann:

            \[b_2 := b_2 - \sum\limits_{i=1}^1 \langle v_2, c_i \rangle c_i = v_2 - \langle v_2, c_1 \rangle c_1 = \begin{pmatrix} 3 \\ 3 \\ 1 \\ 1 \end{pmatrix} - 4 \cdot \frac{1}{2} \begin{pmatrix} 1 \\ 1 \\ 1 \\ 1 \end{pmatrix} = \begin{pmatrix} 1 \\ 1 \\ -1 \\ -1 \end{pmatrix},\]

            also $c_2 = \frac{1}{\|b_2\|} \cdot \begin{pmatrix} 1 \\ 1 \\ -1 \\ -1 \end{pmatrix} = \frac{1}{2} \begin{pmatrix} 1 \\ 1 \\ -1 \\ -1 \end{pmatrix}$. $b_{n+1} := v_{n+1} - \sum\limits_{i=1}^n \langle v_{n+1}, c_i \rangle c_i$.
        \item Dann:

            \begin{align*}
                b_3 &:= v_3 - \sum_{i=1}^2 \langle v_3, c_i \rangle c_i = v_3 - \langle v_3, c_1 \rangle c_1 - \langle v_3, c_2 \rangle c_2\\
                    &= \begin{pmatrix} 5 \\ 3 \\ 3 \\ 1 \end{pmatrix} - 6 \cdot \frac{1}{2} \begin{pmatrix} 1 \\ 1 \\ 1 \\ 1 \end{pmatrix} - 2 \cdot \frac{1}{2} \begin{pmatrix} 1 \\ 1 \\ -1 \\ -1 \end{pmatrix} = \begin{pmatrix} 1 \\ -1 \\ 1 \\ -1 \end{pmatrix}, 
            \end{align*}

            \[\text{also } c_3 = \frac{1}{\|b_3\|} \cdot \begin{pmatrix} 1 \\ -1 \\ 1 \\ -1 \end{pmatrix} = \frac{1}{2} \begin{pmatrix} 1 \\ -1 \\ 1 \\ -1 \end{pmatrix}.\]
        \item Dann:

            \begin{align*}
                b_4 &:= v_4 - \sum_{i=1}^3 \langle v_4, c_i \rangle c_i = v_4 - \langle v_4, c_1 \rangle c_1 - \langle v_4, c_2 \rangle c_2 - \langle v_4, c_3 \rangle c_3\\
                    &= \begin{pmatrix} 8 \\ 4 \\ 2 \\ 2 \end{pmatrix} - 4 \cdot \begin{pmatrix} 1 \\ 1 \\ 1 \\ 1 \end{pmatrix} - 2 \cdot \begin{pmatrix} 1 \\ 1 \\ -1 \\ -1 \end{pmatrix} - 1 \cdot \begin{pmatrix} 1 \\ -1 \\ 1 \\ -1 \end{pmatrix} = \begin{pmatrix} 1 \\ -1 \\ -1 \\ 1 \end{pmatrix},
            \end{align*}

            \[\text{also } c_4 = \frac{1}{\|b_4\|} \cdot \begin{pmatrix} 1 \\ -1 \\ 1 \\ -1 \end{pmatrix} = \frac{1}{2} \begin{pmatrix} 1 \\ -1 \\ -1 \\ 1 \end{pmatrix}.\]

            Unsere ONB ist dann $(c_1, c_2, c_3, c_4)$. Und \emph{ja}: $L(c_1) = L(v_1)$, $L(c_1, c_2) = L(v_1, v_2)$, $L(c_1, c_2, c_3) = L(v_1, v_2, v_3)$, $L(c_1, \dots, c_4) = \mathbb{R}^4$.
    \end{itemize}
\end{example}

\section{Beispiel zur approximativen Lösung eines LGS}\label{sec:24.14}

$Ax = b$ mit

\[A = \begin{pmatrix} 1 & 1 & 1\\ 1 & -1 & 1\\ 1 & 1 & -1\\ 1 & -1 & -1 \end{pmatrix}, b = \begin{pmatrix} 3.1 \\ 0.9 \\ 0.9 \\ -0.9 \end{pmatrix}\]

(der Vektor $b$ könnte etwa durch Messungen entstanden sein und Messfehler enthalten).

Mit Gauß-Elimination überzeugt man sich, dass $Ax = b$ keine Lösung hat. Stattdessen betrachten $A^* Ax = A^* b$, haben

\[A^* A = \begin{pmatrix} 1 & 1 & 1 & 1 \\ 1 & -1 & 1 & -1 \\ 1 & 1 & -1 & -1 \end{pmatrix} \cdot \begin{pmatrix} 1 & 1 & 1 \\ 1 & -1 & 1 \\ 1 & 1 & -1 \\ 1 & -1 & -1 \end{pmatrix} = \begin{pmatrix} 4 & 0 & 0 \\ 0 & 4 & 0 \\ 0 & 0 & 4 \end{pmatrix},\]

Unsere Messfehler ergeben sich aus

\[\|Ax_0 - b\| = \left\| \begin{pmatrix} 3 \\ 1 \\ 1 \\ -1 \end{pmatrix} - \begin{pmatrix} 3.1 \\ 0.9 \\ 0.9 \\ -0.9 \end{pmatrix} \right\| = \left\| \begin{pmatrix} -0.1 \\ 0.1 \\ 0.1 \\ -0.1 \end{pmatrix} \right\| = 0.2.\]
