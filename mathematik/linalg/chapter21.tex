\chapter{Diagonalisierbarkeit, Trigonalisierbarkeit}

\paragraph{Stichworte:} Diagonalisierbarkeitskriterien mit Eigenräumen und $X_A$ bzw. $X_f$, geometrische und algebraische Vielfachkeit eines EWs, trigonalisierbar, Fahnenbasis

\section{Rückblick}

Die EWtheorie ist nützlich im Problem, eine \emph{Normalform} (vgl~\ref{sec:20.1}) aufzustellen: Sei $V$ ein $K$-VR, $\dim_K V = n$, Geg. ein Endo $f: V \to V$. Gibt es eine Basis $B$ bezüglich der die darstellende Matrix $\widetilde{A} = \prescript{}{B}[f]_B$ möglichst einfache Gestalt hat? M.a.W. gibt es zu irgendeiner darstellenden Matrix $A$ eine dazu ähnliche Matrix $\widetilde{A}$, sodass $\widetilde{A}$ möglichst einfache Gestalt hat? (Sodass auf diese Weise auch eine ``geometrische'' Interpretation von $f$ möglich wird.) Haben dies in Zusammenfassung~\ref{sec:20.13} für diagonalisierbare Endos / Matrizen geklärt: $\text{diag'bar} \Leftrightarrow \exists \text{Basis aus EVen, und:} \exists n\text{-versch. EWe} \Rightarrow \text{diag'bar}$.

\section{Äquivalente Bedingungen für Diagonalisierbarkeit}\label{sec:21.2}

Wir möchten der Diagonalisierbarkeit noch näher mit den \emph{Eigenräumen $E_\lambda$} spezifizieren: (Erinnerung: $E_\lambda := \ker(f - \lambda \id_V)$ für einen EW $\lambda$.)

\begin{theorem}
    Sei $V$ ein $K$-VR, $\dim_K V = n$, $f: V \to V$ Endo. Äquivalent sind:

    \begin{enumerate}[label=(\roman*)]
        \item $f$ diag'bar,
        \item in $V$ existiert Basis aus EVen von $f$,
        \item $V$ ist \emph{direkte} Summe der Eigenräume von $f$, d.h. $V = E_{\lambda_1} \oplus \dots \oplus E_{\lambda_k}$, ein $k \leq n$,
        \item die Summe der Dimensionen der Eigenräume von $f$ ist gleich $n$, d.h. $n = \sum\limits_{i=1}^k \dim E_{\lambda_i}$, wo $k \leq n$ und $\lambda_1, \dots, \lambda_k$ die p.w.v. EWe.
    \end{enumerate}
\end{theorem}

\begin{proof}
    \emph{$(i) \Rightarrow (ii)$:} hat $f$ bzgl. $B$ die Diag'gestalt $\begin{smallmatrix}
        \lambda_1 & & 0\\
        & \ddots & \\
        0 & & \lambda_n
    \end{smallmatrix}=A$, d.h. $A e_i = \lambda_i * e_i$, so gilt $f(v_i) = \lambda v_i$ für die Basiselemente $v_i$ von $B$ (da ja $K_B(v_i) = e_i$ gilt). Also besteht $B$ aus EVen von $f$.
    
    \emph{$(ii) \Rightarrow (iii)$:} seien $\lambda_1, \dots, \lambda_k$ die verschiedenen EWe von $f$. Nach Satz \emph{20.6} sind Familien $(v_1, \dots, v_K)$, wo jedes $v_i \neq 0$ aus $E_{\lambda_i}$ stammt, linear unabhängig. Es folgt $E_{\lambda_i} \cap \left(\sum\limits_{j\neq i} E_{\lambda_j}\right) = 0$ für jedes $i \leq k$. Also ist die Summe der $E_{\lambda_i}$ direkt, d.h. $E_{\lambda_1} \oplus \dots \oplus E_{\lambda_k} \subseteq V$. Für ``$\supseteq$'' sei $v \in V$ beliebig, bzgl. der Basis $(v_1, \dots, v_n)$ aus EVen ist $v = c_1 v_1 + \dots + c_n v_n$. Fassen wir in dieser Formel alle Summanden zu EVen zum gleichen EW $c_i$ zusammen, erhalten wir $v = \widetilde{v}_1 + \dots + \widetilde{v}_k$, die $\widetilde{v}_i \in E_{\lambda_i}$.

    \emph{$(iii) \Rightarrow (iv)$:} klar mit~\ref{sec:12.8}

    \emph{$(iv) \Rightarrow (i)$:} Seien $E_{\lambda_1}, \dots, E_{\lambda_k}$ die Eigenräume von $f$, $\dim E_{\lambda_i} =: m_i$. Wählen in jedem $E_{\lambda_i}$ Basis $B_i$, setze $B = B_1 \cup \dots \cup B_k$, ist linear unabhänfig nach Satz~\ref{sec:20.6}. Wegen $m_1 + \dots + m_k = n$ ist $B$ sogar Basis von $V$, bzgl. der $f$ Diag'gestalt hat.
\end{proof}

\section{Bemerkung}\label{sec:21.3}

Satz~\ref{sec:21.2} gilt analog für $K^n$ statt $V$ und Matrix $A \in K^{n \times n}$ statt $f$. Wir formulieren noch ein \emph{Kriterium für Diag'barkeit mit dem charakteristischen Polynom.} Dieses hat den Vorteil, dass wir dafür keine Eigenräume berechnen müssen wie in \emph{21.2}, es genügt, deren Dimension (über eine Rangüberlegung) zu bestimmen.

\section{Satz}\label{sec:21.4}

\begin{theorem}
    Sei $V$ ein $K$-VR, $\dim V = n$, $f: V \to V$ Endo. Dann gilt:

    \[f \text{ diag'bar } \Leftrightarrow X_f \text{ hat die Form } X_f(T) = (-1)^n (T-\lambda_1)^{r_1} \dots (T - \lambda_k)^{r_k} \quad (*)\]

    mit $r_i \in \mathbb{N}$, p.w.v. $\lambda_1, \dots, \lambda_k \in K$, \emph{und} wenn für $i = 1, \dots, k$ gilt: $\underbrace{\dim \im}_{\rg}(f - \lambda_i \id_V) = n - r_i$.
\end{theorem}

\section{Bemerkungen}\label{sec:21.5}

\begin{remark}
    \begin{enumerate}
        \item hat $X_f$ die Form $\oast$, so sagt man: $X_f$ \emph{zerfällt in Linearfaktoren}, und $r_i$ ist die \emph{Vielfachkeit der Nullstelle $\lambda_i$}

            (In diesem Fall heißt $f$ (und jede Matrix, die $f$ darstellt) zerfallend.)
        \item Die $\lambda_i$ sind gerade die (p.w.v.) EWe von $f$.
        \item die zweite Forderung besagt \emph{$\dim E_{\lambda_i} = r_i$} für $i=1, \dots, k$, da $E_{\lambda_i} = \ker(f - \lambda_i \cdot id_V)$, aufgrund des Rangsatzes
        \item entsprechendes gilt für $A \in K^{n \times n}$, ersetze $\dim \im(f - \lambda_i \id_V)$ durch $\rg(A - \lambda_i I_n)$.
        \item hinreichend für Diag'barkeit: $X_f$ zerfällt \emph{in lauter verschiedene} Linearfaktoren (dann alle $r_i = 1$).
    \end{enumerate}
\end{remark}

Im allgemeinen werden $r_i = \dim E_{\lambda_i}$ und die Exponenten in $(*)$ nicht übereinstimmen. Sobald sie es tun, so sagt Satz \emph{21.4}, ist $f$ diag'bar (und umgekehrt). Wir wollen diese Zahlen zunächst unterscheiden und führen dafür folgende Begriffe ein.

\section{Geometrische- und Algebraische Vielfachkeit}\label{sec:21.6}

\begin{definition}{(geometrische- und algebraische Vielfachkeit)}
    Die \emph{geometrische Vielfachkeit} eines EWs $\lambda_i$ ist $\dim E_{\lambda_i}$, die Dimension des Eigenraums.

    Die \emph{algebraische Vielfachkeit} eines EWs $\lambda_i$ ist $r_i$, der Exponent von $(T - \lambda_i)$ in $(*)$.
\end{definition}

\section{Korollar}\label{sec:21.7}

\begin{corollary}
    Sind geometrische und algebraische Vielfachkeiten gleich (für jeden EW), so ist $f$ diag'bar (und umgekehrt).

    (klar mit~\ref{sec:21.4},~\ref{sec:21.6})
\end{corollary}

\section{Beweis von Satz~\ref{sec:21.4}}\label{sec:21.8}


\begin{proof}
    \emph{``$\Rightarrow$''}: $f$ diag'bar $\Rightarrow$ hat Abb.matrix $A_f = \begin{pmatrix}
        \lambda_1 & & & & & &\\
                  & \ddots & & & & 0 &\\
                  & & \lambda_1 & & & &\\
                  & & & \ddots & & &\\
                  & & & & \lambda_k & &\\
                  & 0 & & & & \ddots &\\
                  & & & & & & \lambda_k
    \end{pmatrix}$ mit p.w.v. $\lambda_1, \dots, \lambda_k \in K$.

    Dann: 

    \[\det(A_f - T \cdot I_n) = (\lambda_1 - T)^{r_1} \cdots (\lambda_k - T)^{r_k} = (-1)^n (T - \lambda_1)^{r_1} \cdots (T - \lambda_k)^{r_k}\]

    , da $r_1 + \dots + r_k = n$, und 

    \[\dim \im (f - \lambda_i \id_V) = \rg (A_f - \lambda_i I_n) = \rg \underbrace{\begin{pmatrix}
        \lambda_1 - \lambda_i & & &\\
                              & \ddots & &\\
                              & & \lambda_1 - \lambda_i &\\
                              & & & \ddots
    \end{pmatrix}}_{\text{hat $r_i$ viele Nullzeilen für $\lambda_i - \lambda_i = 0$}} = n - r_i.\]

    \emph{``$\Leftarrow$'':} $\oast \Rightarrow$ die $\lambda_1, \dots, \lambda_k$ sind EWe von $f$,

    und da $\dim \im (f-\lambda_i \id_V) = n - r_i$ ist $\dim E_{\lambda_i} = \dim\ker(f - \lambda_i \id_V) = n - \overbrace{\rg(f - \lambda_i \id_V)}^{n - r_i} = r_i$. Also $\dim E_{\lambda_1} + \dots + \dim E_{\lambda_k} = r_1 + \dots + r_k = n$. Wegen Satz~\ref{sec:21.2}(iv) ist $f$ diagonalisierbar.
\end{proof}

\section{Beispiele}\label{sec:21.9}

\begin{example}
    \begin{enumerate}
        \item \begin{itemize}
            \item $A = \begin{pmatrix}
                0 & 1 \\ -1 & 0
            \end{pmatrix}$ über $k = \mathbb{R}$ \emph{nicht} diag'bar: charakteristischens Polynom ist $X_A (T) = T^2 + 1)$, ohne Nst. über $\mathbb{R}$.
            \item über $K = \mathbb{C}$: $X_A (T) = (T-i)(T-(-i))$, hat 2 verschiedene EWe $\to$ diag'bar zu $\widetilde{A} = \begin{pmatrix}
                    i & 0 \\ 0 & -i
                    \end{pmatrix} = S^{-1} A S$ mit $S = \left(\begin{array}{c|c}
                    1 & 1 \\ i & -i
            \end{array}\right)$, wobei die Spalten die EVen von $A$ sind (zum EW $i, -i$)

            \[A \cdot \begin{pmatrix}
                1 \\ i
            \end{pmatrix} = \begin{pmatrix}
            0 & 1 \\ -1 & 0
            \end{pmatrix} \cdot \begin{pmatrix}
                1 \\ i
            \end{pmatrix} = \begin{pmatrix}
                i \\ -1
            \end{pmatrix} = i \cdot \begin{pmatrix}
                1 \\ i
            \end{pmatrix},\]

            \[A \cdot \begin{pmatrix}
                1 & -i
            \end{pmatrix} = \begin{pmatrix}
                0 & 1 \\ -1 & 0
            \end{pmatrix} \cdot \begin{pmatrix}
                1 \\ -i
            \end{pmatrix} = \begin{pmatrix}
                -i \\ -1
            \end{pmatrix} = -i \cdot \begin{pmatrix}
                1 \\ -i
            \end{pmatrix}\]
        \end{itemize}
        
            \emph{Fazit:} $f: \mathbb{R}^2 \to \mathbb{R}^2, x \mapsto A \cdot x$ hat keine EWe über $\mathbb{R}$, und ist geometrisch eine Drehung um $-90 \textdegree$, dennL $e_1 = \begin{pmatrix} 1 \\ 0 \end{pmatrix} \mapsto \begin{pmatrix} 0 \\ -1 \end{pmatrix}$, $e_2= \begin{pmatrix} 0 \\ 1 \end{pmatrix} \mapsto \begin{pmatrix} 1 \\ 0 \end{pmatrix}$.
        \item $A = \begin{pmatrix} 1 & 0 \\ 1 & 1 \end{pmatrix}$ hat $X_A(T) = {(T-1)}^2$, die algebraische Vielfachheit des EWs 1 ist also $=2$, aber $\rg(A - 1 \cdot I_n) = \rg \left(\begin{smallmatrix} 0 & 0 \\ 1 & 0 \end{smallmatrix}\right) = 1$, d.h. die geometrische Vielfachheit ist $n - 1 = 2 - 1 = 1 \neq 2$.

            Also: alg. und geom. Vielfachheit verschieden! $\Rightarrow$ $A$ nicht diag'bar wegen Korollar \emph{21.7}.
    \end{enumerate}
\end{example}

\section{Trigonalisierbarkeit}\label{sec:21.10}

Es stellt sich die Frage, wie nichtdiagonalisierbare Matrizen dennoch auf eine einfache Normalform gebracht werden könnten. Wir zeigen jetzt, wann immerhin eine (obere) Dreiecksmatrix (Bild?) erreichbar ist, vgl. die Definition \emph{14.9} solcher Matrizen. Wie diese oberen Dreiecksmatrizen noch näher spezifiziert werden können, wird dann in Linearer Algebra \RN{2} als ``Jordansche Normalform'' vorgestellt und bewiesen.

\begin{definition}
    Besitzt ein Endo $f$ eine Matrixdarstellung als (endliche) Dreiecksmatrix, so heißt $f$ \emph{trigonalisierbar} bzw. \emph{triangulierbar}. (Entsprechend eine Matrix $A$ falls $f_A: K^n \to K^n, x \mapsto Ax$, trigonalisierbar ist.)
\end{definition}

\section{Bemerkung}\label{sec:21.11}

\begin{remark}
    Wieder können wir \OE Matrizen $A$, die $f$ darstellen, betrachten.
\end{remark}

\section{Trigonalisierbarkeit und Invertierbarkeit}\label{sec:21.12}

Unmittelbar aus der Definition ist ersichtlich:

\begin{corollary}
    Ist $A \in K^{n \times n}$ trigonalisierbar, so gibt es eine invertierbare Matrix $S \in K^{n \times n}$, sodass $S^{-1} A S = \begin{pmatrix}
        \lambda_1 & & * & \\
        & \lambda_2 & & \\
        & & \ddots & \\
        0 & & & \lambda_n
    \end{pmatrix}$ eine obere Dreiecksmatrix ergibt. Dabei sind die Diagonalelemente $\lambda_1, \dots, \lambda_n$ dieser Matrix genau die EWe von $A$.
\end{corollary}

\begin{proof}
    Haben $X_{S^{-1}AS} (T) = (\lambda_1 - T) \cdot (\lambda_2 - T) \cdots (\lambda_n - T)$, die Nst. $=$ EWe von $S^{-1} A S$ (bzw. von $A$ wegen Korollar \emph{20.4}) sind genau die $\lambda_1, \dots, \lambda_n$.
\end{proof}

\section{Trigonalisierbarkeit Kriterien}\label{sec:21.13}

Wir können die Trigonalisierbarkeit wie folgt durch Kriterien charakterisieren:

\begin{theorem}
    Sei $A \in K^{n \times n}$. Dann sind äquivalent:

    \begin{enumerate}[label=(\arabic*)]
        \item $A$ ist (über $K$) triangulierbar,
        \item $X_A$ zerfällt über $K$ in Linearfaktoren,
        \item Es gibt eine Basis $B = (v_1, \dots, v_n)$ des $K^n$ derart, dass $f_A(L(v_1, \dots, v_j)) \subseteq L(v_1, \dots, v_j)$ für alle $j \in \{1, \dots, n\}$ gilt.

            \[f_A: K^n \to K^n, x \mapsto Ax\]
    \end{enumerate}
\end{theorem}

\section{Fahnenbasis}\label{sec:21.14}

\begin{definition}{(Fahnenbasis)}
    Eine Basis wie in Satz~\ref{sec:21.13}.(3) heißt \emph{Fahnenbasis} für $A$.

    In diesem Fall sind die UVRe $U_j := L(v_1, \dots, v_j)$ $f_A$-invariant, d.h. $f_A(U_j) \subseteq U_j$.
\end{definition}

\section{Beweis von Satz~\ref{sec:21.13}}\label{sec:21.15}

\begin{proof}
    \begin{enumerate}
        \item[$(1) \Rightarrow (2)$:] Ist $A$ ähnlich zur oberen Dreiecksmatrix $C$ mit Diagonalelementen $\lambda_1, \dots, \lambda_2$, so ist $X_A (T) = X_C(T) = (T - \lambda_1) \cdots (T-\lambda_n)$, vgl. Beweis von Korollar~\ref{sec:21.12}.

        \item[$(2) \Rightarrow (3)$:] Zu zeigen: $X_A$ zerfällt in Linearfaktoren $\Rightarrow A$ trigonalisierbar.

            Vollständige Induktion nach $n$: \emph{$n = 1$}: $X_A (T) = \lambda_n - T$, wähle $B = (v_1)$, wo $v_1$ ein EV zum EW $\lambda_1$ ist. Dann ist $B$ eine Fahnenbasis.

            \emph{$n-1 \to n$:} Da $X_A$ in Linearfaktoren zerfällt und $\deg X_A = n \geq 2$ ist, hat $A$ einen EW $\lambda$ und einen EV $v_1$ zu $\lambda$. Ergänze $v_1$ durch Vektoren $v_2, \dots, v_n$ zu einer Basis $C$ von $V$. Dann ist $\prescript{}{C}{[f_A]}_C = \left(
            \begin{array}{ccccc}
                \lambda & * & * & \cdots & * \\ \cline{2-5}
                \multicolumn{1}{c|}{0} & & & & \\
                \multicolumn{1}{c|}{\vdots} & & & A_2 & \\
                \multicolumn{1}{c|}{0} & & & &
            \end{array}\right)$

            Nun kann die Induktionsvoraussetzung \emph{nicht} auf $V_2 := L(v_2, \dots, v_n)$ und $f_A | V_2$ angewendet werden, da $f_A | V_2$ i.a. kein Endomorphismus von $V_2$ ist.

            Konstruiere stattdessen $f_2 \in \End(V_2)$ wie folgt.

            Ist $v \in V_2$, so ist $f_A(v) = \alpha_1 v_1 + \dots + \alpha_n v_n$ für eindeutig bestimmte $\alpha_1, \dots, \alpha_n \in K$.

            Setze dann $f_2(v) := \alpha_2 v_2 + \dots + \alpha_n v_n$. Dann ist $f_2 \in \End(V_2)$ und $A_2 = \prescript{}{C_2}{[f_2]}_{C_2}$ mit $C_2 := (v_2, \dots, v_n)$. Da $X_A(T) = (\lambda - T) X_{A_2} (T)$ und $X_A$ in Linearfaktoren zerfällt, zerfällt $X_{A_2}$ in Linearfaktoren. Nach Ind.vor. existiert eine Basis $B'=(w_2, \dots, w_n)$ von $V_2$, sodass $\prescript{}{B'}{[f_2]}_{B'}$ eine obere Dreiecksmatrix ist. Jetzt sei $B:=(v_1, w_2, w_3, \dots, w_n)$.

            \begin{itemize}
                \item Dies ist eine Basis von $V$, da $V_2 = L(B')$ und $C$ eine Basis von $V$ ist.
                \item $B$ ist eine Fahnenbasis: Es sit $f_A(v_1) = \lambda v_1$, also $U_1 := L(v_1)$ ist $f_A$-invariant. Und für $i = 2, \dots, n$ gilt $f_A(w_i) = \gamma_1 v_1 + \gamma_2 w_2 + \dots + \gamma_i w_i$ nach Wahl von $B'$, also $f_A(U_i) \subseteq U_i$, $U_i := L(v_1, w_2, \dots, w_i)$, unter Beachtung von $U_1 \subseteq U_2 \subseteq \dots \subseteq U_n$.
            \end{itemize}
        \item[$(3) \Rightarrow (1)$:] Sei $B = (v_1, \dots, v_n)$ eine Fahnenbasis für $A$. Dann ist $f_A(v_j) \in L(v_1, \dots v_j)$, also existiert $\gamma_{ij} \in K$ mit $f_A(v_j) = \sum\limits_{i=1}^j \gamma_{ij} v_i$, sodass $\prescript{}{B}{[f_A]}_B = (\gamma_{ij})_{ij}$ eine obere Dreiecksmatrix ist, wenn man noch $\gamma_{ij} := 0$ für $i > j$ setzt.
    \end{enumerate}
\end{proof}

\section[Bestimmung einer Fahnenbasis]{Verfahren zur Bestimmung einer Fahnenbasis von $f (=f_A)$ (laut Induktion in~\ref{sec:21.15})}\label{sec:21.16}

\begin{enumerate}
    \item Setze $m : = 0$
    \item Seien $\mu_1, \dots \mu_k$ die \emph{p.w.v.} EWe von $f$.

        Berechne zu jedem Eigenraum $E_{\mu_i}$ eine Basis und vereinige diese zu einer Basis $(v_{m+1}, \dots, v_{m+r})$ von $E_{\mu_1} \oplus \dots \oplus E_{\mu_k}$.

        Jeder Vektor $v_i$ gehört zur gesuchten Fahnenbasis.
    \item Ergänze diese Basis durch $w_1, w_2, \dots$ zu einer Basis $C$ von $V$. Dann ist 
        
        \[\prescript{}{C}{[f_A]}_C = \left(\begin{array}{ccccc}
                \lambda_1 & & & & \multicolumn{1}{|c}{}\\
                          & \lambda_2 & & & \multicolumn{1}{|c}{*}\\
                          & & \ddots & & \multicolumn{1}{|c}{}\\
                          & & & \lambda_r & \multicolumn{1}{|c}{}\\ \cline{1-5}
                          & \multicolumn{2}{c}{0} & & \multicolumn{1}{|c}{C}
        \end{array}\right)\]

        (Die $\lambda_1, \dots, \lambda_r \in \{\mu_1, \dots, \mu_k\}$ seien entsprechend den Basisvektoren angeordnet.)
    \item Setze $D := (w_1, w_2, \dots)$ und $W := L(D)$.

        Definiere $f_2 \in \End(W)$ durch $\prescript{}{D}{[f_2]}_D := C$. (Mit C aus Schritt 3.)
    \item Setze $f := f_2$, $V := W$, $m := r + 1$ und fahre bei 2. fort.

        ($r$ kann sich in Schritt 2. bei jeder Iteration ändern.)
\end{enumerate}

\section{Beispiele}\label{sec:21.17}

\begin{example}
    \begin{enumerate}
        \item Gesucht: Fahnenbasis für $A = \left(\begin{smallmatrix}
                1 & 1 \\ 1 & 1
            \end{smallmatrix}\right) \in \mathbb{R}^{2 \times 2}$?

            Haben $X_A(T) = (1 - T)^2 - 1 = T (T - 2)$, haben EW $0$ und $2$.

            Haben Basis $B = ( \left(\begin{smallmatrix}
                1 \\ -1
            \end{smallmatrix}\right), \left(\begin{smallmatrix}
                1 \\ 1
            \end{smallmatrix}\right))$ aus EVen, ist damit schon Fahnenbasis.

            ($A$ ist bereits diagonalisierbar, nämlich ähnlich zu $\left(\begin{smallmatrix}
                    0 & 0 \\ 0 & 2
            \end{smallmatrix}\right)$.)
        \item Gesucht: Fahnenbasis für $A = \left(\begin{smallmatrix}
                    1 & -4 \\ 1 & -3
            \end{smallmatrix}\right) \in \mathbb{R}^{2 \times 2}$?

            Haben $X_A(T) = (1 - T) (-3 -T) + 4 = T^2 + 2T + 1 = (T + 1)^2$ mit EW $-1$.

            Wähle $v_1 := \left(\begin{smallmatrix}2 \\ 1 \end{smallmatrix}\right)$, ist EV zu $-1$. Ergänze $v_1$ durch $w_1 := \left(\begin{smallmatrix}0 \\ 1\end{smallmatrix}\right)$ zu Basis $C$.

            Dann ist $\prescript{}{C}{[f_A]}_C = \left(\begin{smallmatrix}-1 & -2 \\ 0 & -1\end{smallmatrix}\right)$, denn $f_A\left(\begin{smallmatrix} 0 \\ 1\end{smallmatrix}\right) = \left(\begin{smallmatrix}-4 \\ -3\end{smallmatrix}\right) = -2 \cdot \left(\begin{smallmatrix}2 \\ 1\end{smallmatrix}\right) - 1 \cdot \left(\begin{smallmatrix}0 \\ 1\end{smallmatrix}\right)$.

            Dies ist obere Dreiecksmatrix, und $C = (v_1, w_1)$ eine Fahnenbasis für $A$.

        \item Gesucht: Fahnenbasis für $A = \begin{pmatrix}
                -1 & 1 & 0 & 0\\
                0 & -1 & 0 & 0\\
                0 & 0 & -2 & -1\\
                0 & 0 & 2 & 1
            \end{pmatrix} \in \mathbb{R}^{4 \times 4}$?

            Haben $X_A(T) = (-1 - T)^2 \cdot ((-1 - T) (1 - T) + 2) = (T+1)^2 (T^2 + T) = T(T + 1)^3$ also die EWe $0$ und $-1$ mit algebraischer Vielfachheit $1$ bzw $3$.

            Nun ist $A + I_4 = \begin{pmatrix}
                0 & 1 & 0 & 0\\
                0 & 0 & 0 & 0\\
                0 & 0 & -1 & -1\\
                0 & 0 & 2 & 2
            \end{pmatrix}$, also $\rg (A - (-1) \cdot I_4) = 2$, d.h. $2$ ist die geometrische Vielfachheit des EWes.

            Somit ist $A$ \emph{nicht} diagonalisierbar. Führe wieder das Verfahren durch:

            $(e_1, e_3 - e_4)$ ist eine Basis des Eigenraums $E_{-1}(A)$, erkennbar an der Matrix $A - (-1) \cdot I_4 = A + I_4$. Als Basis von $E_0(A)$ berechnet man $(e_3 -2 e_4)$.

            Ergänzt man die Vektoren $e_1, e_3 - e_4, e_3 - 2 e_4$ durch (z.B.) $e_2$ zu einer Basis $B$ des $\mathbb{R}^4$, so folgt

            \[\prescript{}{B}{[f_A]}_B = \begin{bmatrix}
                -1 & 0 & 0 & 1\\
                0 & -1 & 0 & 0\\
                0 & 0 & 0 & 0\\
                0 & 0 & 0 & -1
            \end{bmatrix}\]

            in oberer Dreiecksform. (Bezüglich Spalte 4: $A \cdot e_2 = e_1 - e_2 = 1 \cdot e_1 + (-1) \cdot e_2$ ($e_2$ ist 4. Vektor in $B$))

            Somit ist $B = (e_1, e_3 - e_4, e_3 - 2 e_4, e_2)$ eine Fahnenbasis.
    \end{enumerate}
\end{example}
