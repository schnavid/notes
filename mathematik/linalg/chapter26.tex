\chapter{Hauptachsentransformation}\label{ch:26}

\paragraph{Stichworte:} Hauptachsentransformation für normale Endos / Matrizen, 1. Spezialfall = selbstadj., 2. Spezialfall: unitär / orthogonal, Isometrie, (reelle) Normalform für orthogonale Endos / Matrizen

\section{Einführung}\label{sec:26.1}

Ist $V$ ein \emph{$n$-dimensionaler} unitärer Raum und $\widetilde{f} \in \End(V)$ normal, so können wir $\widetilde{f}$ über eine beliebig gewählte ONB $B = (v_1, \dots, v_n)$ durch eine normale Matrix $A$ darstellen, d.h. mit $A A^* = A^* A$. DIese Matrix $A$ definiert einen normalen Endomorphismus $f \in \End(\mathbb{C}^n)$, wobei $\mathbb{C}^n$ mit dem Standard-Skalarprodukt versehen ist.

\[
    \xymatrix{
        B \subseteq V \ar[r]^{\widetilde{f}} \ar[d] & V \supseteq B \ar[d] \\
        \mathbb{C}^n \ar[r]_A & \mathbb{C}^n}
\]

Nach Satz~\ref{sec:25.9} besitzt dann der $\mathbb{C}$ eine ONB aus EVen $(x_1, \dots, x_n)$ im $\mathbb{C}^n$, für die gilt:

\[f(x_i) = A x_i = \lambda_i x_i, i = 1, \dots, n, \text{ für } \lambda_i \in \mathbb{C}.\]

Jetzt bilden wir die Matrix $X:= (x_1 | \dots | x_n)$, deren Spalten also diese EVen sind, und $\Lambda := \diag(\lambda_1, \dots, \lambda_n) = \left(\begin{smallmatrix}\lambda_1 & & & 0\\ & \lambda_2 & &\\ & & \ddots &\\ 0 & & & \lambda_n\end{smallmatrix}\right)$.

Dann:

\[AX = A \cdot (x_1 | \dots | x_n) = (A x_1 | \dots | A x_n) = (\lambda_1 x_1 | \dots | \lambda_n x_n) = X \cdot \Lambda.\]

Da die Spalten von $X$ eine ONB bilden, ist laut Satz~\ref{sec:25.8} $X$ eine unitäre Matrix, somit $X^{-1} = X^*$. Also folgt $X^{-1} A X = X^* A X = \Lambda$.

Dies zeigt:

\section{Hauptachsentransformation für normale Endos / normale Matrizen}\label{sec:26.2}

\begin{theorem}
    Jede normale Matrix $A \in \mathbb{C}^{n \times n}$ lässt sich ähnlich auf Diagonalform transformieren: \emph{$X^{-1} A X = \Lambda$}, wobei die Matrix $X$ unitär gewählt werden kann und die Einträge $\lambda_1, \dots, \lambda_n$ der Diagonalmatrix $\Lambda = \diag(\lambda_1, \dots, \lambda_n)$ genau aus den EWen von $A$ besteht.
\end{theorem}

Erster Spezialfall: \emph{Selbstadjungierte Endomorphismen}

Laut Definition~\ref{sec:25.5} heißt ein Endo $f$ eines unitären Raumes selbstadjungiert $(K = \mathbb{C}$) bzw. symmetrisch ($K = \mathbb{R}$), wenn $f = f^*$ bzw. für Matrizen $A = A^*$ bzw. $A = A^T$ gilt. Solche Endos sind insb. normal, so dass die bisherigen Sätze gelten. Darüber hinaus gilt:

\section{Über selbstadjungierte Endos}\label{sec:26.3}

\begin{theorem}
    \begin{enumerate}
        \item Alle EWe eines selbstadjungierten Endos / Matrix sind reell.
        \item Für den Fall $K = \mathbb{R}$ (er ist in Satz~\ref{sec:25.9} nicht erfasst) gilt:

            \noindent\begin{minipage}{0.2\linewidth}``\emph{Haupt\-achsen\-trans\-formation}'' für symmetrische Endos / Matrizen\end{minipage}$\left\{\begin{minipage}{0.8\linewidth}Jede symmetrische (reelle) Matrix $A \in \mathbb{R}^{n \times n}$ besitzt eine ONB aus EVen in $\mathbb{R}^n$. Sie lässt sich mit einer (reellen) orthogonalen Matrix $X$ auf Diagonalform $\Lambda$ bringen: $X^{-1} A X = X^T A X = \Lambda$.\end{minipage}\right.$
    \end{enumerate}
\end{theorem}

\begin{proof}
    \emph{Zu (1):} Sei $x$ ein EV zum EW $\lambda$ des selbstadjungierten Endos $f$. Dann ist

    \begin{align*}
        \lambda \cdot \left\langle x, x \right\rangle &= \left\langle \lambda x, x \right\rangle = \left\langle f(x), x \right\rangle \\
                                                      &= \left\langle x, f^*(x) \right\rangle = \left\langle x, f(x) \right\rangle = \left\langle x, \lambda x \right\rangle = \overline{\lambda} \left\langle x, x \right\rangle,
    \end{align*}

    und da $\left\langle x, x \right\rangle \neq 0$ für $x \neq o$ folgt $\lambda = \overline{\lambda}$, d.h. $\lambda \in \mathbb{R}$.

    \emph{Zu (2):} Jede reelle symmetrische Matrix $A$ definiert einen selbstadjungierten Endo $f$ im $\mathbb{C}^n$. Zu diesem existiert eine ONB $(x_1, \dots, x_n$) des $\mathbb{C}^n$ aus EVen zu \emph{reellen} $\lambda_i$. Nun ist $x \neq o$ EV zum EW $\lambda$ genau wenn $A x - \lambda x = o$, d.h. $x \in \ker(A - \lambda I_n) \subseteq \mathbb{C}^n$. Somit gibt es genau $\dim \ker(A - \lambda I_n)$ in $\mathbb{R}^n$ bestimmen, der dieselbe Dimension hat. Folglich können wir auch eine ONB aus reellen EVen finden.
\end{proof}

\section{Beispiel}\label{sec:26.4}

\begin{example}
    \begin{itemize}
        \item Sei $V = C([0, 1])$ der $\mathbb{R}$-VR der stetigen Funktionen $[0, 1]\to\mathbb{R}$, versehen mit dem Skalarprodukt $\left\langle f, g \right\rangle = \int\limits_0^1 f(t) g(t) dt$. Betrachten den Endo $\Phi: V \to V$ mit $\Phi(f)(t) := t \cdot f(t)$ für alle $t \in [0, 1]$. Dann ist $\Phi$ selbstadjungiert, da $\left\langle \Phi(f), g \right\rangle = \int\limits_0^1 t f(t) g(t) d t = \left\langle f, \Phi(g) \right\rangle$. Aber: $f$ hat \emph{keine} EWe (da $C[0, 1]$ unendlich dimensional, sonst würde~\ref{sec:26.2} greifen), da $\lambda f(t) = \Phi(f)(t) = t f(t)$ ein $\lightning$ für $f \neq o$ liefert.
        \item Die symmetrische Matrix $A = \left(\begin{smallmatrix}
                    1 & 2 \\
                    2 & 1
                \end{smallmatrix}\right)$ hat die EWe $-1$ und $3$, die Transformationsmatrix $X$ mit $X = \frac{1}{\sqrt{2}} \left(\begin{smallmatrix}
                    1 & 1 \\
                    -1 & 1
                    \end{smallmatrix}\right)$ erfüllt $X^{-1} = \frac{1}{\sqrt{2}} \left(\begin{smallmatrix}
                    1 & -1\\
                    1 & 1
                \end{smallmatrix}\right) = X^T$, d.h. $X$ ist orthogonal, und haben $X^{-1} A X = \left(\begin{smallmatrix}
                    -1 & 0\\
                    0 & 3
                \end{smallmatrix}\right)$.
    \end{itemize}
\end{example}

Zweiter Spezialfall: \emph{Unitäre ($K = \mathbb{C}$) bzw. orthogonale $(K = \mathbb{R}$) Endomorphismen}

Für diese gilt $f^* = f^{-1}$, d.h. $f f^* = f^* f = \id$.

\section{Über unitäre / orthogonale Endos}\label{sec:26.5}

\begin{theorem}
    Sei $V$ ein $n$-dimensionaler unitärer Raum ($K \in \{\mathbb{R}, \mathbb{C}\}$), $f\in \End(V)$. Dann sind äquivalent:

    \begin{enumerate}
        \item $f$ ist unitär
        \item Ist $(x_1, \dots, x_n)$ ONB, so auch $(f(x_1), \dots, f(x_n))$. ($f$ bildet beliebige ONBs auf ONBs ab)
        \item Es \emph{gibt} eine ONB $(x_1, \dots, x_n)$, sodass $(f(x_1), \dots, f(x_n))$ ONB.
        \item $\forall x \in V$: $\left\| f(x) \right\| = \left\| x \right\|$, d.h. $f$ ist \emph{isometrisch} bzw. \emph{längentreu}.
        \item $\forall x \in V$: $\left\| x \right\| = 1 \Rightarrow \left\| f(x) \right\| = 1$.
        \item $\forall x, y \in V$: $\left\langle f(x), f(y) \right\rangle = \left\langle x, y \right\rangle$
    \end{enumerate}
\end{theorem}

\begin{proof}
    \emph{$(1) \Rightarrow (2)$: } Ist $f$ unitär, $(x_1, \dots, x_n)$ eine ONB, folgt $\left\langle f(x_i), f(x_j)\right\rangle = \left\langle x_i, \overbrace{f^* \circ f(x_j)}^{= \id_V} \right\rangle = \delta_{ij}$.

    \emph{$(2) \Rightarrow (3)$: } Was in (2) für jede ONB  behauptet wird, wird hier nur für eine gefordert.

    \emph{$(3) \Rightarrow (4)$:} Sei $(x_1, \dots, x_n)$ und $(f(x_1), \dots, f(x_n))$ jeweils eine ONB. Für $x = \sum\limits_{j=1}^n \alpha_j x_j \in V$ gilt dann

        \begin{align*}
            \left\| f(x) \right\|^2 &= \left\langle \sum_i \alpha_i f(x_i), \sum_j \alpha_j f(x_j) \right\rangle\\
                                    &= \sum_{i,j} \alpha_i \overline{\alpha_j} \left\langle f(x_i), f(x_j) \right\rangle = \sum_{i,j} \alpha_i \overline{\alpha_j} \delta_{i,j}\\
                                    &= \sum_{i,j} \alpha_i \overline{\alpha_j} \left\langle x_i, x_j \right\rangle = \left\langle \sum_i \alpha_i x_i, \sum_j \alpha_j x_j \right\rangle = \left\langle x, x \right\rangle = \left\| x \right\|^2.
        \end{align*}
    \emph{$(4) \Rightarrow (5)$:} $\left\| f(x) \right\| = \left\| x \right\| = 1 \Rightarrow \left\| f(x) \right\| = 1$ klar,

    \emph{$(5) \Rightarrow (4)$:} Falls $x=0$, ist (4) klar. Ansonsten setze $z := \frac{1}{\|x\|} \cdot x$, haben

        \[\|z\| = \frac{\|x\|}{\|x\|} = 1 \overbrace{\Rightarrow}^{(5)} 1 = \left\| f(z) \right\| = \left\| f\left(\frac{1}{\|x\|} \cdot x\right)\right\| = \left\| \frac{1}{\|x\|} \cdot f(x) \right\| = \frac{\|f(x)\|}{\|x\|}.\]

    \emph{$(4) \Rightarrow (6)$:} Für alle $x, y \in V$ gilt (durch Nachrechnen):

        \begin{itemize}
            \item im Fall $K = \mathbb{C}$: $\| x + y \|^2 - \|x - y\|^2 + i(\|x + iy\| - \|x - iy\|^2) = 4 \langle x, y \rangle$,
            \item im Fall $K = \mathbb{R}$: $\| x + y \|^2 - \| x - y \|^2 = 4 \langle x, y \rangle$.
        \end{itemize}

        Wenn $f$ nach (4) Normen enthält, muss $f$ also auch Skalarprodukte erhalten.

    \emph{$(6) \Rightarrow (1)$:} Für alle $x, y \in V$ folgt aus (6): $\langle x, y \rangle = \langle (x), f(y) \rangle = \langle x, f^* \circ f(y) \rangle$, also $\langle x, y - f^* \circ f(y) \rangle = 0$, also $y = f^* \circ f(y)$ für jedes $y$, und damit $f^* \circ f = \id_V$. Da $V$ endlich dimensional, ist somit $f^* = f^{-1}$. (surj. / inj. $\Rightarrow$ bij.,~\ref{sec:13.17})
\end{proof}

Wir übersetzen den Sachverhalt von Satz~\ref{sec:26.5} auf Matrizen.

\section{Satz}\label{sec:26.6}

\begin{theorem}
    Sei $A \in \mathbb{C}^{n \times n}$ oder $A \in \mathbb{R}^{n \times n}$. Dann sind äquivalent:

    \begin{enumerate}
        \item $A$ ist unitär ($\mathbb{C}$), d.h. $A^* = A^{-1}$,

            bzw. $A$ ist orthogonal ($\mathbb{R}$), d.h. $A^T = A^{-1}$.
        \item Ist $(x_1, \dots, x_n)$ ONB, so auch $(Ax_1, \dots, Ax_n)$ (des $\mathbb{C}^n$ bzw. des $\mathbb{R}^n$).
        \item Die Spalten von $A$ sind eine ONB (des $\mathbb{C}^n$ bzw. des $\mathbb{R}^n$).
        \item $A$ ist isometrisch / längentreu, d.h. $\forall x: \| Ax \| = \|x\|$
        \item $\forall x: \|x\| = 1 \Rightarrow \|Ax\| = 1$.
        \item $\forall x, y: \left\langle Ax, Ay \right\rangle = \left\langle x, y \right\rangle$.
    \end{enumerate}
\end{theorem}

\begin{proof}
    Direkt aus Satz~\ref{sec:26.5}, bei (3) wurde die Einheitsvektorenbasis benutzt.
\end{proof}

\section{Bemerkung}\label{sec:26.7}

\begin{remark}
    \begin{itemize}
        \item (6) kann man auch als \emph{winkeltreu} bezeichnen, wenn der Kosinussatz~\ref{sec:23.7}, d.h. $\langle x, y \rangle = \|x\| \|y\| \cos \gamma$ beachtet wird.
        \item Anstelle (3) kann man auch noch $(3) \Leftrightarrow (7)$ sagen:

            $(7)$ Die Zeilen von $A$ sind eine ONB.

            Denn die Zeilen von $A$ sind die Spalten von $A^T$. Nun ist auch $A^T$ unitär, da

            \[(A^T)^* \cdot A^T = \overline{A} \cdot A^T = \overline{A \overline{A}^T} = \overline{A A^*} = \overline{I_n} = I_n,\]

            jetzt $(1) \Leftrightarrow (3)$ für $A^T$.
        \item Auch noch: (8) Es gibt ONBen $B$ und $C$ des $\mathbb{C}^n$ bzw. $\mathbb{R}^n$ mit $A = \prescript{}{C}{[I_n]}_B$, dual Beweis.

            Aber wie gesagt, wir möchten \emph{dieselbe} Basis haben, um ähnliche Matrizen zu betrachten \dots
    \end{itemize}
\end{remark}

\section{Definition}\label{sec:26.8}

\begin{definition}
    Für $\dim V < \infty$ bilden die Isometrien $f: V \to V$ eine Untergruppe von $\Aut(V)$, die mit $O(V)$ bezeichnet wird. Die zugehörige Matrixgruppe heißt \emph{$O(n)$}, \emph{orthogonale Gruppe}. Matrizen daraus mit $\det A = 1$ bilden wieder eine UG, nämlich die \emph{spezielle orthogonale Gruppe} \emph{$SO(n)$}. Mit der \emph{allgemeinen linearen Gruppe} \emph{$GL(n)$} $:= \{A \in K^{n \times n}; \det A \neq 0\}$ bezeichnet die Gruppe der invertierbaren Matrizen.

    $O(n) := \{A \in \mathbb{R}^{n \times n}; A \text{ orthogonal}\} = \{A; A^T = A^{-1}\}$ \emph{orthogonale Gruppe}

    $U(n) := \{U \in \mathbb{C}^{n \times n}; U \text{ unitär}\} = \{U; \overline{U^T} = U^{-1}\}$ \emph{unitäre Gruppe}

    $SO(n)$, $SU(n)$, $SL(n)$ sind die UG von $O(n)$, $U(n)$, $GL(n)$

    mit $\det =  +1$ (und heißen \emph{spezielle} \dots Gruppe).
\end{definition}

\section{Bemerkung}\label{sec:26.9}

\begin{remark}
    Isometrien mit $\det A = 1$ heißen \emph{Drehungen},

    die mit $\det A = -1$ heißen \emph{Drehspiegelungen}.

    Zuletzt bringen wir eine beliebige orthogonale Matrix mit einer ONB auf einfache \emph{Normalform}, sodass wir eine klare geometrische Interpretation erhalten.
\end{remark}

\section{Satz}\label{sec:26.10}

\begin{theorem}
    Zu jeder orthogonalen Matrix $A \in \mathbb{R}^{n \times n}$ (d.h. mit $A^T = A^{-1}$) gibt es eine ONB $(x_1, \dots, x_n)$ des $\mathbb{R}^n$ bezüglich der $A$ die folgende Gestalt hat:

    \[X^* A X = \left(\begin{array}{ccccccccc}
            1 && \multicolumn{1}{c|}{} &&&&&&\\
              & \ddots & \multicolumn{1}{c|}{} &&&&& 0 &\\
              && \multicolumn{1}{c|}{1} &&&&&&\\\cline{1-6}
              && \multicolumn{1}{c|}{} & -1 && \multicolumn{1}{c|}{} &&&\\
              && \multicolumn{1}{c|}{} && \ddots & \multicolumn{1}{c|}{} &&&\\
              && \multicolumn{1}{c|}{} &&& \multicolumn{1}{c|}{-1} &&&\\\cline{4-9}
              && & && \multicolumn{1}{c|}{} & \multicolumn{1}{c|}{R_1} &&\\\cline{7-7}
              &0&& && \multicolumn{1}{c|}{} & & \ddots &\\\cline{9-9}
              && & && \multicolumn{1}{c|}{} & & \multicolumn{1}{c|}{} & R_S
    \end{array}\right),\]

    wobei $X = (x_1 | \dots | x_n)$ aus den $x_i$ als Spalten gebildet wird (sodass $x^* = x^{-1}$, beachte $x^* = x^T$).

    und die $R_j, j=1,\dots,S$, Drehmatrizen der Form

    \[R_j = \begin{pmatrix}
        \cos \varphi_j & -\sin \varphi_j\\
        \sin \varphi_j & \cos \varphi_j
    \end{pmatrix}, \varphi_j \in \mathbb{R}\]

    sind.

    Somit bewirkt eine durch eine orthogonale Matrix beschriebene lineare Abbildung folgendes:

    \noindent\begin{minipage}{0.2\linewidth}diese UVRe müssen nicht \emph{alle} existieren\end{minipage}$\left\{\begin{minipage}{0.8\linewidth}\begin{enumerate}
        \item Auf einem UVR, dem Eigenraum zum EW $+1$, wirkt $f$ als \emph{Identität},
        \item auf einem weiteren UVR, dem Eigenraum zu $-1$, wirkt $f$ als \emph{Spiegelung},
        \item auf weiteren $2$-dimensionalen UVRen wirkt $f$ als \emph{ebene Drehung}.
    \end{enumerate}\end{minipage}\right.$

    Alle diese Räume sind paarweise orthogonal und spannen ganz $\mathbb{R}^n$ auf.
\end{theorem}

\begin{proof}
    Der Endo $f: x \mapsto Ax$ des $\mathbb{C}^n$ ist unitär, besitzt also eine Basis aus EVen zu EWen, die alle Betrag $1$ haben (vgl.~\ref{sec:25.9},\ref{sec:25.8}).

    \begin{itemize}
        \item Für reelle EWe, also $+1$ oder $-1$, können wir auf reelle EVen schließen, genau wie in~\ref{sec:26.3}(2) bei symmetrischen Matrizen.
        \item Echt komplexe EWe vom Betrag $1$ haben die Form

            \[\lambda = e^{i\varphi} = \cos \varphi + i \sin \varphi \overbrace{(\neq \pm 1)}^{\varphi \notin \pi\mathbb{Z}}.\]
            
            Bestimme deren EVen: Sei $x = u - iv$ mit $u, v \in \mathbb{R}^n$ ein EV zum EW $\lambda = e^{i\varphi} \neq \pm 1$

            \begin{align*}
                \text{Dann ist } Ax &= Au - iAv = e^{i\varphi} (u - iv) = (\cos\varphi + i \sin\varphi) (u - iv)\\
                                    &= (u \cos \varphi + v \sin \varphi) - i (-u \sin \varphi + v \cos \varphi),
            \end{align*}

            durch vergleich von Real- und Imaginärteil also

            \[Au = u \cos \varphi + v \sin \varphi, Av = -u \in \varphi + v \cos \varphi.\]

            Dann gilt für $\overline{x} = u + iv$:

            \begin{align*}
                A \overline{x} &= Au + iAv = (u \cos \varphi + v \sin \varphi) + i(-u \sin \varphi + v \cos \varphi)\\
                               &= (\cos \varphi - i \sin \varphi) \cdot (u + iv) = e^{-i\varphi} \cdot A \overline{x},
            \end{align*}

            \[\left(\left\{\begin{split}u + iv = \overline{x} \\ u - iv = x\end{split}\right\} \Leftrightarrow \left\{\begin{split}u = \frac{x+\overline{x}}{2}\\v = \frac{\overline{x} - x}{2i}\end{split}\right\} \boxast\right)\]

            also ist $\overline{x}$ EV zum EW $e^{-i\varphi}$ von $A$, und da $e^{i\varphi} \neq e^{-i\varphi}$ für $\varphi \notin \mathbb{Z}\pi$, sind somit $x$ und $\overline{x}$ orthogonal nach~\ref{sec:25.11}(4).

            Folglich ist

            \[0 = \langle x, \overline{x} \rangle = \langle u - iv, u + iv \rangle = \langle u, u \rangle -i \langle v, u \rangle + i \langle u, v \rangle - \langle v, v \rangle,\]

            wobei alle Skalarprodukte der $u$ und $v$ reell sind, insbesondere ist also $\langle u, v \rangle = \langle v, u \rangle$.

            Damit ist $\langle u, v \rangle = 0$, d.h. $u \perp v$ und $\langle u, u \rangle = \langle v, v \rangle$, d.h. $\|u \| = \|v\|$.

            Schließlich können wir noch so normieren, dass $\|u\| = \|v\| = 1$ ist.
        \item Damit folgt: Zu jedem EW $e^{i\varphi} \neq \pm 1$ ist auch $e^{-i\varphi}$ EW, und zu jedem solchen Paar gibt es Vektoren $u, v \in \mathbb{R}^n$, $\|u\| = \|v\| = 1$, $\langle u, v \rangle = 0$ so, dass

            \[Au = u \cos \varphi + v \sin \varphi, Av = -u \sin \varphi + v \cos\varphi,\]

            d.h.

            \[A \cdot (u | v) = (u | v) \cdot \begin{pmatrix}
                \cos \varphi & -\sin \varphi\\
                \sin \varphi & \cos \varphi
            \end{pmatrix}.\]
        \item Nun kann man folgendermaßen eine ONB $(z_1, \dots, z_n) \in \mathbb{R}^n$ zu $A$ gewinnen. Wähle zunächst beliebige orthogonale Basis $(x_1, \dots, x_n) \in \mathbb{C}^n$ aus EVen zu $A$, die wir so anordnen, dass zunächst die zum $EW + 1$ (falls vorhanden), dann die zum EW $-1$ (falls vorhanden) kommen, schließlich die zu echt komplexen EWen $e^{i\varphi_j} \neq \pm 1$ (falls vorhanden). Dazu wählen wir die passenden EVen zu $+1$, $-1$, und die EVen zu $e^{i\varphi_j} \neq \pm 1$ so, dass diese zu Paaren der Form $x_j, \overline{x_j}$ auftreten, wir haben ja gesehen, dass $\overline{x_j}$ zu $e^{-i\varphi_j}$ gehört. Jedes solche Paare können wir durch das oben konstruierte Paar $u_j, v_j$ reeller Vektoren ersetzen.

            Wegen $L(u_j, v_j) = L(x_j, \overline{x_j})$ und der Konstruktion erhält man eine ONB und die gewünschte Darstellung von $A$.
    \end{itemize}
\end{proof}

\section{Beispiel}\label{sec:26.11}

\begin{example}
    Für $\alpha, \beta \in \mathbb{R}$ sei

    \[A := \frac{1}{2} \begin{pmatrix}
        \alpha & 1 - \beta & -\alpha & 1+\beta\\
        1+\beta & \alpha & 1-\beta & -\alpha\\
        -\alpha & 1+\beta & \alpha & 1-\beta\\
        1-\beta & -\alpha & 1+\beta & \alpha
    \end{pmatrix} \in \mathbb{R}^{4 \times 4}.\]

    Dies als Abbildung $\mathbb{C}^4 \to \mathbb{C}^4, z \mapsto Az$ betrachtet ergibt die EWe $\lambda_1 = 1$, $\lambda_2 = -1$, $\lambda_3 = \alpha - i\beta$, $\lambda_4 = \overline{\lambda_3} = \alpha + i\beta$ mit zugehörigen EVen $v_1 = \left(\begin{smallmatrix}1\\1\\1\\1\end{smallmatrix}\right)$, $v_2=\left(\begin{smallmatrix}1\\-1\\1\\-1\end{smallmatrix}\right)$, $v_3=\left(\begin{smallmatrix}1-i\\1+i\\-1+i\\-1-i\end{smallmatrix}\right)$, $\overline{v_3}=\left(\begin{smallmatrix}1+i\\1-i\\-1-i\\-1+i\end{smallmatrix}\right)$.
    

    Nach Satz~\ref{sec:26.10} haben wir, um zur reellen Normalform zu kommen, die Vektoren $x_1 := v_1$, $x_2 := v_2$, $x_3 := \frac{1}{2}(v_3 + v_4)$, $x_4 := \frac{1}{2i} (v_3 - u_4)$ zu bilden ($\boxast$), also $x_1 = \left(\begin{smallmatrix}1\\1\\1\\1\end{smallmatrix}\right)$, $x_2 = \left(\begin{smallmatrix}1\\-1\\1\\-1\end{smallmatrix}\right)$, $x_3 = \left(\begin{smallmatrix}1\\1\\-1\\-1\end{smallmatrix}\right)$, $x_4 = \left(\begin{smallmatrix}-1\\1\\-1\\1\end{smallmatrix}\right)$, die ein OS bilden $\to X^* A X = \begin{pmatrix}1 &&&\\&-1&&\\&&\alpha&-\beta\\&&\beta&\alpha\end{pmatrix}, X = \frac{1}{2} (x_1 | x_2 | x_3 | x_4)$

    Ist $\alpha^2 + \beta^2 = 1$, ist die $2 \times 2$-Matrix $\left(\begin{smallmatrix}\alpha & -\beta\\ \beta & \alpha\end{smallmatrix}\right)$ eine Drehung.
\end{example}

\section{Beispiel}\label{sec:26.12}

\begin{example}
    Wie in Beispiel~\ref{sec:25.13} betrachten $W = C([0, 2\pi])$ und $V \subseteq W$ darin wie dort, versehen mit dem Skalarprodukt $\langle f, g \rangle := \int\limits_0^{2\pi} f(t) \overline{g(t)} dt$ für $f, g \in V$. Für jedes $k \in \mathbb{N}$ ist der Endo $T_k: V \to V, f(t) \mapsto e^{ikt} f(t)$ ein \emph{unitärer} Endo, denn $\forall f, g \in V$:

    \begin{align*}
        \langle T_k(f), T_k(g) \rangle &= \int_0^{2\pi} e^{ikt} f(t) \cdot \overline{e^{ikt} g(t)} dt = \int_0^{2\pi} e^{ikt} f(t) e^{-ikt} \overline{g(t)} dt\\
                                       &= \int_0^{2\pi} f(t) \overline{g(t)} dt = \langle f, g \rangle,
    \end{align*}

    d.h.~\ref{sec:26.5}(6) gilt, der dortige Beweis von ``\ref{sec:26.5}(6) $\Rightarrow$ unitär'' ist auch in $\infty$-dimensionalen unitären Räumen richtig, da er ohne Basiswahl auskommt.
\end{example}

\section{Ergänzung}\label{sec:26.13}

Die in~\ref{sec:26.8} definierten Gruppen sind auch wirklich welche, die dort erklärten Teilmengen von $GL(n)$ sind abgeschlossen bzgl. Multiplikation und Inversenbildung.
So ist z.B. das Produkt orthogonaler Matrizen wieder orthogonal.

Aus $A^T A = I_n$ und $B^T B = I_n$ folgt $(A B)^T (A B) = B^T A^T A B = B^T I_n B = B^T B = I_n$.

\section[Beispiel (Symmetrische Matrix)]{Beispiel für eine Hauptachsentransformation einer Symmetrischen Matrix laut~\ref{sec:26.3}}\label{sec:26.14}

\begin{example}
    Gegeben sei $A = \frac{1}{4} \left(\begin{smallmatrix}5 & -1 & \sqrt{2}\\ -1 & 5 & -\sqrt{2}\\\sqrt{2} & -\sqrt{2} & 6\end{smallmatrix}\right)$, ist symmetrisch: $A^T = A$ \checkmark.

    Bestimme EWe:

    \begin{align*}
        \det(A - T I_3) &= \frac{1}{4^3} \det(4A - 4T \cdot I_3) = \frac{1}{64} \det\begin{pmatrix}
            5 - 4T & -1 & \sqrt{2}\\
            -1 & 5 - 4T & -\sqrt{2}\\
            \sqrt{2} & -\sqrt{2} & 6-4T
        \end{pmatrix}=\dots\\
                        &= \frac{1}{64} (-64 T^3 + 256 T^2 - 320 T + 128) = 0 \Leftrightarrow (T-1)^2(T-2) = 0.
    \end{align*}

    $\to \lambda_1 = 1,\lambda_2 = 2$, EVen zu $\lambda_1 = 1$ sind $\underbrace{\left(\begin{smallmatrix}1\\1\\0\end{smallmatrix}\right)}_{x_1}$, $\underbrace{\left(\begin{smallmatrix}-1\\1\\\sqrt{2}\end{smallmatrix}\right)}_{x_2}$, zu $\lambda_2 = 2$ ist $\underbrace{\left(\begin{smallmatrix}\sqrt{2}\\-\sqrt{2}\\2\end{smallmatrix}\right)}_{x_3}$.

    \[\text{Normierung } \to X = \underbrace{\frac{1}{2\sqrt{2}}\begin{pmatrix}2 & -\sqrt{2} & \sqrt{2}\\ 2 & \sqrt{2} & -\sqrt{2}\\ 0 & 2 & 2\end{pmatrix}}_{\text{ONB aus EVen}}.\]

    Dann gilt $X^T A X = \begin{pmatrix}1 & 0 & 0\\0 & 1 & 0\\ 0 & 0 & 2\end{pmatrix}$.
\end{example}

\section[Beispiel (Orthogonale Matrix)]{Beispiel für eine Hauptachsentransformation einer Orthogonalen Matrix laut~\ref{sec:26.10}}\label{sec:26.15}

\begin{example}
    Betrachten $A = \frac{1}{5} \left(\begin{smallmatrix}-3 & 4 \\ 4 & 3\end{smallmatrix}\right) \in \mathbb{R}^{2 \times 2}$, ist orthogonal, da Spalten eine ONB des $\mathbb{R}^2$ bilden.

    EWe ausrechnen:

    \begin{align*}
        \det(A - T I_2) &= \frac{1}{25} \det\begin{pmatrix} -3 -5T & 4\\ 4 & 3-5T\end{pmatrix}\\
                        &= \frac{1}{25} \left((5T)^2 -9 -16\right) = T^2 - 1 = (T-1)(T+1),
    \end{align*}

    also $\lambda_1 = 1$, $\lambda_2 = -1$ sind die EWe. Die EVen sind $x_1 = \left(\begin{smallmatrix}1\\2\end{smallmatrix}\right)$, $x_2 = \left(\begin{smallmatrix}-2\\1\end{smallmatrix}\right)$ $\to$ mit $X = \frac{1}{\sqrt{5}} \left(\begin{smallmatrix}1&-2\\2&1\end{smallmatrix}\right)$ ist

    \begin{align*}
        X^T A X &= \frac{1}{25} \begin{pmatrix}1 & 2\\-2 & 1\end{pmatrix}\cdot \begin{pmatrix}-3 & 4\\ 4 & 3\end{pmatrix}\cdot \begin{pmatrix}1 & -2\\2 & 1\end{pmatrix}\\
        &= \frac{1}{25} \begin{pmatrix}1 & 2 \\ -2 & 1\end{pmatrix} \cdot \begin{pmatrix}5 & 10\\ 10 & -5\end{pmatrix}\\
        &= \frac{1}{25} \begin{pmatrix}25 & 0\\0 & -25\end{pmatrix} = \begin{pmatrix} 1 & 0\\0 & -1\end{pmatrix} \leftarrow \text{ Spiegelung an ``$x$-Achse''}
    \end{align*}
\end{example}
