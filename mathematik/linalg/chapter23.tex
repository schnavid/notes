\chapter{Geometrie im $\mathbb{R}^n$}\label{ch:23}

\paragraph{Stichworte:} senkrecht / orthogonal, elementargeometrische Sätze, Winkelmessung, Kosinussatz, Drehmatrizen, Vektorprodukt, Orthonormalsystem, Orientierung eines ONS, Hesse-Normalform, Lot

Der $\mathbb{R}^n$ versehen mit dem Standard-S.P. ist \emph{der} Prototyp eines euklidischen Raumes. Wir beschäftigen uns hier ein wenig mit der anschaulichen Geometrie, die in euklidischen Räumen gilt. Sei im folgenden $\langle \cdot, \cdot \rangle$ das \emph{reelle} Standard-S.P.

\section{Orthogonal}\label{sec:23.1}

\begin{definition}
    Im $\mathbb{R}^n$ heißen zwei Vektoren $x, y \in \mathbb{R}^n$ \emph{senkrecht aufeiner} / \emph{orthogonal}, falls $\langle x, y \rangle = 0$ ist. In Zeichen: \emph{$x \perp y$}.

\end{definition}

\begin{example}
    Anschaulich im $\mathbb{R}^2$:

    \[\langle x, y \rangle = \left\langle \begin{pmatrix}\xi_1\\\xi_2\end{pmatrix}, \begin{pmatrix}\eta_1\\\eta_2\end{pmatrix}\right\rangle=\xi_1\eta_1+\xi_2\eta_2\]

    \begin{center}
        \begin{tikzpicture}
            \coordinate (x) at (3,1);
            \coordinate (o) at (0,0);
            \coordinate (y) at (-1, 3);

            \draw[thick, <->]
                (x)node[above right] {$x$}
                -- (o)
                -- (y) node[above left] {$y$};
            \draw[thick] (-3, 0) -- (4, 0);
            \draw[thick] (0, 0) -- (0, 3);

            \draw[dashed, thick] (-1,0) -- (y) node[midway, left] {$\xi_1$};

            \draw[dashed, thick] (3,0) -- (x) node[midway, right] {$\xi_2$};

            \draw[decorate, decoration={brace, amplitude=5pt}] (0,0) -- (-1,0) node[midway, below] {$-\xi_2$};

            \draw[decorate, decoration={brace, amplitude=5pt}] (3,0) -- (0,0) node[midway, below] {$\xi_1$};

            \tkzMarkRightAngle[size=.5](x,o,y)
            \tkzLabelAngle[pos=.3](x,o,y){$\cdot$}
        \end{tikzpicture}

        Vektoren $x, y$ gleicher Länge:

        \[x = \begin{pmatrix}\xi_1\\\xi_2\end{pmatrix}, y=\begin{pmatrix}-\xi_2\\\xi_1\end{pmatrix} \text{ oder } \begin{pmatrix}\xi_2\\-\xi_1\end{pmatrix}\]
    \end{center}
\end{example}

\begin{remark}
    Der Nullvektor $o$ ist orthogonal zu jedem Vektor.
\end{remark}

Wir werden in~\ref{ch:24} mehr über Orthogonalität in unitären Räumen erfahren.

\section[Elementargeometrische Sätze]{Elementargeometrische Sätze mit vektiellem Beweis (im $\mathbb{R}^n$, eine Auswahl)}\label{sec:23.2}

\begin{itemize}
    \item \emph{Satz vom Rechteck:} Parallelogramm ist Rechteck $\Leftrightarrow$ Diagonalen gleichlang

        \begin{align*}
            &\|x-y\|^2 = \|x+y\|^2\\
            \Leftrightarrow &\|x\|^2 - 2\langle x, y\rangle + \|y\|^2 = \|x\|^2 + 2\langle x, y \rangle + \|y\|^2\\
            \Leftrightarrow &\langle x, y \rangle = 0
        \end{align*}
    \item \emph{Satz vom Rhombus:} Parallelogramm hat gleichlange Seiten $\Leftrightarrow$ Diagonalen senkrecht

        \[\langle x - y, x+y\rangle = \|x\|^2 - \|y\|^2, \text{ somit: } \|x\| = \|y\| \Leftrightarrow \langle x-y,x+y\rangle = 0\]
    \item \emph{Satz von Pythagoras:} Dreieck rechtwinklig $\Leftrightarrow$ $\|x\|^2 + \|y\|^2 = \|x - y\|^2$

        Klar mit $\|x - y\|^2 = \|x\|^2 - 2\langle x, y \rangle + \|y\|^2$
    \item Drehung um $\frac{\pi}{2}$ im $\mathbb{R}^2$: $\perp: \mathbb{R}^2 \to \mathbb{R}^2, x \mapsto x^\perp := \begin{pmatrix}-\xi_2\\\xi_1\end{pmatrix}$, falls $x = \begin{pmatrix}\xi_1\\\xi_2\end{pmatrix}$
    \item Lie-Klammer im $\mathbb{R}^2$: $[x, y] := \langle x^\perp, y \rangle = \det(x | y)$, ist bilinear \& alternierend: $[x, y] = -[y, x]$
    \item Es gilt im $\mathbb{R}^2 : \langle x, y \rangle^2 + [x, y]^2 = \|x\|^2 + \|y\|^2$, vgl. C-S-Ungleichung~\ref{sec:22.15}.
\end{itemize}

\section{Winkelmessung (\enquote{Trigonometrie})}\label{sec:23.3}

\begin{center}
    \begin{minipage}{0.4\linewidth}
        \begin{center}
            \begin{tikzpicture}
                \coordinate (O) at (0, 0);
                \coordinate (P) at (70:2);
                \coordinate (R) at (40:2);

                \draw[->, thick] (70:1) arc (70:400:1);

                \draw[thick] (O) -- (P);
                \draw[thick] (O) -- (R);

                \node[above] at (P) {$P$};
                \node[right] at (R) {$R$};
                \node[below left] at (O) {$O$};
            \end{tikzpicture}

            Positiver Winkel (gegen Uhrzeigersinn)
        \end{center}
    \end{minipage}\begin{minipage}{0.4\linewidth}
        \begin{center}
            \begin{tikzpicture}
                \coordinate (O) at (0, 0);
                \coordinate (P) at (70:2);
                \coordinate (R) at (40:2);

                \draw[->, thick] (70:1) arc (70:40:1);

                \draw[thick] (O) -- (P);
                \draw[thick] (O) -- (R);

                \node[above] at (P) {$P$};
                \node[right] at (R) {$R$};
                \node[below left] at (O) {$O$};
            \end{tikzpicture}

            Negativer Winkel (im Uhrzeigersinn)
        \end{center}
    \end{minipage}
\end{center}

Durch die Angabe dreier Punkte $P, O, R$ in $\mathbb{R}^n$ wird (in dieser Reihenfolge) ein (gerichteter) Winkel $\sphericalangle(P O R)$ am Scheitelpunkt $O$ erklärt. Seiner Größe soll ein Zahlenwert $\in \mathbb{R}$ zugeordnet werden, den Wert des Winkels. Das Vorzeichen soll angeben, ob der Winkel rechts ($\to +$) oder links ($\to -$) des Streckenzuges von $P$ nach $O$ nach $R$ verläuft; er verläuft dann gegen bzw. im Uhrzeigersinn.

\section{Winkel im Kreismodell}\label{sec:23.4}

Wählen \OE $P = \left(\begin{smallmatrix}1\\0\end{smallmatrix}\right) \in \mathbb{R}^2$, $O = \left(\begin{smallmatrix}0\\0\end{smallmatrix}\right)$ als Scheitelpunkt fest, und $R$ auf dem \emph{Einheitkreis} liegend (sodass $\|R\| = 1$).

\begin{center}
    \begin{tikzpicture}
        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 0);
        \coordinate (R) at (45:1);

        \draw[thick] (-1.5, 0) -- (1.5, 0);
        \draw[thick] (0, -1.5) -- (0, 1.5);
        \draw[thick] (O) circle (1);
        \draw[thick] (0, 0) -- (1.15, 1.15);
        \draw[thick] (0.75, 0) arc(0:45:0.75);

        \node[below right] at (P) {$P$};
        \node[above] at (R) {$R$};
        \node[below left] at (O) {$O$};
        \node at (22.5:0.5) {$\alpha$};
    \end{tikzpicture}
\end{center}

Jeder Punkt R stegt dann für einen Winkel $\alpha \in \mathbb{R}$.

Der Betrag des Winkels wird mit der \emph{Länge des Bogens} von $P$ nach $R$ angegeben. Für $R = \left(\begin{smallmatrix}-1\\0\end{smallmatrix}\right)$, der sogenannte \emph{gestreckte Winkel}, wird diese Zahl \emph{$\pi$} genannt (d.h die Kreiszahl $\pi$) wird so definiert (in Analysis \RN{2} wird gezeigt, dass diese Zahl $\pi$ mit der aus Analysis \RN{1} gemachten Definition von $\pi$ ($\frac{\pi}{2} =$ kleinste positive Nullstelle vom $\cos$) übereinstimmt). Der halbe gestreckte Winkel ist der \emph{rechte Winkel} (d.h.  $RO$, $PO$ mit $R = \left(\begin{smallmatrix}0\\1\end{smallmatrix}\right)$ stehen senkrecht), beträgt also $\frac{\pi}{2}$. Dem Vollkreis entsprechen $2\pi$. Mit mehrmaligen Durchlaufen des Kreises werden so beliebige Winkel $\alpha \in \mathbb{R}$ erklärt (im \emph{Bogenmaß}; die Umrechnung in das \emph{Gradmaß} ist linear gemäß $180 \textdegree = \pi$).

\section{Winkelfunktionen}\label{sec:23.5}

\begin{definition}
    Die beiden Koordinaten von $R_\alpha = \left(\begin{smallmatrix}\xi_1\\\xi_2\end{smallmatrix}\right)$ im Einheitskreismodell heißen \emph{Kosinus} und \emph{Sinus} von $\alpha$, also $\cos \alpha := \xi_1$, $\sin \alpha := \xi_2$, sodass wir $R = \left(\begin{smallmatrix}\cos\alpha\\\sin\alpha\end{smallmatrix}\right)$ schreiben.

    Man erhält die \emph{Winkelfunktionen} $\cos: \mathbb{R} \to [-1,1]$, $\sin: \mathbb{R} \to [-1, 1]$.
\end{definition}

\section{Bemerkung}\label{sec:23.6}

\begin{remark}
    \begin{enumerate}
        \item Schließt ein Vektor $x = \begin{pmatrix}\xi_1 \\ \xi_2\end{pmatrix} \in \mathbb{R}^2$ mit der 1. Koordinatenachse den Winkel $\alpha$ ein, so ist $\xi_1 = \|x\| \cos \alpha$ und $\xi_2 = \|x\| \sin \alpha$, da $x = \|x\| \cdot R_\alpha$ für den Referenzpunkt $R_\alpha$.
        \item Haben $|e^{i\alpha}| = 1$ für jedes $\alpha \in \mathbb{R}$ laut Analysis \RN{1}. Mit der dortigen Formel $e^{i\alpha} = \cos\alpha + i\sin\alpha$ schließt man, wenn man $\mathbb{R}^2 \cong \mathbb{C}$ beachtet, dass die Winkelfunktionen der Analysis \RN{2} genau mit den hier erklärten Winkelfunktonen übereinstimmen. Aus $e^{i(\alpha + \beta)} = e^{i\alpha} \cdot e^{i\beta}$ erhält man dann die Additionstheoreme und alle daraus resultierenden Formeln, speziell das Additionstheorem für den $\cos$:

            \[\cos(\alpha + \beta) = \Re\left(e^{i\alpha} \cdot e^{i\beta}\right) = \cos\alpha\cos\beta - \sin\alpha\sin\beta.\]
    \end{enumerate}
\end{remark}

\section{Kosinussatz}\label{sec:23.7}

\begin{theorem}
    Für den Winkel $\alpha = \sphericalangle(x, y)$ zwischen $x, y \in \mathbb{R}^n$ gilt $\cos \alpha = \frac{\langle x, y \rangle}{\|x\| \cdot \|y\|}$.
\end{theorem}

\begin{proof}
    \begin{itemize}
        \item \emph{Fall $n=2$:} Haben $\alpha = \sphericalangle\left(x, L\left(\begin{smallmatrix}1\\0\end{smallmatrix}\right)\right) - \sphericalangle\left(y, L\left(\begin{smallmatrix}1\\0\end{smallmatrix}\right)\right) =: \varphi - \psi$, nach dem Additionstheorem für den $\cos$ folgt

            \begin{align*}
                \cos \alpha &\ueq \cos(\varphi - \psi) = \cos\varphi\cos\psi + \sin\varphi\sin\psi\\
                            &\ueq[\ref{sec:23.6}.1.] \frac{\xi_1}{\|x\|} \cdot \frac{\eta_1}{\|y\|} + \frac{\xi_2}{\|x\|} \cdot \frac{\eta_2}{\|y\|} = \frac{\overbrace{\langle x, y \rangle}^{\mathclap{\text{S.P. im $\mathbb{R}^2$}}}}{\|x\| \cdot \|y\|}.
            \end{align*}
        \item Für den Winkel $\alpha$ zwischen $x, y$ gilt also

            \[\|x - y\|^2 = \|x\|^2 + \|y\|^2 - 2\langle x, y \rangle = \|x\|^2 + \|y\|^2 - 2\|x\| \cdot \|y\| \cos \alpha.\]
        \item \emph{Beliebiges $n$:} Die letzte Formel gilt für jedes $n$. Damit ist für jedes $n$ richtig, dass $\|x\|^2 + \|y\|^2 - 2 \|x\| \cdot \|y\| \cos \alpha = \|x - y\|^2 = \|x\|^2 + \|y\|^2 - 2 \underbrace{\langle x, y \rangle}_{\mathclap{\text{S.P. im $\mathbb{R}^n$}}}$, also $\cos\alpha = \frac{\langle x, y \rangle}{\|x\| \cdot \|y\|}$ für den zwischen $x, y$ eingeschlossenen Winkel $\alpha$ richtig ist.
    \end{itemize}
\end{proof}

\section{Drehmatrizen}\label{sec:23.8}

Sei $f_\alpha: \mathbb{R}^2 \to \mathbb{R}^2$ die lineare Abbildung, die die Drehung um $\alpha$ (mit Drehzentrum $O$) bewirke. Die zugehörige Matrix $A_\alpha$ erhalten wir mit den Bildern der Einheitsvektoren als Spalten.

Haben $f_\alpha\begin{pmatrix}1\\0\end{pmatrix} = \begin{pmatrix}\cos \alpha\\\sin \alpha\end{pmatrix}$ per Definition und $f_\alpha\begin{pmatrix}0\\1\end{pmatrix} = \begin{pmatrix}-\sin\alpha\\\cos\alpha\end{pmatrix}$.

\begin{center}
    \begin{tikzpicture}[scale=3]
        \draw[thick, ->] (-1, 0) -- (2, 0);
        \draw[thick, ->] (0, 0) -- (0, 1.5);

        \coordinate[label=below:$\begin{pmatrix}1\\0\end{pmatrix}$] (A) at (1, 0);
        \coordinate[label=right:$\begin{pmatrix}0\\1\end{pmatrix}$] (B) at (0, 1);

        \coordinate (R) at ({cos(20)}, {sin(20)});
        \coordinate (S) at ({-sin(20)}, {cos(20)});

        \draw[dashed, ->] (A) -- (R);
        \draw[dashed, ->] (B) -- (S);
        \draw[dashed] ({-sin(20)}, 0) -- (S);

        \draw[thick] (0, 0) -- (R);
        \draw[thick] (0, 0) -- (S);

        \draw[decorate, decoration={brace, amplitude=5pt}] ({-sin(20)}, 0) -- (S) node[left, midway] {$\cos\alpha$}; 
        \draw[decorate, decoration={brace, amplitude=5pt}] (0, 0) -- ({-sin(20)}, 0) node[below, midway] {$-\sin\alpha$};

        \node[right] at (R) {$R_\alpha = \begin{pmatrix} \cos\alpha \\ \sin\alpha \end{pmatrix}$};

        \draw (0.6, 0) arc(0:20:0.6);
        \node at (10:0.4) {$\alpha$};

        \draw (0, 0.6) arc(90:110:0.6);
        \node at (100:0.4) {$\alpha$};
    \end{tikzpicture}
\end{center}

Damit wird $f_\alpha(x) = A_\alpha \cdot x$ mit der \emph{Drehmatrix} $D_\alpha := \begin{pmatrix} \cos\alpha & -\sin\alpha\\ \sin\alpha & \cos\alpha \end{pmatrix}, \alpha\in\mathbb{R}$.

\begin{example}
    Der Drehung um $\frac{\pi}{2}$, d.h. $x \mapsto x^\perp$ aus~\ref{sec:23.2}, entspricht die Drehmatrix

    \[\begin{pmatrix} 0 & -1 \\ 1 & 0 \end{pmatrix} = \begin{pmatrix} \cos\frac{\pi}{2} & -\sin\frac{\pi}{2} \\ \sin\frac{\pi}{2} & \cos\frac{\pi}{2} \end{pmatrix}.\]
\end{example}

Die Hintereinanderausführung zweier Drehungen $f_\alpha \circ f_\beta$, also $D_\alpha \cdot D_\beta$, ergibt $f_{\alpha + \beta}$, denn:

\begin{align*}
    D_\alpha \cdot D_\beta &= \begin{pmatrix} \cos\alpha & -\sin\alpha \\ \sin\alpha & \cos\alpha \end{pmatrix} \cdot \begin{pmatrix} \cos\beta & -\sin\beta \\ \sin\beta & \cos\beta \end{pmatrix}\\
    &= \begin{pmatrix} \cos\alpha \cos\beta - \sin\alpha \sin\beta & -\cos\alpha\sin\beta - \sin\alpha\cos\beta \\ \sin\alpha\cos\beta + \cos\alpha\sin\beta & -\sin\alpha\sin\beta + \cos\alpha\cos\beta \end{pmatrix}\\
    &= D_{\alpha + \beta} \text{ laut Additionstheorem.}
\end{align*}

Kein Wunder: den komplexen Zahlen $z_1 = e^{i\alpha}$, $z_2 = e^{i\beta}$ entsprechen $R_\alpha = \begin{pmatrix} \cos\alpha \\ \sin\alpha \end{pmatrix}$, $R_\beta = \begin{pmatrix} \cos\beta \\ \sin\beta \end{pmatrix}$ im $\mathbb{R}^2$, deren Produkt $z_1 \cdot z_2 = e^{i(\alpha + \beta)}$ der Winkelsumme $\alpha + \beta$. Die Multiplikation $e^{i\alpha} \cdot \underbrace{(\xi_1 + i\xi_2)}_x$ entspricht der Drehung von $x$ um $\alpha$, denn $x = \|x\| \cdot e^{i\varphi} \to x \cdot e^{i\alpha} = \|x\| \cdot e^{i(\varphi + \alpha)}$.

\begin{remark}
    $D_\alpha$ hat die beiden EWe $e^{i\alpha}$, $e^{-i\alpha}$. (Übung: Was sind die zugehörigen EVen?)
\end{remark}

\section{Polarkoordinaten}\label{sec:23.9}

Jedes $x \in \mathbb{R}^2$ lässt sich eindeutig in der Form $x = r \cdot e^{i\alpha}$ schreiben, wo $r = \|x\|$ und $\alpha \in [0, 2\pi[$; mit $\alpha \in \mathbb{R}$ ist $\alpha$ nur bis auf Vielfache von $2\pi$ eindeutig. Man nennt das Paar $(r, \alpha) \in \mathbb{R}_{\geq 0} \times [0, 2\pi[$ dann die \emph{Polarkoordinaten von $x$}.

Für $z \in \mathbb{C}$ hat man ebenso $z = r \cdot e^{i\alpha}$, $r = |z|$, mit $(r, \alpha)$ als \emph{Polarkoordinate von $z$}.

Wegen~\ref{sec:23.8} kann die Darstellung von $z \in \mathbb{C}$, bzw. $x \in \mathbb{R}^2$ in Polarkoordinaten nützlich sein (weiteres dazu in Analysis \RN{1}, \RN{2}).

\section{Das Vektorprodukt im $\mathbb{R}^3$}\label{sec:23.10}

\begin{definition}
    Zu $x = \begin{pmatrix} \xi_1 \\ \xi_2 \\ \xi_3 \end{pmatrix}, y = \begin{pmatrix} \eta_1 \\ \eta_2 \\ \eta_3 \end{pmatrix} \in \mathbb{R}^3$ sei

    \[x \times y := \begin{pmatrix} \xi_2\eta_3 - \xi_3\eta_2 \\ \xi_3\eta_1 - \xi_1\eta_3 \\ \xi_1\eta_2 - \xi_2\eta_1 \end{pmatrix} \in \mathbb{R}^3\]

    das \emph{Vektorprodukt von $x$ und $y$}.
\end{definition}

\section{Eigenschaften des Vektorprodukts}\label{sec:23.11}

\begin{theorem}
    Für $u, v, w \in \mathbb{R}^3$, $\alpha, \beta \in \mathbb{R}$ gelten:

    \begin{enumerate}
        \item $(\alpha u + \beta v) \times w = \alpha (u \times w) + \beta (v \times w)$, $u \times (\alpha v + \beta w) = \alpha (u \times v) + \beta (u \times w)$,
        \item $u \times v = -v \times u$, d.h. $\times$ ist antisymmetrisch,
        \item $u$ und $v$ sind je orthogonal zu $u \times v$, d.h. $\langle u, u \times v \rangle = 0 = \langle v, u \times v \rangle$,
        \item für $\alpha = \sphericalangle (u, v)$ ist $\| u \times v \|^2 = \|u\|^2 \cdot \|v\|^2 - \langle u, v \rangle^2 = \|u\|^2 \|v\|^2 \sin^2 \alpha$.
        \item für die Einheitsvektoren $e_1, e_2, e_3$ gilt die Multiplikationstabelle

            \[\begin{array}{c|ccc}
                x & e_1 & e_2 & e_3 \\\hline
                e_1 & o & e_3 & -e_2 \\
                e_2 & -e_3 & o & e_1 \\
                e_3 & e_2 & -e_1 & o
            \end{array}\]
        \item es gilt $u \times v = o \Leftrightarrow \exists \alpha, \beta: \alpha u + \beta v = o$ und nicht $\alpha = \beta = 0$, d.h. $u, v$ sind linear abhängig.
        \item es gilt $\langle u, v \times w \rangle = \det(u, v, w)$.
    \end{enumerate}
\end{theorem}

\begin{proof}
    \emph{(1) - (3), (5)}: geht mit einfachem Nachrechnen, ebenso

    \emph{(4)}: \begin{align*}
        \|u \times v \|^2 = &(\mu_2 \nu_3 - \mu_3 \nu_2)^2 + (\mu_3 \nu_1 - \mu_1\nu_3)^2 + (\mu_1\nu_2 - \mu_2\nu_1)^2\\
        = &(\mu_1\nu_2)^2 + (\mu_1\nu_3)^2 + (\mu_2\nu_1)^2 + (\mu_2\nu_3)^2 + (\mu_3\nu_1)^2 + (\mu_3\nu_2)^2\\
          &- 2(\mu_1\mu_2\nu_1\nu_2 + \mu_1\mu_3\nu_1\nu_3 + \mu_2\mu_3\nu_2\nu_3)\\
        = &(\mu_1^2 + \mu_2^ 2 + \mu_3^2) \cdot (\nu_1^2 + \nu_2^2 + \nu_3^2) - (\mu_1\nu_1 + \mu_2\nu_2 + \mu_3\nu_3)^2\\
        = &\|u\|^2 \cdot \|v\|^2 - \langle u, v \rangle^2 \underbrace{=}_{\mathclap{\text{$\cos$-Satz~\ref{sec:23.7}}}} \|u\|^2 \|v\|^2 - \|u\|^2 \|v\|^2 \cos^2 \alpha\\
        = &\|u\|^2 \|v\|^2 \sin^2\alpha\text{, da } 1 - \cos^2\alpha = \sin^2\alpha.
    \end{align*}

    \emph{(6)} Ist $v = o$, ist $u \times v = o$ und $0 \cdot u + 1 \cdot v = o$ richtig. Sei also \OE $v \neq o$.

    \begin{itemize}
        \item Haben: $u \times v = o \Rightarrow \|u\| \cdot \|v\| = \abs{\langle u, v \rangle}$. Nach dem Zusatz der C-S-Ungleichung~\ref{sec:22.15} sind dann $u, v$ linear abhängig, dann existiert $\alpha, \beta \in \mathbb{R}$, nicht $\alpha = \beta = o$, mit $\alpha u + \beta v = o$.
        \item Sei umgekehrt $\alpha u + \beta v = o$ aber nicht $\alpha = \beta = o$. Da $v \neq o$ ist $\alpha \neq 0$, also $u = - \frac{\beta}{\alpha} v$. Mit $S:= - \frac{\beta}{\alpha}$ ist dann $u \times v = S(v \times v) = v \times (S v) = v \times u = -u \times v$, also $u \times v = o$.
    \end{itemize}

    \emph{(7)}: Zeige (D1), (D2), (D3) in~\ref{sec:18.2} für $f(u, v, w) := \langle u, v \times w \rangle$. Die $3$-Linearität (D1) ist klar wegen (1) und S.P.-Linearität, (D2) ist klar mit (6), (D3) auch klar.
\end{proof}

\begin{remark}
    Zu (4): die r.S. ist der Flächeninhalt des Parallelogramms, das von $u, v$ aufgespannt wird. Für $u = \begin{pmatrix} \mu_1 \\ \mu_2 \\ 0 \end{pmatrix}, v = \begin{pmatrix} \nu_1 \\ \nu_2 \\ 0 \end{pmatrix}$ ist $\|u \times v\| = \left\| \begin{pmatrix} \det(u', v') \\ 0 \\ 0 \end{pmatrix} \right\| = \abs{\det(u', v')} = \abs{\det\begin{pmatrix} \mu_1 & \nu_1 \\ \mu_2 & \nu_2 \end{pmatrix}}$.
\end{remark}

Die Standardbasis $(e_1, e_2, e_3)$ im $\mathbb{R}^3$ bilden ein \emph{Orthogonalsystem}, weil je zwei von ihnen senkrecht aufeinander stehen. Da $\|e_i\| = 1$ für $i = 1, 2, 3$, sind sie \emph{normiert}, wir sprechen von einem \emph{Orthogonalsystem}, kurz ONS (in beliebig unitären Räumen definieren wir dies in~\ref{ch:24}).

Im $\mathbb{R}^3$ kann man ONSe grundsätzlich in zwei Sorten aufteilen gemäß \enquote{Orientierung}.

\section{ONSe}\label{sec:23.12}

\begin{definition}
    \begin{itemize}
        \item Drei Vektoren $x_1, x_2, x_3 \in \mathbb{R}^3$ bilden ein \emph{ONS}, wenn

            \[\forall i, j \in \{1, 2, 3\}: \langle x_i, x_j \rangle = \begin{cases} 1, & i=j \\ 0, & i \neq j \end{cases}.\]
        \item Ein ONS $(x_1, x_2, x_3)$ im $\mathbb{R}^3$ heißt 
            \begin{itemize}
                \item \emph{positiv orientiert}, wenn $\det(x_1, x_2, x_3) = +1$, und
                \item \emph{negativ orientiert}, wenn $\det(x_1, x_2, x_3) = -1$.
            \end{itemize}
    \end{itemize}
\end{definition}

\section{Bemerkung}\label{sec:23.13}

\begin{remark}
    \begin{itemize}
        \item Ein ONS ist linear unabhängig und daher eine Basis des $\mathbb{R}^3$

            \[\exists \lambda_i x_i = o \Rightarrow 0 = \langle o, x_j \rangle = \langle \sum \lambda_i x_i, x_j \rangle = \sum \lambda_i \overbrace{\langle x_i, x_j \rangle}^{=\delta_{i,j}} = \lambda_j \text{ für jedes $j$.}\]
        \item Ein ONS hat stets positive oder negative Orientierung.

            Haben also Darstellung als LK: $x_2 \times x_3 = \lambda_1 x_1 + \lambda_2 x_2 + \lambda_3 x_3$, da $(x_1, x_2, x_3)$ Basis $\Leftrightarrow 0 = \langle x_2, x_2 \times x_3 \rangle = \lambda_2$, ebenso $\lambda_3 = 0$, also $x_2 \times x_3 = \lambda_1 x_1$.

            Da $\|x_2 \times x_3\| = 1 = \|x_1\|$, folgt $\lambda_1 = \pm 1$ und $\det(x_1, x_2, x_3) = \langle x_1, x_2 \times x_3 \rangle = \langle x_1, \lambda_1 x_1 \rangle = \lambda_1 = \pm 1$.

            Anschaulich:

        \begin{minipage}{0.4\linewidth}
                \begin{tikzpicture}
                    \draw[thick, ->] (0,0) -- (2,0);
                    \draw[thick, ->] (0,0) -- (0,2);
                    \draw[thick, ->] (0,0) -- (45:2);

                    \node[above right] at (45:2) {$e_2$};
                    \node[above] at (0,2) {$e_3$};
                    \node[right] at (2,0) {$e_1$};
                \end{tikzpicture}

                \emph{Orientierung $\oplus$:}

                $e_1, e_2, e_3$: Daumen, Zeigefinger, Mittelfinger der \emph{rechten} Hand
            \end{minipage}\hfill\begin{minipage}{0.4\linewidth}
                \begin{tikzpicture}
                    \draw[thick, ->] (0,0) -- (2,0);
                    \draw[thick, ->] (0,0) -- (0,-2);
                    \draw[thick, ->] (0,0) -- (45:2);

                    \node[above right] at (45:2) {$e_2$};
                    \node[below] at (0,-2) {$-e_3$};
                    \node[right] at (2,0) {$e_1$};
                \end{tikzpicture}

                \emph{Orientierung $\ominus$:}

                $e_1, e_2, -e_3$: Daumen, Zeigefinger, Mittelfinger der \emph{linken} Hand
            \end{minipage}
    \end{itemize}
\end{remark}

\section{Satz}\label{sec:23.14}

Man kann mit einem ONS leicht rechnen, was die Koordinatendarstellung betrifft:

\begin{theorem}
    Sind $(x_1, x_2, x_3) \in \mathbb{R}^3$ ein ONS, gilt $\forall y \in \mathbb{R}^3$:

    \begin{enumerate}[label=(\arabic*)]
        \item $y = \langle y, x_1\rangle x_1 + \langle y, x_2 \rangle x_2 + \langle y, x_3 \rangle x_3$,
        \item $\|y\|^2 = \langle y, x_1 \rangle^2 + \langle y, x_2 \rangle^2 + \langle y, x_3 \rangle^2$.
        \item Es gilt $x_3 = x_1 \times x_2$ (positiv orientiert) oder $x_3 = -x_1 \times x_2$ (negativ orientiert)
    \end{enumerate}
\end{theorem}

\begin{proof}
    \begin{enumerate}[label=(\arabic*)]
        \item $y = \lambda_1 x_1 + \lambda_2 x_2 + \lambda_3 x_3$ zeigt $\langle y, x_j \rangle = \sum\lambda_j \overbrace{\langle x_i, x_j\rangle}^{\delta_{i,j}} = \lambda_j$,
        \item aus (1),
        \item $x_1 \times x_2 \ueq[(x_1, x_2, x_3) \text{ Basis}] \lambda_1 x_1 + \lambda_2 x_2 + \lambda_3 x_3 \ueq[(1)] \underbrace{x_1 \times x_2, x_1 \rangle}_{=0} x_1 + \underbrace{\langle x_1 \times x_2, x_2 \rangle}_{=0} x_2 + \lambda_3 x_3 = \lambda_3 x_3$,
        
            mit $1 = \| x_1 \times x_2\| = \abs{\lambda_3} \cdot \|x_3\| = \abs{\lambda_3}$ folgt $\lambda_3 = \pm 1$.
    \end{enumerate}
\end{proof}

Die Parallelogramminterpretation von~\ref{sec:23.11}(4) lässt sich noch verallgemeinern:

\section{Definition}\label{sec:23.15}

\begin{definition}
    \emph{Spat}:

    \[v_1, \dots, v_n \in \mathbb{R}^n \to \spat(v_1, \dots, v_n) := \left\{\sum_{i=1}^n \lambda_i v_i | 0 \leq \lambda_i \leq 1 \text{ für alle } i\right\},\]

    \emph{Simplex}:

    \[v_1, \dots, v_n \in \mathbb{R}^n \to \Delta(v_1, \dots, v_n) := \left\{ \sum\limits_{i=1}^n \lambda_i v_i | 0 \leq \lambda_i \text{ für alle $i$ und } \sum\limits_{i=1}^n \lambda_i \leq 1\right\}.\]
\end{definition}

\begin{example}
    \emph{$n=2$}: \[\underbrace{\spat}_{\mathclap{\text{ auch: \enquote{Parallelotop}}}}\left(\begin{pmatrix}1\\0\end{pmatrix}, \begin{pmatrix}2\\2\end{pmatrix}\right): \begin{tikzpicture}\draw[draw=none, fill=orange] (0,0) -- (1,0) -- (1.75, 1) -- (0.75,1) -- cycle;\draw[thick, ->] (0,0) -- (1,0); \draw[thick, ->] (0,0) -- (0.75,1); \draw[thick, dashed] (0.75, 1) -- (1.75, 1);\draw[thick, dashed] (1,0) -- (1.75, 1);  \end{tikzpicture}\]

    \[\underbrace{\Delta}_{\mathclap{\enquote{Pyramiden}}}\left(\begin{pmatrix}1\\0\end{pmatrix}, \begin{pmatrix}2\\2\end{pmatrix}\right): \begin{tikzpicture}\draw[draw=none, fill=orange] (0,0) -- (1,0) -- (0.75, 1) -- cycle;\draw[thick, ->] (0,0) -- (1,0); \draw[thick, ->] (0,0) -- (0.75,1); \draw[thick, dashed] (0.75, 1) -- (1, 0); \end{tikzpicture} \text{ Halbe Fläche}\]
\end{example}

\section{Behauptung}\label{sec:23.16}

\begin{claim}
    \[\vol(\spat(v_1, \dots, v_n)) \ueq[\text{da \enquote{Volumen} mit VZ (D1), (D2), (D3) erfüllt in~\ref{sec:18.2}}] \abs{\det(v_1, \dots, v_n)} = n! \cdot \vol\Delta(v_1, \dots, v_n).\]

    (Simplex-Volumen ohne Beweis)
\end{claim}

\section{Affine Räume im $\mathbb{R}^n$}\label{sec:23.17}

Es gibt prinzipieall zwei Arten, affine Räume (d.h. von Geraden / Evenen \dots) zu beschreiben:

\begin{itemize}
    \item \emph{Parameterdarstellung:} $G_{P, a} = \{P + ta\ |\ t \in \mathbb{R}\} = P + \mathbb{R}a$ ist die \emph{Gerade} im $\mathbb{R}^n$ mit $P \in G_{P, a}$ \enquote{in Richtung} $a \in \mathbb{R}^n$, $a$ heißt \emph{Richtungsvektor}.

        Allg. \emph{affiner Raum:} $P + U$, wo $U = L\overbrace{(a_1, \dots, a_r)}^{\text{Richtungsvektoren}}$, vgl. dazu~\ref{sec:12.22}.
    \item \emph{Normalendarstellung} (einer \emph{Hyperebene} im $\mathbb{R}^n$): Mit einer Gleichung der Form $\langle x, c \rangle = \alpha$, d.h. $H_{c, \alpha} = \{x \in \mathbb{R}^n | \langle x, c \rangle = \alpha \}$ für $c \in \mathbb{R}^n$, $c \neq o$,

        dies definiert für $n = 2$ eine Gerade $G_{p, a}$ mit $c = a^\perp$, der \emph{Normalen} von $G_{p,a}$, die senkrecht auf $G_{p,a}$ stegt: Ist $H_{c,\alpha} = P + U$ mit einem UVR $U$, ist für $a \in U$ $a \perp c$, da

        \[\langle a, c \rangle = \langle a + P - P, c \rangle = \langle \underbrace{a + P}_{\mathclap{\in H_{c,a}}}, c \rangle - \langle \underbrace{P}_{\mathclap{\in H_{c,a}}}, c \rangle = \alpha - \alpha = 0.\]

        Weiter: $\dim U = n - 1$, denn $U = \ker(f)$ mit $f: \mathbb{R}^n \to \mathbb{R}, x \mapsto \langle x, c \rangle$ ist lineare Abbildung mit $\im f = \mathbb{R}$, also $\dim U = \dim \ker f \ueq[\text{Rangsatz}] n - \dim \im f = n - 1$.
    \item Ein Spezialfall der Normalendarstellung ist doe \emph{Hessesche Normalform}: $H_{c,\alpha}$ mit \emph{$\|c\| = 1$}.
\end{itemize}

\section{Beispiel zur Normalendarstellung}\label{sec:23.18}

Eine Ebene $E$ im Raum $\mathbb{R}^3$ kann in der Form

\[E = \left\{\begin{pmatrix}\xi_1\\\xi_2\\\xi_3\end{pmatrix}; \underbrace{\gamma_1\xi_1 + \gamma_2 \xi_2 + \gamma_3\xi_3}_{=\langle x, c \rangle} = \alpha\right\}\]

dargestellt werden; $c = \begin{pmatrix} \gamma_1 \\ \gamma_2 \\ \gamma_3 \end{pmatrix}$ ist darin der Normalenvektor, d.h. $c \perp E$.

Die Ebene $E = \left\{ \begin{pmatrix} x \\ y \\ z \end{pmatrix} \in \mathbb{R}^3; 3x - 2y - z = 2 \right\}$ z.b. steht senkrecht auf $c = \begin{pmatrix} 3 \\ -2 \\ -1 \end{pmatrix}$. In dieser Form nennt man die Normalendarstellung auch oft \emph{Koordinaten\-darstellung} von $E$.

Anderes Bsp: $E = \left\{ \begin{pmatrix} x \\ y \\ z \end{pmatrix} \in \mathbb{R}^3; x = o\right\}$ ist die $y$-$z$-Ebene, und

\[E = \left\{(w, x, y, z) \in \mathbb{R}^3; w - 3x - y + 4z = 10 \right\}\]

ist die ($3$-dimensionale) Hyperebene im $\mathbb{R}^4$, $\perp$ zu $\begin{pmatrix} 1 \\ -3 \\ -1 \\ 4 \end{pmatrix}$.

\section{Senkrechte Projektion / Lote fällen}\label{sec:23.19}

\begin{definition}{\emph{Lot von $x \in \mathbb{R}^n$ auf $y \in \mathbb{R}^n$}:}

    Vektor $p(x, y) := \frac{\langle x, y \rangle}{\langle y, y \rangle} \cdot y$, dieser Vektor heißt auch \emph{senkrechte Projektion von $x$ entlang $y$}. Die Zahl $\frac{\langle x, y \rangle}{\langle y, y \rangle} \in \mathbb{R}$ heißt \emph{Komponente von $x$ entlang $y$.}

    Bestimme $\lambda \in \mathbb{R}$ mit $y \perp (x - \lambda y) : 0 = \langle y, x - \lambda y \rangle = \langle y, x \rangle - \lambda \langle y, y \rangle \Leftrightarrow \lambda = \frac{\langle x, y \rangle}{\langle y, y \rangle}$.
\end{definition}

\begin{remark}
    Werden in~\ref{sec:24.9} senkrechte Projektionen auf einen beliebigen UVR definieren.
\end{remark}

\section{Rechnen mit der Hesseschen / Normalform}\label{sec:23.20}

Sei $E = H_{c, \alpha} = \{ x \in \mathbb{R}^n; \langle x, c \rangle = \alpha \}, c \neq o$.

\begin{enumerate}
    \item \begin{claim}
            \begin{enumerate}
                \item Ist $H_{c, \alpha} \subseteq \mathbb{R}^n$ gegeben, so ist der Abstand von $0$ zu $H_{c, \alpha}$ gegeben als $\dist(0, H_{c, \alpha}) = \frac{\abs{\alpha}}{\|c\|}$.
                \item Ist außerdem $\|c\| = 1$, ist dieser Abstand also $= \abs{\alpha}$.
            \end{enumerate}
        \end{claim}

        \begin{proof}
            Der gesuchte Abstand ist die Länge von $p(x, c)$, also

            \[\dist(o, H_{c, a}) = \| p(x, c) \| = \left\| \frac{\langle x, c \rangle}{\langle c, c \rangle} \cdot c \right\| = \frac{\abs{\langle x, c \rangle}}{\|c\|} = \frac{\abs{\alpha}}{\|c\|}.\]
        \end{proof}
    \item \begin{claim}
            Ist $H_{c, \alpha} \subseteq \mathbb{R}^n$ gegeben, so ist der Abstand von (irgendeinem) $q \in \mathbb{R}^n$ zu $H_{c, \alpha}$ gegeben als

            \[\dist(q, H_{c, \alpha}) = \frac{\abs{\langle q, c \rangle - \alpha}}{\|c\|}.\]
        \end{claim}

        \begin{proof}
            Betrachte die um $-q$ verschobene Ebene $E' := \{x' ; x' + q \in E\}$, dann ist der gesuchte Abstand der von $o$ zu $E'$, für $x' \in E$ also 

            \begin{align*}
                &= \| p(x', c) \| = \|p(x - q, ) \|\\
                &= \left\| \frac{\langle x - q, c \rangle}{\langle c, c \rangle} \cdot c \right\| = \left\| \frac{\langle x, c \rangle}{\|c\|^2} \cdot c - \frac{\langle q, c \rangle}{\|c\|^2} \cdot c\right\|\\
                &= \frac{1}{\|c\|} \cdot \abs{\alpha - \langle q, c \rangle}.
            \end{align*}
        \end{proof}
\end{enumerate}
