\chapter{Euklidische und unitäre Vektorräume}\label{ch:22}

\paragraph{Stichworte: } Bilinearform, hermitesch / symmetrisch, Sesquilinearform, positiv (semi-) definiert, Skalarprodukt, euklidischer / unitärer Raum, Standardskalarprodukt, Norm $\left\| \cdot \right\|$, Cauchy-Schwarz, $\Delta$-Ungleichung

Im Zusammenhang mit Determinanten hatten wir schon $n$-multilineare Abbildungen eingeführt. Für $n = 1$ sind das die gewöhnlichen linearen Abbildungen. Der Fall $n = 2$ führt uns zu geometrischen Begriffen wie \enquote{senkrechtstehen}, \enquote{Länge eines Vektors}, \enquote{Winkel}, \enquote{Abstand} etc., sofern wir spezielle Bilinearformen betrachten, die Skalarprodukte heißen.

\section{Bilinearform}\label{sec:22.1}

\begin{definition}{(Bilinearform)}
    Sei $V$ ein $K$-Vektorraum. Eine Abbildung $b: V \times V \to K$ heißt \emph{Bilinearform}, falls gilt:

    \begin{enumerate}
        \item\[\left.\begin{split}
                b(x_1 + x_2, y_1) = b(x_1, y_1) + b(x_2, y_1)\\
                b(x_1, y_1 + y_2) = b(x_1, y_1) + b(x_1, y_2)
            \end{split}\right\} \forall x_1, x_2, y_1, y_2 \in V\]
        \item \[\left.\begin{split}
                b(\alpha x, y) = \alpha b(x, y)\\
                b(x, \alpha y) = \alpha b(x, y)
            \end{split}\right\} \forall x, y \in V, \alpha \in K\]
    \end{enumerate}
\end{definition}

\section{Bemerkung}\label{sec:22.2}

\begin{remark}
    Per Definition ist eine Bilinearform $b: V \times V \to K$ eine auf $V$ $2$-multilineare Abbildung, vgl.~\ref{sec:18.2}(D1)
\end{remark}

\section{Eigenschaften von Bilinearformen}\label{sec:22.3}

Wir benötigen aber spezielle Bilinearformen. Daher definieren wir folgende Eigenschaften.

\begin{definition}
    \begin{enumerate}
        \item $b$ heißt \emph{symmetrisch}, falls $b(x, y) = b(y, x)$ für alle $x, y \in V$.

            \emph{Bem.} Eine symmetrische Abbildung $b$, die linear in \emph{einem} der Argumente ist, ist auch schon im zweiten Argument linear, und damit eine Bilinearform.
        \item Ist $b$ Bilinearform, so heißt $b$ \emph{alternierend}, falls $b(x, x) = 0$ für alle $x \in V$.
            \begin{enumerate}
                \item \emph{Bem.} Durch Ausrechnen von $0 = b(x + y, x + y) = \underbrace{b(x, x)}_{=0} + b(x, y) + b(y, x) + \underbrace{b(y, y)}_{=0}$ folgt sofort $b(x, y) = - b(y, x)$, sofern $1 + 1 \neq 0$ in $K$ gilt, d.h.\ also nicht in $\mathbb{F}_2, \mathbb{F}_4, \mathbb{F}_8, \dots$! Wird $K \in \{\mathbb{R}, \mathbb{C}\}$ vorausgesetzt, wird daher häufig die Definition ``$b(x, y) = -b(y, x)$'' für ``alternierend'' gegeben.
                \item \emph{Bem.} Diese Definition führt genau auf die Def. ``alternierend'' \emph{(D2)} von~\ref{sec:18.2}, mit $n = 2$.
                \item \emph{Bem.} Eine alternierende Bilinearform ist genau eine Determinantenfunktion im Fall $n = 2$.
                \item \emph{Bem.} Statt ``alternierend'' gibt es auch die Begriffe antisymmetrisch / schiefsymmetrisch.
            \end{enumerate}
    \end{enumerate}
\end{definition}

Für uns sollen jetzt nur die Fälle $K \in \{\mathbb{R}, \mathbb{C}\}$ eine Rolle spielen wo die Werte einer Bilinearform reell und dann $> 0$ oder $< 0$ sein können.

\section{Vereinbarung}\label{sec:22.4}

Im gesamten~\ref{pt:6} betrachten wir nur reelle oder komplexe VRe, d.h.\ der Grundkörper $K$ ist $\mathbb{R}$ oder $\mathbb{C}$, d.h. $K \in \{\mathbb{R}, \mathbb{C}\}$. Für eine komplexe Zahl $z = x + iy$ ist dann $\overline{z} = x - iy$ die konjugiert Komplexe.

Alle Sätze und Definitionen --- sofern nicht ausdrücklich etwas anderes gesagt wird --- die sich auf den komplexen Fall beziehen, gelten dann auch für den reellen Fall, wofür ein ``Konjugiert-Strich'' gegenstandslos ist (denn für $x \in \mathbb{R}$ gilt $\left(\overline{x} = x\right)$).

\section{Hermitesche Abbildungen}\label{sec:22.5}

\begin{definition}
    Sei $V$ ein $K$-VR mit $K \in \{\mathbb{R}, \mathbb{C}\}$, und $b: V \times V \to K$ eine Abbildung.

    \begin{enumerate}
        \item $b$ heißt \emph{hermitesch}, falls $b(x, y) = \overline{b(y, x)}$ für alle $x, y \in V$ gilt.

            (Ist $K = \mathbb{R}$, so ist $b$ dann symmetrisch. Dann würde man $b$ eher ``symmetrisch'' nennen.)

            \emph{Bem.} Eine hermitesche Abbildung $b$, die linear in \emph{einem} der Argumente ist, ist i.a. \emph{nicht} schon im zweiten Argument linear, und damit \emph{keine} Bilinearform. Die ``konjugierte Linearität'' bekommt einen anderen Namen:
        \item Sei $K = \mathbb{C}$. Eine Abbildung $b: V \times V \to \mathbb{C}$ heißt \emph{Sesquilinearform}, falls gilt:
            \begin{enumerate}
                \item \[\left.\begin{split}
                        b(x_1 + x_2, y_1) = b(x_1, y_1) + b(x_2, y_1)\\
                        b(x_1, y_1 + y_2) = b(x_1, y_1) + b(x_1, y_2)
                    \end{split}\right\} \forall x_1, x_2, y_1, y_2 \in V\]
                \item \[\left.\begin{split}
                        b(\alpha x, y) = \alpha b(x, y)\\
                        b(x, \alpha y) = \underbrace{\overline{\alpha}}_{\mathclap{\text{Unterschied zu~\ref{sec:22.1}}}} b(x, y)
                    \end{split}\right\} \forall x, y \in V, \alpha \in \mathbb{C}\]
            \end{enumerate}
        \item Eine hermitesche Sesquilinearform heißt \emph{hermitesche Form}.
    \end{enumerate}
\end{definition}

\section{Lemma}\label{sec:22.6}

\begin{lemma}
    Eine Abbildung $b: V \times V \to \mathbb{C}$, die linear im 1. Argument und hermitesch ist, ist eine Sesquilinearform und somit eine hermitesche Form.
\end{lemma}

\begin{proof}
    \emph{Genügt z.z.:} die 2. Teile der Def in~\ref{sec:22.5}.2) Haben:

    \[b(x_1, y_1 + y_2) = \overline{b(y_1 + y_2, x_1)} = \overline{b(y_1, x_1)} + \overline{b(y_2, x_1)} = b(x_1, y_1) + b(x_1, y_2) \text{ und}\]

    \[b(x, \alpha y) = \overline{b(\alpha y, x)} = \overline{\alpha b(y, x)} = \overline{\alpha} \cdot \overline{b(y, x)} = \overline{\alpha} \cdot b(x, y).\]
\end{proof}

\section{Bemerkung}\label{sec:22.7}

\begin{itemize}
    \item Hermitesche Formen benötigt man, damit $b(x, x)$ stets reell ist 

        \[\left(b(x, x) = \overline{b(x, x)} \Leftrightarrow b(x, x) \in \mathbb{R} \right)\]

        und somit $b(x, x)$ ein Vorzeichen hat. Idealerweise sollte $b(x, x) \geq 0$ sein, damit wir $\sqrt{b(x, x)}$ bilden und es als Länge von $x$ interpretieren können.
    \item Eine reelle hermitesche Form $b: V \times V \to \mathbb{R}$ ist eine symmetrische Bilinearform.
\end{itemize}

\section{Skalarprodukt}\label{sec:22.8}

\begin{definition}
    Sei $V$ ein $K$-VR, $K \in \{\mathbb{R}, \mathbb{C}\}$.

    \begin{enumerate}
        \item Eine hermitesche Form $b: V \times V \to K$ heißt \emph{positiv definiert} falls:
            \begin{enumerate}
                \item $b(x, x) \geq 0$ für alle $x \in V$, und
                \item $b(x, x) = 0 \Leftrightarrow x = 0$.
            \end{enumerate}
        \item Eine positiv definierte hermitesche Form heißt auch positiv definiertes, hermitesches \emph{Skalarprodukt}.
        \item Eine reelle, positiv definierte hermitesche Form heißt \emph{reelles} \emph{Skalarprodukt}.

            (Ein \emph{reelles Skalarprodukt} ist also eine \emph{positiv definierte}, \emph{symmetrische Bilinearform über $\mathbb{R}$}.)
        \item Gilt nur (a) in 1., so heißt $b$ \emph{positiv semidefiniert}.
        \item Entsprechend \emph{negativ (semi-) definiert}, wenn \enquote{$\geq$} in 1.(a) durch \enquote{$\leq$} ersetzt wird.
    \end{enumerate}
\end{definition}

\section{Vereinbarung}\label{sec:22.9}

Unter \enquote{Skalarprodukt} verstehen wir im folgenden stets ein positiv definiertes, hermitesches Skalarprodukt. Reelle Skalarprodukte sind so immer inbegriffen.

Wir schreiben im folgenden $\langle x, y \rangle$ für $b(x, y)$ und $\langle \cdot,\cdot \rangle$ für $b$, wenn $b$ ein Skalarprodukt ist.

Das Skalarprodukt $\langle x, y \rangle$ zweier Vektoren $x, y \in V$ ist also stets eine (komplexe) Zahl / ein Skalar, daher der Name.

\section{Eigenschaften von Skalarprodukten}\label{sec:22.10}

Sei $\langle \cdot, \cdot \rangle$ ein Skalarprodukt auf dem $K$-VR $V$, $K \in \{\mathbb{C}, \mathbb{R}\}$. Dann gilt:

\begin{enumerate}
    \item $\forall x, y \in V: \langle x, y \rangle = \overline{\langle y, x \rangle}$ \hfill (hermitesch)
    \item $\forall x_1, x_2, y \in V: \langle x_1 + x_2, y \rangle = \langle x_1, y \rangle + \langle x_2, y \rangle$ \hfill (linear im 1. Argument)
    \item $\forall x, y \in V\ \forall \alpha \in K: \langle \alpha x, y \rangle = \alpha \langle x, y \rangle$ \hfill (linear im 1. Argument)
    \item $\forall x \in V: \langle x, x \rangle \geq 0, \forall x \in V: \langle x, x \rangle = 0 \Leftrightarrow x = 0$ \hfill (positiv definiert)
    \item $\forall x, y \in V: \langle x, 0 \rangle = \langle 0, y \rangle = 0$.
    \item $\forall x_1, x_2, y \in V\ \forall \alpha, \beta \in K: \langle \alpha x_1 + \beta x_2, y \rangle = \alpha \langle x_1 + y \rangle + \beta \langle x_2, y \rangle$
    \item $\forall x, y_1, y_2 \in V\ \forall \alpha, \beta \in K: \langle x, \alpha y_1 + \beta y_2 \rangle = \overline{\alpha} \langle x, y_1 \rangle + \overline{\beta} \langle x, y_2 \rangle$
\end{enumerate}

\begin{proof}
    \emph{(1) - (4):} folgen bereits aus der Definition.

    \emph{(5):} \[\forall x, y, z \in V: \langle 0, y \rangle = \langle 0 \cdot z, y \rangle = 0 \cdot \langle z, y \rangle = 0\] und \[\langle x, 0 \rangle = \overline{\langle 0, x \rangle} = \overline{0} = 0,\]

    \emph{(6):} Klar mit (2) und (3), haben $(6) \Leftrightarrow (2) \land (3)$,

    \emph{(7):} \begin{align*}
        \langle x, \alpha y_1 + \beta y_2 \rangle &\ueq[(1)] \overline{\langle \alpha y_1 + \beta y_2, x \rangle} \ueq[(2)] \overline{\alpha \langle y_1, x \rangle + \beta \langle y_2, x \rangle}\\
                                                  &\ueq \overline{\alpha} \overline{\langle y_1, x \rangle} + \overline{\beta} \overline{\langle y_2, x \rangle} \ueq[(1)] \overline{\alpha} \langle x, y_1 \rangle + \overline{\beta} \langle x, y_2 \rangle. \quad\qedhere
    \end{align*}
\end{proof}

\section{Beispiel}\label{sec:22.11}

\begin{example}
    Das wichtigste Beispiel ist das Standard-Skalarprodukt:

    \begin{itemize}
        \item Es sei $V := \mathbb{C}^n$, $x = (x_1, \dots, x_n) \in V$, $y = (y_1, \dots, y_n) \in V$,

            so heißt das durch $\langle x, y \rangle := \sum\limits_{i=1}^n x_i \overline{y_i}$ definierte Skalarprodukt (Auch: $\langle x, y \rangle = x^T \cdot \overline{y}$) das \emph{(kanonische) Skalarprodukt} bzw. \emph{Standard-Skalarprodukt}.

            (\emph{Bem.} anstelle $\langle x, y \rangle = x^T \cdot \overline{y}$ ist auch $\overline{x}^T \cdot y$ möglich, wenn für Sesquilinearformen die Konjugation im 1. statt 2. Argument genommen wird, was Konventionssache ist. Der Vektor $\overline{x}^T$ heißt der zu $x$ hermitesch adjungierte Vektor.)

            Für $n = 2, x = \left(\begin{smallmatrix}\xi_1\\\xi_2\end{smallmatrix}\right), y = \left(\begin{smallmatrix}\eta_1\\\eta_2\end{smallmatrix}\right) \in \mathbb{C}^2$ ist $\langle x, y \rangle = \xi_1 \overline{\eta_1} + \xi_2 \overline{\eta_2}$.
        \item Für $K = \mathbb{R}, V := \mathbb{R}^n$, ist mit $\langle x, y \rangle := \sum\limits_{i=1}^n x_i y_i$ für $x = (x_1, \dots, x_n)^T \in V$, $y = (y_1, \dots, y_n)^T \in V$, das (\emph{reelle}) \emph{Standard-Skalarprodukt} definiert. (Auch: $\langle x, y \rangle = \underbrace{x^T}_{\text{Zeilenvektor}} \cdot \underbrace{y}_{\text{Spaltenvektor}}$)

            Für $n = 2, x = \left(\begin{smallmatrix}\xi_1\\\xi_2\end{smallmatrix}\right), y = \left(\begin{smallmatrix}\eta_1\\\eta_2\end{smallmatrix}\right) \in \mathbb{R}^2$ ist $\langle x, y \rangle = \xi_1 \eta_1 + \xi_2 \eta_2$.
        \item Sei $V := C([\alpha, \beta])$ der $\mathbb{C}$-VR der auf dem Intervall $[\alpha, \beta]\subseteq\mathbb{R}$ stetigen koomplexwerigen Funktionen, d.h. $C([\alpha, \beta]) := \{f: [\alpha, \beta]\to\mathbb{C}; \text{ $f$ stetig}\}$

            Sei $p \in V$ so, dass $p$ nur reelle positive Werte annimmt, d.h. $p: [\alpha, \beta]\to \mathbb{R}_{>0}$, $p$ stetig. Dann ist für $f, g \in V$ durch

            \[\langle f, g \rangle := \int_\alpha^\beta p(t) f(t) \overline{g(t)} dt \quad\quad \oast\]

            ein Skalarprodukt definiert.
    \end{itemize}
\end{example}

\begin{proof}
    \begin{enumerate}
    \setcounter{enumi}{3}
        \item $\langle f, f \rangle = \int\limits_\alpha^\beta p(t) \abs{f(t)}^2 dt > 0$ und $= 0 \Leftrightarrow \forall t \in [\alpha, \beta]: f(t) = 0$, d.h. $f = 0$.
        \setcounter{enumi}{5}
        \item \begin{align*}
            \langle \alpha_0 f_1 + \beta_0 f_2, g \rangle &= \int_\alpha^\beta p(t) \cdot \left(\alpha_0 f_1(t) + \beta_0 f_2(t)\right) \overline{g(t)} dt\\
                                                          &= \alpha_0 \int_\alpha^\beta p(t) f_1(t) \overline{g(t)} dt + \beta_0 \int_\alpha^\beta p(t) f_2(t) \overline{g(t)} dt\\
                                                          &= \alpha_0 \langle f_1, g \rangle + \beta_0 \langle f_2, g \rangle
        \end{align*}
        \setcounter{enumi}{0}
        \item \begin{align*}
            \langle f, g \rangle &\ueq \int_\alpha^\beta p(t) f(t) \overline{g(t)} dt = \overline{\int_\alpha^\beta \overline{p(t) f(t) \overline{g(t)} dt}}\\
                             &\ueq \overline{\int_\alpha^\beta \overline{p(t)}\  \overline{f(t)} g(t) dt}\\
                             &\ueq[p(t) \text{ reell: } p(t) = \overline{p(t)}] \overline{\int_\alpha^\beta p(t) \overline{f(t)} g(t) dt} = \overline{\langle g, f \rangle}.
        \end{align*}
    \end{enumerate}
\end{proof}

\section{Unitäre Räume}\label{sec:22.12}

\begin{definition}
    Ein reeller oder komplexer Vektorraum mit Skalarprodukt heißt \emph{euklidischer} oder \emph{unitärer Raum}
\end{definition}

\begin{remark}
    Der Begriff \enquote{euklidisch} wird nur im reellen Fall benutzt.
\end{remark}

\section{Norm und Abstand}\label{sec:22.13}

\begin{definition}
    Gegeben sei ein Skalarprodukt $\langle \cdot, \cdot \rangle$ auf $V$. Dann definieren wir für $x, y$

    die \emph{Norm} von $x$: $\|x\| := \sqrt{\langle x, x \rangle}$ \hfill (denn $\langle x, x \rangle \geq 0$),
    
    den \emph{Abstand} von $x$ und $y$: $d(x, y) := \| x - y \|$.
\end{definition}

\begin{remark}
    \begin{itemize}
        \item Die Funktion $d: V^2 \to \mathbb{R}_{\geq 0}$ ist dann eine \emph{Metrik}, vgl. Analysis.
        \item die Norm $\|x\|$ eines Vektors $x$ bezeichnet man auch als \emph{Länge} von $x$.

            Gelegentlich schreibt man auch $|x|$ statt $\|x\|$ und nennt dies den \emph{Betrag} von $x$.
    \item das Wort \enquote{Länge} wurde in vorangehenden Kapiteln ausschließlich synonym für \enquote{Kardinalität} benutzt. Erst jetzt sprechen wir von \enquote{Länge} eines Vektors im üblichen, anschaulichen Sinne, denn:
        \item Haben speziell $d(x, o) = \|x\|$, d.h. die Norm von $x$ ist der Abstand von $x$ zu $o$.
    \end{itemize}
\end{remark}

\section{Beispiel}\label{sec:22.14}

\begin{example}
    \begin{itemize}
        \item Die Norm von $x \in \mathbb{C}^n$ im Falle des Standardproduktes beträgt

            \[\|x\| = \sqrt{\langle x, x \rangle} = \left(\sum_{i=1}^n |\xi_i|^2\right)^{\frac{1}{2}},\]

            wobei $|\cdot|$ der Betrag auf $\mathbb{C}$ ist, d.h. $|\xi| = \sqrt{\xi \cdot \overline{\xi}}$, also $\|x\| = \left(\sum\limits_{i=1}^n \xi_i \overline{\xi_i}\right)^{\frac{1}{2}}$.
        \item Die Norm von $x \in \mathbb{R}^n$ im Falle des Standardskalarproduktes beträgt

            \[\|x\| = \sqrt{\langle x, x \rangle} = \left(\sum_{i=1}^n |\xi_i|^2\right)^{\frac{1}{2}},\]

            wo $|\cdot|$ der Betrag auf $\mathbb{R}$ ist, d.h. $|\xi| = \sqrt{\xi^2}$, also $\|x\| = \left(\sum\limits_{i=1}^n \xi_i^2\right)^{\frac{1}{2}}$.
        \item Speziell $n = 2, x \in \mathbb{R}^2:\ \|x\|=\sqrt{\xi_1^2 + \xi_2^2}$, die \enquote{Länge} laut Pythagoras, vgl.~\ref{ch:23}.
    \end{itemize}
\end{example}

\section{Cauchy-Schwarzsche Ungleichung / Ungleichung von Cauchy-Schwarz (kurz: \enquote{C-S})}\label{sec:22.15}

Die folgende Abschätzung für ein Skalarprodukt ist mit das wichtigste Werkzeug der linearen Algebra in Anwendungen:

\begin{theorem}
    In einem unitären Raum $V$ gilt $\forall x, y \in V: \underbrace{|\langle x, y \rangle|}_{\mathclap{\text{(komplexer) Betrag}}} \leq \|x\| \cdot \|y\|$,

    wobei \enquote{$=$} anstelle \enquote{$\leq$} genau dann gilt, wenn $x, y$ linear abhängig sind.
\end{theorem}

\begin{proof}
    Ist $\langle x, y \rangle = 0$, ist nichts zu zeigen. Sei also \OE $\langle x, y \rangle \neq 0$, und damit $x \neq 0, y \neq 0$ wegen~\ref{sec:22.10}(5).

    Setze

    \[\alpha := \frac{\overline{\langle x, y \rangle}}{\abs{\langle x, y \rangle}} \cdot \|y\|, \beta := \frac{\abs{\langle x, y \rangle}}{\| y \|}.\]

    Dann ist

    \begin{align*}
        0 &\neq \| \alpha x \cdot \beta y \|^2 = \langle \alpha x - \beta y, \alpha x - \beta y \rangle = \alpha \overline{\alpha} \| x \|^2 - \alpha \overline{\beta} \langle x, y \rangle - \beta \overline{alpha} \langle y, x \rangle + \beta \overline{\beta} \| y \|^2\\
          &= \|y\|^2 \cdot \|x\|^2 - \overline{\langle x, y \rangle} \cdot \langle x, y \rangle - \underbrace{\langle x, y \rangle \cdot \overline{\langle x, y \rangle} + \abs{\langle x, y \rangle}^2}_{=0}\\
          &= \|x\|^2 \cdot \|y\|^2 - \abs{\langle x, y \rangle}^2.
    \end{align*}

    Damit folgt die behauptete Ungleichung, und $=0$ kann nur für $\alpha x - \beta y = 0$ eintreten nach~\ref{sec:22.16}(4).
\end{proof}

\section{Weitere Rechenregeln für die Norm}\label{sec:22.16}

\begin{theorem}
    Für die Norm $\|\cdot\|$ in einem unitären Raum $V$ gilt:

    \begin{enumerate}
        \item $\forall x \in V: \|x\| \geq 0$, und $\| x \| = 0 \Leftrightarrow x = 0$.
        \item $\forall x \in V\ \forall \alpha \in K: \|\alpha x \| = |\alpha| \cdot \|x\|$, wo $|\alpha|$ der Betrag von $\alpha \in K \in \{\mathbb{R}, \mathbb{C}\}$ ist,
        \item $\forall x, y \in V: \| x + y\| \leq \|x\| + \|y\|$. \hfill (\emph{Dreiecksungleichung})
    \end{enumerate}
\end{theorem}

\begin{proof}
    \begin{enumerate}
        \item folgt direkt aus~\ref{sec:22.10}(4) der positiven Definiertheit von $\langle \cdot, \cdot \rangle$.
        \item $\|\alpha x\| = \sqrt{\langle \alpha x, \alpha x \rangle} = \sqrt{\alpha \cdot \overline{\alpha} \langle x, x \rangle} = \sqrt{|\alpha|^2 \cdot \langle x, x \rangle} = |\alpha| \cdot \|x\|$.
        \item Benutzen hier die C-S-Ungleichung~\ref{sec:22.15} wie folgt:

            \begin{align*}
                \|x+y\|^2 = \langle x + y, x + y \rangle &= \|x\|^2 + \langle x, y \rangle + \langle y, x \rangle + \| y \|^2\\
                                                         &\leq \|x\|^2 + 2 \cdot |\langle x, y \rangle| + \|y\|^2\\
                                                         &\leq \|x\|^2 + 2\|x\| \cdot \|y\| + \|y\|^2 = \left(\|x\| + \|y\|\right)^2.
            \end{align*}

            jetzt $\sqrt{ }$-ziehen und beachten, dass alle Terme $\geq 0$ sind,
    \end{enumerate}
\end{proof}

\begin{remark}
    \begin{enumerate}
        \item Schreiben kurz \enquote{$\Delta$-Ungleichung} für \enquote{Dreiecksungleichung}.
        \item Eine Abbildung $x \mapsto \|x\|$ eines reellen / komplexen VRs $V$ heißt \emph{Norm von $V$}, wenn~\ref{sec:22.16}(1)-(3) gelten.
    \end{enumerate}
\end{remark}

\section{Beispiel}\label{sec:22.17}

\begin{example}
    \begin{itemize}
        \item Der unitäre Raum $C([\alpha, \beta])$ mit dem Skalarprodukt $\oast$ hat als Norm $\|f\| := \left(\int\limits_\alpha^\beta p(t) |f(t)|^2 dt\right)^{\frac{1}{2}}$.

            Die C-S-Ungleichung lauet hier

            \[\abs{\int_\alpha^\beta p(t)f(t)\overline{g(t)}dt} \leq \left(\int_\alpha^\beta p(t) |f(t)|^2 dt\right)^{\frac{1}{2}} \cdot \left(\int_\alpha^\beta p(t)|g(t)|^2 dt\right)^{\frac{1}{2}}\]

            Übung: Wie lautet hier die Dreiecksungleichung?
        \item Die C-S-Ungleichung im Falle $V = \mathbb{C}^n$ lautet

            \hfill $\abs{\sum\limits_{i=1}^n \xi_1 \overline{\eta_i}}^2 \leq \left(\sum\limits_{i=1}^n \abs{\xi_i}^2\right) \cdot \left(\sum\limits_{i=1}^n \abs{\eta_i}^2\right)$. \hfill (haben Ungleichung quadriert)

            im Falle $V = \mathbb{R}^n$ lautet sie $\abs{\sum\limits_{i=1}^n \xi_i \eta_i}^2 \leq \left(\sum\limits_{i=1}^n \xi_i^2\right) \cdot \left(\sum\limits_{i=1}^n \eta_i^2\right)$.
    \end{itemize}
\end{example}

\section{Bemerkung}\label{sec:22.18}

\begin{remark}
    Für eine (selbstadjungierte) Matrix $A \in \mathbb{C}^{n \times n}$ wird durch $\langle x, y \rangle_A := \langle x, Ay \rangle$ eine hermitesche Form definiert. Wann ist diese aber ein Skalarprodukt, d.h. positiv definiert? Werden ein Kriterium in L27 (ähm hat ja nicht so gut geklappt) kennenlernen und dort mit dem Trägheitssatz von Sylvester beweisen.
\end{remark}
