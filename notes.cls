\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{notes}[Notes]

\RequirePackage[german]{babel}
\LoadClass[a4paper, 12pt]{book}
\RequirePackage{csquotes}
\RequirePackage{amsthm}
\RequirePackage[backend=bibtex,bibstyle=authoryear]{biblatex}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{tikz}
\RequirePackage{libertine}
\RequirePackage{hyperref}
\RequirePackage{ulem}
\RequirePackage{calc}
\RequirePackage{mathtools}
\RequirePackage{parskip}
\RequirePackage{enumitem}
\RequirePackage{framed}
\RequirePackage{stmaryrd}
%\RequirePackage{autobreak}
\RequirePackage[all]{xy}
\RequirePackage{chngcntr}
\RequirePackage{bookmark}
\RequirePackage{stackengine}
\RequirePackage{fancyhdr}
\RequirePackage{tkz-euclide}

\usetkzobj{all}
\usetikzlibrary{quotes, angles}

\setlength{\headheight}{15pt}
\fancypagestyle{plain}{\fancyhf{}\renewcommand{\headrulewidth}{0pt}}

\pagestyle{fancy}
\fancyhf{}% Clear header/footer
\fancyhead[LO]{\nouppercase\leftmark}
\fancyhead[RE]{\nouppercase\rightmark}
\fancyhead[RO,LE]{\thepage}

\theoremstyle{definition}
\newtheorem{definition}{Def}[section]
\newtheorem{theorem}{Satz}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{deduction}{Folgerung}[section]
\newtheorem{corollary}{Korollar}[section]
\newtheorem{application}{Anwendung}[section]
\newtheorem{discussion}{Diskussion}[section]
\newtheorem{claim}{Beh}[section]

\theoremstyle{remark}
\newtheorem*{remark}{Bem}
\newtheorem*{addition}{Zusatz zur Def}
\newtheorem*{bez}{Bezeichnung}
\newtheorem*{example}{Bsp}

\theoremstyle{plain}

\counterwithin*{equation}{section}
\counterwithin*{equation}{subsection}
\renewcommand\theequation{\arabic{equation}}

\newcommand*{\estimate}{\mathrel{\hat=}}

\DeclareMathOperator\rg{rg}
\DeclareMathOperator\id{id}
\DeclareMathOperator\diag{diag}
\DeclareMathOperator\im{im}
\DeclareMathOperator\Spur{Spur}
\DeclareMathOperator\Hom{Hom}
\DeclareMathOperator\End{End}
\DeclareMathOperator\Aut{Aut}
\DeclareMathOperator\spat{spat}
\DeclareMathOperator\vol{vol}
\DeclareMathOperator\dist{dist}

\newcommand\bracetext[2]{%
  \par\smallskip
   \noindent\makebox[\textwidth][r]{$\text{#1}\left\{
    \begin{minipage}{\textwidth}
    #2
    \end{minipage}
  \right.\nulldelimiterspace=0pt$}\par\smallskip
}

\newcommand\bracetextright[2]{%
  \par\smallskip
   \noindent\makebox[\textwidth][l]{$\left.
    \begin{minipage}{\textwidth}
    #1
    \end{minipage}
  \right\}\text{#2}\nulldelimiterspace=0pt$}\par\smallskip
}

%\newcommand\oast{\stackMath\mathbin{\stackinset{c}{0ex}{c}{0ex}{\ast}{\bigcirc}}}

\newcommand{\notestitle}[2]{
  \begin{titlepage}
	\centering
	{\scshape\LARGE Heinrich Heine Universität Düsseldorf\par}
	\vspace{1cm}
	{\scshape\Large Digitalisiert von Studenten für Studenten\par}
	\vspace{1.5cm}
	{\huge\bfseries #1\par}
	\vspace{2cm}
	{\Large\itshape Skript von #2\par}
	\vfill
    % Bottom of the page
	{\large \today\par}
  \end{titlepage}
}

\renewcommand{\Re}{\operatorname{Re}}
\renewcommand{\Im}{\operatorname{Im}}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}

\newlength{\currentparskip}
\setlength{\currentparskip}{\parskip}
\newcommand{\@minipagerestore}{\setlength{\parskip}{\currentparskip}}

\newcommand{\ueq}[1][]{%
  \if\relax\detokenize{#1}\relax
    \sbox0{$\underbrace{=}_{}$}%
    \mathrel{\mathmakebox[\wd0]{=}}
  \else
    \mathrel{\underbrace{=}_{\mathclap{#1}}}
  \fi}
