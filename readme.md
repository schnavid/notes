# Mathematik

## Analysis

Professor:  Apl. Prof. Dr. Axel Grünrock

[Seite der Analysis 1 Vorlesung WiSe 2019/20](http://reh.math.uni-duesseldorf.de/~gruenroc/AnaI_HP/Ana1WiSe1920_hp.html)

[Aktuelles Skript](https://schnavid.gitlab.io/notes/mathematik/analysis.pdf)

## Lineare Algebra

Professor: Priv.-Doz. Dr. Karin Halupczok

[Seite der Lineare Algebra 1 Vorlesung WiSe 2019/20](http://reh.math.uni-duesseldorf.de/~internet/LAI_WS1920/)

[Aktuelles Skript](https://schnavid.gitlab.io/notes/mathematik/linalg.pdf)
